import com.fasterxml.jackson.databind.ObjectMapper;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Map;
import java.util.Map.Entry;

import org.hamcrest.Matchers;

String filteredGroupDataAttrListJSONString = "${filteredGroupDataAttrListJSONString}";
String filteredGroupDataListJSONString = "${filteredGroupDataListJSONString}";

String filterCondition = replaceColumnReferences();
output = stripFilterCondition(filterCondition);

def replaceColumnReferences(){

	String filterCondition = "${filterCondition}";	

	HashMap<String,String> allTablesReferences = new ObjectMapper().readValue("${allTablesReferencesJSONString}", HashMap.class);
	
	while(filterCondition.indexOf("{")>-1){
		int startWith = filterCondition.indexOf("{");
		int endWith = filterCondition.indexOf("}");
		
		String columnReferences = filterCondition.substring(startWith+1,endWith);
		String columnPrefix = allTablesReferences.get(columnReferences);
		filterCondition = filterCondition.replace("{"+columnReferences+"}",columnPrefix);
	}
	
	return filterCondition;
}

def stripFilterCondition(String filterCondition){
	
	String finalResult = "";
	
	while(filterCondition.indexOf("(")>-1){
		int startWith = filterCondition.indexOf("(");
		int endWith = filterCondition.indexOf(")");
		
		finalResult += filterCondition.substring(0,startWith);		
		finalResult += solveBracket(filterCondition.substring(startWith+1,endWith));
		
		filterCondition = filterCondition.substring(endWith+1,filterCondition.length());
	}
	finalResult += filterCondition;	
	return finalResult;
}

def solveBracket(String statement){
	if(statement.indexOf(",")>-1){
		String[] condition = statement.split("\\,");				
		return filterList(condition);
	}
	return "";
}

def filterList(condition){

	String column = condition[0];
	String conditionList = condition[1];

	List<HashMap<String,Object>> filteredGroupDataAttrList = new ObjectMapper().readValue(filteredGroupDataAttrListJSONString, List.class);
	List<HashMap<String,Object>> filteredGroupDataList = new ObjectMapper().readValue(filteredGroupDataListJSONString, List.class);
	
	List<String> operands = getListFromPattern("[=]|[!][=]|[<]|[>]", conditionList);
	if(operands.size()!=1){
		throw new Exception("No operand found in filtered condition (must only be 1)");
	}
	
	List<String> twoObjects = getListFromPattern("([A-Z]{6}[.][A-Z]{4}|['].*['])", conditionList);
	if(twoObjects.size()!=2){
		throw new Exception("Wrong format [TABLENAME].[COLSNAME][operand][TABLENAME].[COLSNAME]");
	}
	
	String operand = operands.get(0);
	String key = twoObjects.get(0).trim();	
	String value = twoObjects.get(1).trim();
	if(value.indexOf("'")>-1){
		value = value.replace("'","");		
	}else{
		boolean found = false;
		for(HashMap<String,Object> filteredGroupData:filteredGroupDataList){
			for(Entry<HashMap<String,Object>> entry:filteredGroupData.entrySet()){
				if(entry.getKey().equalsIgnoreCase(value)){
					value = (String)entry.getValue();
					found = true;
					break;
				}
			}
			if(found){
				break;
			}
		}
	}
	
	switch(operand){
		case "=":
			filteredGroupDataAttrList = select(filteredGroupDataAttrList, 
					having(on(HashMap.class).get(key).toString(),
						Matchers.equalToIgnoringCase(value)));
			break;
			
		case "!=":
			filteredGroupDataAttrList = select(filteredGroupDataAttrList, 
					having(on(HashMap.class).get(key).toString(), 
						Matchers.not(Matchers.equalToIgnoringCase(value))));
			break;		
			
		case ">":
			List<HashMap<String,Object>> newArrayList = new ArrayList<>();
			for(HashMap<String,Object> filteredGroupDataAttr:filteredGroupDataAttrList){
				boolean found = false;
				for(Entry<HashMap<String,Object>> entry:filteredGroupDataAttr.entrySet()){
					if(entry.getKey().equalsIgnoreCase(key) 
							&& Integer.parseInt(value)>Integer.parseInt(entry.getValue())){
						found = true;
						break;
					}
				}
				
				if(!found){
					newArrayList.add(filteredGroupDataAttr);
				}				
			}
			filteredGroupDataAttrList = newArrayList;
			break;
			
		case "<":
			List<HashMap<String,Object>> newArrayList = new ArrayList<>();
			for(HashMap<String,Object> filteredGroupDataAttr:filteredGroupDataAttrList){
				boolean found = false;
				for(Entry<HashMap<String,Object>> entry:filteredGroupDataAttr.entrySet()){
					if(entry.getKey().equalsIgnoreCase(key) 
							&& Integer.parseInt(value)<Integer.parseInt(entry.getValue())){
						found = true;
						break;
					}
				}
				
				if(!found){
					newArrayList.add(filteredGroupDataAttr);
				}				
			}
			filteredGroupDataAttrList = newArrayList;
			break;
			
		default:
			throw new Exception("Operand not supported [use only =,!=,<,>]: "+operand);
			
	}	
	
	if(filteredGroupDataAttrList.size()<1){
		throw new Exception("Unable to select list from "+conditionList);
	}
	
	return filteredGroupDataAttrList.get(0).get(column);
}

def getListFromPattern(regexp,rawString){
	Pattern pattern = Pattern.compile(regexp);
	Matcher matcher = pattern.matcher(rawString);
	
	List<String> listMatches = new ArrayList<String>();
	while (matcher.find()) {
		listMatches.add(matcher.group(0));
	}
	return listMatches;
}