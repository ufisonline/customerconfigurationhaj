
java.util.Iterator<ResourceAlloc> i = objList.iterator();

List<String> uniqueList = new ArrayList<String>();
while(i.hasNext()) {
	ResourceAlloc ra = (ResourceAlloc) i.next();
	if(uniqueList.contains(ra.getId())){
		continue;
	}
	uniqueList.add(ra.getId());	
}

java.util.Collections.sort(uniqueList);

String transacId = "";
for(String single:uniqueList){
	transacId += (transacId.length()==0)?single:","+single;
}

ResourceAlloc r = $ResourceAlloc1;
if(!uniqueList.contains(r.getId())){
	return;
}
	