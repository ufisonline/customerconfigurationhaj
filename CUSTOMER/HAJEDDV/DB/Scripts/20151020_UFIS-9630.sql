ALTER TABLE VIPTAB ADD DES3 CHAR(3);
ALTER TABLE VIPTAB ADD DES4 CHAR(4);
ALTER TABLE VIPTAB ADD RETI CHAR(14);
ALTER TABLE VIPTAB MODIFY SEAT CHAR(4);


ALTER TABLE VIPTAB MODIFY DES3  DEFAULT ' ';
ALTER TABLE VIPTAB MODIFY DES4  DEFAULT ' ';
ALTER TABLE VIPTAB MODIFY RETI  DEFAULT ' ';

COMMENT ON COLUMN VIPTAB.DES3 IS 'Passenger Destination Airport IATA Code';
COMMENT ON COLUMN VIPTAB.DES4 IS 'Passenger Destination Airport ICAO Code';
COMMENT ON COLUMN VIPTAB.RETI IS 'Received Time';
COMMENT ON COLUMN VIPTAB.SEAT IS 'Passenger Seat Number';
COMMIT;

DELETE FROM SYSTAB WHERE TANA='VIP' AND FINA='SEAT';
COMMIT;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') ADDI
,a.column_name FINA
,'SIN' HOPO
,a.data_length FELE
, substr(a.data_type,0,1) FITY
, 'N' INDX 
,'TAB' PROJ
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' SYST
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'VIPTAB' 
and a.column_name IN ( 
'DES3'
,'DES4'
,'RETI'
,'SEAT'
);
commit;



ALTER TABLE SRLTAB ADD PAXN CHAR(64);
ALTER TABLE SRLTAB ADD PAXR CHAR(64);
ALTER TABLE SRLTAB ADD SEAT CHAR(4);
ALTER TABLE SRLTAB ADD DES3 CHAR(3);
ALTER TABLE SRLTAB ADD DES4 CHAR(4);
ALTER TABLE SRLTAB ADD REFR CHAR(64);


ALTER TABLE SRLTAB MODIFY PAXN  DEFAULT ' ';
ALTER TABLE SRLTAB MODIFY PAXR  DEFAULT ' ';
ALTER TABLE SRLTAB MODIFY SEAT  DEFAULT ' ';
ALTER TABLE SRLTAB MODIFY DES3  DEFAULT ' ';
ALTER TABLE SRLTAB MODIFY DES4  DEFAULT ' ';
ALTER TABLE SRLTAB MODIFY REFR  DEFAULT ' ';

COMMENT ON COLUMN SRLTAB.PAXN IS 'Passenger Name';
COMMENT ON COLUMN SRLTAB.PAXR IS 'Passenger Request';
COMMENT ON COLUMN SRLTAB.SEAT IS 'Passenger Seat Number';
COMMENT ON COLUMN SRLTAB.DES3 IS 'Passenger Destination Airport IATA Code';
COMMENT ON COLUMN SRLTAB.DES4 IS 'Passenger Destination Airport ICAO Code';
COMMENT ON COLUMN SRLTAB.REFR IS 'Received From';
COMMIT;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') ADDI
,a.column_name FINA
,'SIN' HOPO
,a.data_length FELE
, substr(a.data_type,0,1) FITY
, 'N' INDX 
,'TAB' PROJ
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' SYST
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'SRLTAB' 
and a.column_name IN ( 
'PAXN'
,'PAXR'
,'SEAT'
,'DES3'
,'DES4'
,'REFR'
);
commit;

