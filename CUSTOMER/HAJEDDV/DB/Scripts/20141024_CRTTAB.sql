SET DEFINE OFF;
Insert into CRTTAB
   (URNO, CTYP, DESR, ALOC, HOPO, CDAT, LSTU, USEC, USEU, VAFR, VATO)
 Values
   (1, 'CIC', 'Check in Counter', 'CIC', 'HAJ', '              ', '              ', ' ', ' ', '              ', '              ');
Insert into CRTTAB
   (URNO, CTYP, DESR, ALOC, HOPO, CDAT, LSTU, USEC, USEU, VAFR, VATO)
 Values
   (2, 'SEC', 'Security Counter', 'SEC', 'HAJ', '              ', '              ', ' ', ' ', '              ', '              ');
Insert into CRTTAB
   (URNO, CTYP, DESR, ALOC, HOPO, CDAT, LSTU, USEC, USEU, VAFR, VATO)
 Values
   (3, 'IMM', 'Immigration Counter', 'IMM', 'HAJ', '              ', '              ', ' ', ' ', '              ', '              ');
Insert into CRTTAB
   (URNO, CTYP, DESR, ALOC, HOPO, CDAT, LSTU, USEC, USEU, VAFR, VATO)
 Values
   (4, 'TXF', 'Transfer Counter', 'TXF', 'HAJ', '              ', '              ', ' ', ' ', '              ', '              ');
Insert into CRTTAB
   (URNO, CTYP, DESR, ALOC, HOPO, CDAT, LSTU, USEC, USEU, VAFR, VATO)
 Values
   (5, 'LOB', 'Lost Baggage Counter', 'LOB', 'HAJ', '              ', '              ', ' ', ' ', '              ', '              ');
Insert into CRTTAB
   (URNO, CTYP, DESR, ALOC, HOPO, CDAT, LSTU, USEC, USEU, VAFR, VATO)
 Values
   (6, 'CUS', 'Customs', 'CUS', 'HAJ', '              ', '              ', ' ', ' ', '              ', '              ');
COMMIT;
