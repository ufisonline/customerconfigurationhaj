DROP TABLE UFIS_MONITOR CASCADE CONSTRAINTS;

CREATE TABLE UFIS_MONITOR
(
  PNODE   CHAR(100 BYTE)                        DEFAULT ' ',
  CNODE   CHAR(100 BYTE)                        DEFAULT ' ',
  CDAT    CHAR(14 BYTE)                         DEFAULT ' ',
  LSTU    CHAR(14 BYTE)                         DEFAULT ' ',
  STATUS  CHAR(1 BYTE)                          DEFAULT ' ',
  TRIES   NUMBER                                DEFAULT 0
)
TABLESPACE CEDA01
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;