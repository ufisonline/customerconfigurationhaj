SET DEFINE OFF;
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FSM', 'BATCH_PROCESSING', 'DISPLAY_FIELDS', '[{"ArrivalDepartureId":"A""Caption":"Flight Number""PropertyName":"ArrivalFlight.FullFlightNumber""IsDefault":true}{"ArrivalDepartureId":"A""Caption":"Aircraft Stand""PropertyName":"ArrivalFlight.PositionOfArrival"}{"ArrivalDepartureId":"A""Caption":"Gate 1""PropertyName":"ArrivalFlight.GateOfArrival1"}{"ArrivalDepartureId":"A""Caption":"Gate 2""PropertyName":"ArrivalFlight.GateOfArrival2"}{"ArrivalDepartureId":"A""Caption":"Belt 1""PropertyName":"ArrivalFlight.Belt1"}{"ArrivalDepartureId":"A""Caption":"Belt 2""PropertyName":"ArrivalFlight.Belt2"}{"ArrivalDepartureId":"A""Caption":"Scheduled Time Of Arrival""PropertyName":"ArrivalFlight.ScheduledTimeOfArrival""DisplayFormat":"HH:mm/dd"}{"ArrivalDepartureId":"A""Caption":"Requested Schedule Time Of Arrival""PropertyName":"ArrivalFlight.RequestedScheduledTimeOfArrival""DisplayFormat":"HH:mm/dd"}{"ArrivalDepartureId":"D""Caption":"Flight Number""PropertyName":"DepartureFlight.FullFlightNumber""IsDefault":true}{"ArrivalDepartureId":"D""Caption":"Aircraft Stand""PropertyName":"DepartureFlight.PositionOfDeparture"}{"ArrivalDepartureId":"D""Caption":"Gate 1""PropertyName":"DepartureFlight.GateOfDeparture1"}{"ArrivalDepartureId":"D""Caption":"Gate 2""PropertyName":"DepartureFlight.GateOfDeparture2"}{"ArrivalDepartureId":"D""Caption":"Scheduled Time Of Departure""PropertyName":"DepartureFlight.ScheduledTimeOfDeparture""DisplayFormat":"HH:mm/dd"}{"ArrivalDepartureId":"A""Caption":"Status""PropertyName":"ArrivalFlight.OperationalType"}{"ArrivalDepartureId":"D""Caption":"Status""PropertyName":"DepartureFlight.OperationalType"}{"ArrivalDepartureId":"D""Caption":"Requested Schedule Time Of Departure""PropertyName":"DepartureFlight.RequestedScheduledTimeOfDeparture""DisplayFormat":"HH:mm/dd"}]', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FSM', 'BATCH_PROCESSING', 'EDIT_FIELDS', '[{"ArrivalDepartureId":"A""Caption":"Aircraft Stand""PropertyName":"PositionOfArrival""LookupCategory":"PSTTAB"}{"ArrivalDepartureId":"A""Caption":"Gate 1""PropertyName":"GateOfArrival1""LookupCategory":"GATTAB"}{"ArrivalDepartureId":"A""Caption":"Gate 2""PropertyName":"GateOfArrival2""LookupCategory":"GATTAB"}{"ArrivalDepartureId":"A""Caption":"Belt 1""PropertyName":"Belt1""LookupCategory":"BLTTAB"}{"ArrivalDepartureId":"A""Caption":"Belt 2""PropertyName":"Belt2""LookupCategory":"BLTTAB"}{"ArrivalDepartureId":"A""Caption":"Requested Schedule Time""PropertyName":"RequestedScheduledTimeOfArrival"}{"ArrivalDepartureId":"D""Caption":"Aircraft Stand""PropertyName":"PositionOfDeparture""LookupCategory":"PSTTAB"}{"ArrivalDepartureId":"D""Caption":"Gate 1""PropertyName":"GateOfDeparture1""LookupCategory":"GATTAB"}{"ArrivalDepartureId":"D""Caption":"Gate 2""PropertyName":"GateOfDeparture2""LookupCategory":"GATTAB"}{"ArrivalDepartureId":"D""Caption":"Requested Schedule Time""PropertyName":"RequestedScheduledTimeOfDeparture"}{"ArrivalDepartureId":"B""Caption":"Status""PropertyName":"OperationalType""LookupType":"CommonEntityCollectionBuilder""LookupCategory":"BatchProcessingFlightTypes""IsMandatory":true}]', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FSM', 'GLOBAL', 'LOOKUPS', '[{"CategoryCode":"ACTTAB""CategoryName":"Aircraft Types""EntityName":"EntDbAircraftType""KeyProperty":"IATACode""DisplayProperty":"DisplayCodeIATA""VectorName":"VectorAirplane""Types":["FlightSchedule"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"IATACode""Header":"IATA""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0"SortOrder":"Ascending"}{"$type":"TextColumnViewModel""FieldName":"ICAOCode""Header":"ICAO""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"ALTTAB""EntityName":"EntDbAirline""CategoryName":"Airlines""KeyProperty":"DisplayCodeICAO""DisplayProperty":"DisplayCodeICAO""VectorName":"VectorAirline""Types":["FlightSchedule"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"IATACode""Header":"IATA""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0"SortOrder":"Ascending"}{"$type":"TextColumnViewModel""FieldName":"ICAOCode""Header":"ICAO""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"APTTAB""EntityName":"EntDbAirport""CategoryName":"Airports""KeyProperty":"IATACode""DisplayProperty":"DisplayCodeIATA""VectorName":"VectorAirport""Types":["FlightSchedule"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"IATACode""Header":"IATA""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0"SortOrder":"Ascending"}{"$type":"TextColumnViewModel""FieldName":"ICAOCode""Header":"ICAO""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"STYTAB""EntityName":"EntDbServiceType""CategoryName":"Service Types""KeyProperty":"Code""DisplayProperty":"Code""VectorName":"VectorFreighterMail""Types":["FlightSchedule"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"Code""Header":"Code""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0"SortOrder":"Ascending"}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"PSTTAB""EntityName":"EntDbParkingStand""CategoryName":"Aircraft Stands""KeyProperty":"Name""DisplayProperty":"Name""Columns":[{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":100.0"SortOrder":"Ascending"}]}{"CategoryCode":"GATTAB""EntityName":"EntDbGate""CategoryName":"Gates""KeyProperty":"GateName""DisplayProperty":"GateName""Columns":[{"$type":"TextColumnViewModel""FieldName":"GateName""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":100.0"SortOrder":"Ascending"}]}{"CategoryCode":"CICTAB""EntityName":"EntDbCheckInCounter""CategoryName":"Check-In Counters""KeyProperty":"Name""DisplayProperty":"Name""Columns":[{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":100.0"SortOrder":"Ascending"}{"$type":"TextColumnViewModel""FieldName":"Terminal""Header":"Terminal""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":70.0}]}{"CategoryCode":"BLTTAB""EntityName":"EntDbBaggageBelt""CategoryName":"Baggage Belts""KeyProperty":"Name""DisplayProperty":"Name""Columns":[{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":100.0"SortOrder":"Ascending"}]}]', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'AIRLINES', 'DISPLAY_CODE', 'ICAO', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'AIRCRAFT_TYPES', 'MANDATORY_CODE', 'IATA', '2014081300000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'AIRPORTS', 'MANDATORY_CODE', 'IATA', '2014081300000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'CUSTOM_LABEL', 'PARKING_STAND', 'StringLibrary.AIRCRAFT_STAND', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'CUSTOM_LABEL', 'PARKING_STANDS', 'StringLibrary.AIRCRAFT_STANDS', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'CUSTOM_LABEL', 'CHUTES', 'StringLibrary.LATERALS', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'DATA', 'DISABLE_CLIENT_CHAR_CONVERSION', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'DATA', 'MAINTAIN_CHAR_COMPATIBILITY', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GDVV', 'GLOBAL', 'MQ_INFO', '{"topic": "/topic/BC_FROM_UFIS","address": "ws://192.168.1.131:61614/stomp","username": "jceda","password": "jceda22"}', '20140901094100', 'PHYOE', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GDVA', 'GLOBAL', 'WS_URL', 'http://192.168.1.131:8080/KernelCommunication-web/rest/processCommand', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'GLOBAL', 'SHOW_FLNOTOCSGN', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'GLOBAL', 'ALIAS_SERIALIZATION_ENTITIES', 'EntMessage;EntDbMessageBox;EntDbMessageChannel;EntDbMessageRecipient;EntDbMessageSender;EntDbJourneyMatrix', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'SETTINGS', 'SHOW_BAG_DELIVERY_DURATION', 'Y', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'STATUS_RULE', 'REFERENCE_TYPES', '[{"Code":"AFT""Name":"Flight"}{"Code":"CCAB""Name":"Cki-Start"}{"Code":"CCAE""Name":"Cki-End"}{"Code":"JOBI""Name":"Job inf."}{"Code":"JOBC""Name":"Job conf."}{"Code":"JOBF""Name":"Job fin."}]', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'STATUS_RULE', 'FLIGHT_FIELD_ARRIVAL', '[{"Code":"B1BA""Name":"Belt start"}{"Code":"B1EA""Name":"Belt end"}{"Code":"GA1X""Name":"A-Gate open"}{"Code":"GA1Y""Name":"A-Gate closed"}{"Code":"ONBL""Name":"Onblock"}]', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'GLOBAL', 'ALLOCATION_ENTRY', '[{"Code":"BAGGAT""Description":"Baggage belts to gate""AllocationType":2"ChildField":"BLT.URNO""ParentField":"GAT.URNO"}{"Code":"GATPOS""Description":"Gates to positions""AllocationType":2"ChildField":"GAT.URNO""ParentField":"PST.URNO"}{"Code":"WROGAT""Description":"Lounges to gate""AllocationType":2"ChildField":"WRO.URNO""ParentField":"GAT.URNO"}{"Code":"EXTBAG""Description":"Exits to baggage belts""AllocationType":2"ChildField":"EXT.URNO""ParentField":"BLT.URNO"}{"Code":"BLTTRB""Description":"Baggage belts to transit belts""AllocationType":2"ChildField":"BLT.URNO""ParentField":"TRB.URNO"}{"Code":"GATGAT""Description":"Gate to gate""AllocationType":2"ChildField":"GAT.URNO""ParentField":"GAT.URNO"}]', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'GLOBAL', 'SINGLE_PROFILE_APPS', 'CSA;GOCM', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'SETTINGS', 'SHOW_PST_JOURNEY_TIME', 'Y', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'GLOBAL', 'ENC_BY', 'SERVER', '20131008000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'GLOBAL', 'ENC_METHOD', 'SHA-256', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'STATUS_RULE', 'FLIGHT_FIELD_DEPARTURE', '[{"Code":"GD1X""Name":"D-Gate open"}{"Code":"GD1Y""Name":"D-Gate closed"}{"Code":"AIRB""Name":"Airborne"}{"Code":"OFBL""Name":"Offblock"}{"Code":"FLTO""Name":"Flt open"}{"Code":"FLTC""Name":"Flt finalized"}{"Code":"BOAO""Name":"Board. start"}{"Code":"FCA1""Name":"Last Board.Call 1"}{"Code":"FCA2""Name":"Last Board.Call 2"}]', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('AIRPORT_MAP', 'AIRPORT_VIEW', 'CENTER_POINT', '43.676667;-79.630556', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('AIRPORT_MAP', 'AIRPORT_VIEW', 'ZOOM_LEVEL', '15', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'PARKING_STANDS', 'SHOW_REMOTE_DEICING_PAD', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('AIRPORT_MAP', 'AIRPORT_VIEW', 'MAP_URL', 'http://tile.openstreetmap.org', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'GLOBAL', 'SHOW_TRANSIT_BELTS', 'Y', '20131009000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'GLOBAL', 'SHOW_CHUTE', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'GLOBAL', 'SHOW_STEV', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'GLOBAL', 'USE_STATUS_MANAGER', 'Y', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'GLOBAL', 'STEV_LABEL', 'Terminal;T', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'GLOBAL', 'CHUTE_DISPLAY', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'CANCELLED_FLIGHT', 'REASON_FOR_CXX', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'FLIGHT_BY_DOMESTIC_INTERNATIONAL', 'FLIGHTS_ACCORDING_FLTI', 'S/D/R,Schengen,I/M,Int', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'LOAD_AND_PAX', 'LOAD_COLUMNS', '{"OptionalLoadColumns":[{"Label":"Actual""LoadColumns":[{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["PAX""F"""""]"Header":"F""Name":"F""Width":30.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["PAX""B"""""]"Header":"C""Name":"C""Width":30.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["PAX""E"""""]"Header":"Y""Name":"Y""Width":40.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["PAX""""""I"]"Header":"INF""Name":"INF""Width":30.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":[""""""""]"Header":"TTL""Name":"TTL""Width":40.0"Expression":"F + C + Y + INF"}]}]"LoadColumns":[{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["PAD""T"""""]"Header":"ID""Name":"ID""Width":30.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["PAX""T""R"""]"Header":"TR""Name":"TR""Width":30.0"Expression":null}{"DataSources":["LDM""USR""MVT"]"Keys":["PAX""T""T"""]"Header":"TRF""Name":"TRF""Width":30.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["LOA""B"""""]"Header":"BAG""Name":"BAG""Width":40.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["LOA""C"""""]"Header":"CGO""Name":"CGO""Width":40.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":["LOA""M"""""]"Header":"MAIL""Name":"MAIL""Width":40.0"Expression":null}{"DataSources":["LDM""USR""MVT""KRI"]"Keys":[""""""""]"Header":"PTTL""Name":"PTTL""Width":40.0"Expression":"BAG + CGO + MAIL"}]}', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'LOAD_AND_PAX', 'SHOW_NO_SEATS', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'FLIGHT_CARD', 'ARRIVAL_FIELDS', '[{"Code":"Flight No.""Name":"FullFlightNumber"}{"Code":"Route""Name":"ArrivalRoute"}{"Code":"SIBT""Name":"ScheduledInblockTime"}{"Code":"EIBT""Name":"EstimatedInblockTime"}{"Code":"ALDT""Name":"ActualLandingTime"}{"Code":"AIBT""Name":"ActualInblockTime"}{"Code":"Bay""Name":"PositionOfArrival"}{"Code":"Gate""Name":"GateOfArrival1"}]', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'FLIGHT_CARD', 'DEPARTURE_FIELDS', '[{"Code":"Flight No.""Name":"FullFlightNumber"}{"Code":"Route""Name":"DepartureRoute"}{"Code":"SOBT""Name":"ScheduledOffblockTime"}{"Code":"EOBT""Name":"EstimatedOffblockTime"}{"Code":"AOBT""Name":"ActualOffblockTime"}{"Code":"ATOT""Name":"ActualTakeOffTime"}{"Code":"Bay""Name":"PositionOfDeparture"}{"Code":"Gate""Name":"GateOfDeparture1"}]', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'GLOBAL', 'NIGHT_FLIGHT_SLOT', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'SETTINGS', 'SHOW_RUNWAY_IN_USE', 'Y', '2012031600000 ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GLOBAL', 'GLOBAL', 'DEFAULT_TIME_ZONE', 'UTC', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'CITY', 'CHECK_DUPL_CITY_NAME', 'N', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'GLOBAL', 'SYNC_ARR_AND_DEP', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'BELT_USAGE', 'SHOW_TOTAL_PAX', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'GLOBAL', 'SHOW_BLACK_LIST', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'BELT_USAGE', 'SHOW_BAG_DELIVERY', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FIPS', 'DESKTOP', 'NEW_REPORTS', 'C:\Ufis\Appl\Ufis.Reports.exe', '20131218000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FIPS', 'DESKTOP', 'NEW_REPORTS_PATH', 'C:\Ufis\Appl\Ufis.Reports.exe', '20131218000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FIPS', 'DESKTOP', 'USE_NEW_REPORTS', 'Y', '20131218000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FIPS', 'TOWING', 'TOWINGTIME1', '10', '20131219000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FIPS', 'TOWING', 'TOWINGTIME2', '20', '20131219000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'GLOBAL', 'ALLOW_PUBLISHING_USER_VIEW', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GOCM', 'VIEWS', 'SAVE_GRID_FILTER', 'Y', '              ', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('REPORTS', 'DELAYED_FLIGHT', 'COMBINE_DELAY_CODES', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('RMS', 'FIXED_RESOURCE_GROUP', 'SHOW_COLUMN_LIST', '{"GroupSetting":[{"ColumnName":["IATACode""ICAOCode""Name"]"TableName":"APT"}{"ColumnName":["GateName""GateType""GateCapacity"]"TableName":"GAT"}]}', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('MDM', 'GATE', 'USE_SWING_GATE', 'Y', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, HOPO)
 Values
   ('ACTIVEDIRECTORY', 'LDAP_SRV_IP1', '-', '192.168.1.93:389', '20130928000000', 'UFIS$ADMIN', '20130928000000', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, HOPO)
 Values
   ('ACTIVEDIRECTORY', 'LDAP_SRV_IP2', '-', '192.168.1.93:389', '20130928000000', 'UFIS$ADMIN', '20130928000000', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, HOPO)
 Values
   ('ACTIVEDIRECTORY', 'LDAP_TYPE', 'LDAP', 'X', '20130928000000', 'UFIS$ADMIN', '20130928000000', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('FIPS', 'CHECKIN', 'HIDECHECKIN', 'N', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'ARRIVAL', 'LAYOUT_01', '{"grps":[{"hdr":"Passenger","ico":"person_group","grds":[{"cols":[{"hdr":"First"},{"hdr":"Business"},{"hdr":"Economy"},{"hdr":"+Economy 2"},{"hdr":"Jump Seat"},{"hdr":"Infant"},{"hdr":"Total"}],"rows":[{"lbl":"Config","typ":1,"src":["LDM","AIM"]},{"lbl":"Booked","typ":1,"src":["PFI","AIM"]},{"lbl":"Avail"},{"lbl":"Other","typ":1},{},{"lbl":"Disem","typ":1},{},{"lbl":"Transit","typ":1},{},{"lbl":"Onboard"},{"lbl":"On Grid"},{"lbl":"Joining","typ":1},{},{"lbl":"Total Onboard"}],"cells":[{"pos":"A1","keys":["PCF","F"],"src":["USR"]},{"pos":"B1","keys":["PCF","B"]},{"pos":"C1","keys":["PCF","E"]},{"pos":"D1","keys":["PCF",""]},{"pos":"E1","typ":5},{"pos":"F1","typ":5},{"pos":"G1","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A2","keys":["PXB","F"]},{"pos":"B2","keys":["PXB","B"]},{"pos":"C2","keys":["PXB","E"]},{"pos":"D2","keys":["PXB",""]},{"pos":"E2","typ":5},{"pos":"F2","typ":5},{"pos":"G2","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A3","typ":3,"exp":"1+2-5|4"},{"pos":"B3","typ":3,"exp":"1+2-5|4"},{"pos":"C3","typ":3,"exp":"1+2-5|4"},{"pos":"D3","typ":3,"exp":"1+2-5|4"},{"pos":"E3","typ":5},{"pos":"F3","typ":5},{"pos":"G3","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"H3","exp":"A+B+C+D"},{"pos":"A4","keys":["","F"],"src":["USR"]},{"pos":"B4","keys":["","B"],"src":["USR"]},{"pos":"C4","keys":["","E"],"src":["USR"]},{"pos":"D4","keys":["",""],"src":["USR"]},{"pos":"E4","typ":5,"src":["USR"]},{"pos":"F4","typ":5,"src":["USR"]},{"pos":"G4","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A5","keys":["PXO","F"]},{"pos":"B5","keys":["PXO","B"]},{"pos":"C5","keys":["PXO","E"]},{"pos":"D5","keys":["PXO","2"]},{"pos":"E5","typ":5},{"pos":"F5","typ":5},{"pos":"G5","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A6","keys":["","F"],"src":["USR"]},{"pos":"B6","keys":["","B"],"src":["USR"]},{"pos":"C6","keys":["","E"],"src":["USR"]},{"pos":"D6","keys":["",""],"src":["USR"]},{"pos":"E6","keys":["",""],"src":["USR"]},{"pos":"F6","keys":["","T","","I"],"src":["USR"]},{"pos":"G6","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A7","keys":["DIS","F"]},{"pos":"B7","keys":["DIS","B"]},{"pos":"C7","keys":["DIS","E"]},{"pos":"D7","keys":["DIS","2"]},{"pos":"E7","keys":["DIS","J"]},{"pos":"F7","keys":["DIS","T","","I"]},{"pos":"G7","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A8","keys":["PAX","F","R"],"src":["USR"]},{"pos":"B8","keys":["PAX","B","R"],"src":["USR"]},{"pos":"C8","keys":["PAX","E","R"],"src":["USR"]},{"pos":"D8","src":["USR"]},{"pos":"E8","src":["USR"]},{"pos":"F8","keys":["PAX","","R","I"],"src":["USR"]},{"pos":"G8","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A9","keys":["PAX","F","R"]},{"pos":"B9","keys":["PAX","B","R"]},{"pos":"C9","keys":["PAX","E","R"]},{"pos":"D9","keys":["PAX","2","R"]},{"pos":"E9","keys":["PAX","J","R"]},{"pos":"F9","keys":["PAX","","R","I"]},{"pos":"G9","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A10","keys":["POB","F"]},{"pos":"B10","keys":["POB","B"]},{"pos":"C10","keys":["POB","E"]},{"pos":"D10","keys":["POB","2"]},{"pos":"E10","keys":["POB","J"]},{"pos":"F10","keys":["POB","","I"]},{"pos":"G10","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A11","keys":["POG","F"]},{"pos":"B11","keys":["POG","B"]},{"pos":"C11","keys":["POG","E"]},{"pos":"D11","keys":["POG","2"]},{"pos":"E11","keys":["POG","J"]},{"pos":"F11","keys":["POG","","","J"]},{"pos":"G11","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A12","typ":1,"keys":["PXJ","F"],"src":["USR"]},{"pos":"B12","typ":1,"keys":["PXJ","B"],"src":["USR"]},{"pos":"C12","typ":1,"keys":["PXJ","E"],"src":["USR"]},{"pos":"D12","typ":1,"src":["USR"]},{"pos":"E12","typ":1,"src":["USR"]},{"pos":', '20120712000000', 'UFIS$ADMIN', '20120924040128', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'ARRIVAL', 'LAYOUT_02', '"F12","typ":1,"keys":["PXJ","T","","I"],"src":["USR"]},{"pos":"G12","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A13","keys":["PXJ","F"]},{"pos":"B13","keys":["PXJ","B"]},{"pos":"C13","keys":["PXJ","E"]},{"pos":"D13","keys":["PXJ","2"]},{"pos":"E13","keys":["PXJ","J"]},{"pos":"F13","keys":["PXJ","T"]},{"pos":"G13","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A14","typ":3,"exp":"9|8+13|12"},{"pos":"B14","typ":3,"exp":"9|8+13|12"},{"pos":"C14","typ":3,"exp":"9|8+13|12"},{"pos":"D14","typ":3,"exp":"9|8+13|12"},{"pos":"E14","typ":3,"exp":"9|8+13|12"},{"pos":"F14","typ":3,"exp":"9|8+13|12"},{"pos":"G14","typ":3,"exp":"A+B+C+D+E+F"}]}]},{"hdr":"UnderLoad","ico":"person","grds":[{"hlbl":true,"cols":[{"hdr":"UnderLoad"}],"rows":[{},{}],"cells":[{"pos":"A1","typ":1,"keys":["LOA","U"],"src":["LDM","AIM"]},{"pos":"A2","keys":["LOA","U"],"src":["LDM","AIM"]}]}]},{"hdr":"Load","ico":"bag","grds":[{"hlbl":true,"cols":[{"hdr":"Mail"},{"hdr":"Cargo"},{"hdr":"Baggage"},{"hdr":"Total"}],"rows":[{},{}],"cells":[{"pos":"A1","typ":1,"keys":["LOA","M"],"src":["LDM","AIM"]},{"pos":"B1","typ":1,"keys":["LOA","C"],"src":["LDM","AIM"]},{"pos":"C1","typ":1,"keys":["LOA","B"],"src":["LDM","AIM"]},{"pos":"D1","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A2","keys":["LOA","M"],"src":["LDM","AIM"]},{"pos":"B2","keys":["BWT","LOA","C"],"src":["LDM","AIM"]},{"pos":"C2","keys":["LOA","B"],"src":["LDM","AIM"]},{"pos":"D2","typ":3,"exp":"A+B+C"},{"pos":"E2","typ":2,"keys":["LOA","B","R"]}]}]}]}', '20120712000000', 'UFIS$ADMIN', '20120924040133', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'ARRIVAL', 'WIDTH', '923', '20120712000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'DEPARTURE', 'HEIGHT', '681', '20120712000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'DEPARTURE', 'INCLUDE_ROTATION', 'TRUE', '20120712000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'ARRIVAL', 'HEIGHT', '660', '20120712000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, HOPO)
 Values
   ('LOAD_EDITOR', 'DEPARTURE', 'LAYOUT_01', '{"grps":[{"hdr":"Passenger","ico":"person_group","grds":[{"cols":[{"hdr":"First"},{"hdr":"Business"},{"hdr":"Economy"},{"hdr":"+Economy 2"},{"hdr":"Jump Seat"},{"hdr":"Infant"},{"hdr":"Total"}],"rows":[{"lbl":"Config","typ":1,"src":["LDM","AIM"]},{"lbl":"Booked","typ":1,"src":["PFI","AIM"]},{"lbl":"Avail"},{"lbl":"Other","typ":1},{},{"lbl":"Disem","typ":1},{},{"lbl":"Transit","typ":1},{},{"lbl":"Onboard"},{"lbl":"On Grid"},{"lbl":"Joining","typ":1},{},{"lbl":"Total Onboard"}],"cells":[{"pos":"A1","keys":["PCF","F"],"src":["USR"]},{"pos":"B1","keys":["PCF","B"]},{"pos":"C1","keys":["PCF","E"]},{"pos":"D1","keys":["PCF",""]},{"pos":"E1","typ":5},{"pos":"F1","typ":5},{"pos":"G1","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A2","keys":["PXB","F"]},{"pos":"B2","keys":["PXB","B"]},{"pos":"C2","keys":["PXB","E"]},{"pos":"D2","keys":["PXB",""]},{"pos":"E2","typ":5},{"pos":"F2","typ":5},{"pos":"G2","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A3","typ":3,"exp":"1+2-5|4"},{"pos":"B3","typ":3,"exp":"1+2-5|4"},{"pos":"C3","typ":3,"exp":"1+2-5|4"},{"pos":"D3","typ":3,"exp":"1+2-5|4"},{"pos":"E3","typ":5},{"pos":"F3","typ":5},{"pos":"G3","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"H3","exp":"A+B+C+D"},{"pos":"A4","keys":["","F"],"src":["USR"]},{"pos":"B4","keys":["","B"],"src":["USR"]},{"pos":"C4","keys":["","E"],"src":["USR"]},{"pos":"D4","keys":["",""],"src":["USR"]},{"pos":"E4","typ":5,"src":["USR"]},{"pos":"F4","typ":5,"src":["USR"]},{"pos":"G4","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A5","keys":["PXO","F"]},{"pos":"B5","keys":["PXO","B"]},{"pos":"C5","keys":["PXO","E"]},{"pos":"D5","keys":["PXO","2"]},{"pos":"E5","typ":5},{"pos":"F5","typ":5},{"pos":"G5","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A6","keys":["","F"],"src":["USR"]},{"pos":"B6","keys":["","B"],"src":["USR"]},{"pos":"C6","keys":["","E"],"src":["USR"]},{"pos":"D6","keys":["",""],"src":["USR"]},{"pos":"E6","keys":["",""],"src":["USR"]},{"pos":"F6","keys":["","T","","I"],"src":["USR"]},{"pos":"G6","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A7","keys":["DIS","F"]},{"pos":"B7","keys":["DIS","B"]},{"pos":"C7","keys":["DIS","E"]},{"pos":"D7","keys":["DIS","2"]},{"pos":"E7","keys":["DIS","J"]},{"pos":"F7","keys":["DIS","T","","I"]},{"pos":"G7","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A8","keys":["PAX","F","R"],"src":["USR"]},{"pos":"B8","keys":["PAX","B","R"],"src":["USR"]},{"pos":"C8","keys":["PAX","E","R"],"src":["USR"]},{"pos":"D8","src":["USR"]},{"pos":"E8","src":["USR"]},{"pos":"F8","keys":["PAX","","R","I"],"src":["USR"]},{"pos":"G8","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A9","keys":["PAX","F","R"]},{"pos":"B9","keys":["PAX","B","R"]},{"pos":"C9","keys":["PAX","E","R"]},{"pos":"D9","keys":["PAX","2","R"]},{"pos":"E9","keys":["PAX","J","R"]},{"pos":"F9","keys":["PAX","","R","I"]},{"pos":"G9","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A10","keys":["POB","F"]},{"pos":"B10","keys":["POB","B"]},{"pos":"C10","keys":["POB","E"]},{"pos":"D10","keys":["POB","2"]},{"pos":"E10","keys":["POB","J"]},{"pos":"F10","keys":["POB","","I"]},{"pos":"G10","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A11","keys":["POG","F"]},{"pos":"B11","keys":["POG","B"]},{"pos":"C11","keys":["POG","E"]},{"pos":"D11","keys":["POG","2"]},{"pos":"E11","keys":["POG","J"]},{"pos":"F11","keys":["POG","","","J"]},{"pos":"G11","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A12","typ":1,"keys":["PXJ","F"],"src":["USR"]},{"pos":"B12","typ":1,"keys":["PXJ","B"],"src":["USR"]},{"pos":"C12","typ":1,"keys":["PXJ","E"],"src":["USR"]},{"pos":"D12","typ":1,"src":["USR"]},{"pos":"E12","typ":1,"src":["USR"]},{"pos":', '20120712000000', 'UFIS$ADMIN', '20120924040300', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'DEPARTURE', 'LAYOUT_02', '"F12","typ":1,"keys":["PXJ","T","","I"],"src":["USR"]},{"pos":"G12","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A13","keys":["PXJ","F"]},{"pos":"B13","keys":["PXJ","B"]},{"pos":"C13","keys":["PXJ","E"]},{"pos":"D13","keys":["PXJ","2"]},{"pos":"E13","keys":["PXJ","J"]},{"pos":"F13","keys":["PXJ","T"]},{"pos":"G13","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A14","typ":3,"exp":"9|8+13|12"},{"pos":"B14","typ":3,"exp":"9|8+13|12"},{"pos":"C14","typ":3,"exp":"9|8+13|12"},{"pos":"D14","typ":3,"exp":"9|8+13|12"},{"pos":"E14","typ":3,"exp":"9|8+13|12"},{"pos":"F14","typ":3,"exp":"9|8+13|12"},{"pos":"G14","typ":3,"exp":"A+B+C+D+E+F"}]}]},{"hdr":"UnderLoad","ico":"person","grds":[{"hlbl":true,"cols":[{"hdr":"UnderLoad"}],"rows":[{},{}],"cells":[{"pos":"A1","typ":1,"keys":["LOA","U"],"src":["LDM","AIM"]},{"pos":"A2","keys":["LOA","U"],"src":["LDM","AIM"]}]}]},{"hdr":"Load","ico":"bag","grds":[{"hlbl":true,"cols":[{"hdr":"Mail"},{"hdr":"Cargo"},{"hdr":"Baggage"},{"hdr":"Total"}],"rows":[{},{}],"cells":[{"pos":"A1","typ":1,"keys":["LOA","M"],"src":["LDM","AIM"]},{"pos":"B1","typ":1,"keys":["LOA","C"],"src":["LDM","AIM"]},{"pos":"C1","typ":1,"keys":["LOA","B"],"src":["LDM","AIM"]},{"pos":"D1","typ":3,"exp":"A+B+C+D+E+F"},{"pos":"A2","keys":["LOA","M"],"src":["LDM","AIM"]},{"pos":"B2","keys":["BWT","LOA","C"],"src":["LDM","AIM"]},{"pos":"C2","keys":["LOA","B"],"src":["LDM","AIM"]},{"pos":"D2","typ":3,"exp":"A+B+C"},{"pos":"E2","typ":2,"keys":["LOA","B","R"]}]}]}]}', '20120712000000', 'UFIS$ADMIN', '20120924040306', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('LOAD_EDITOR', 'DEPARTURE', 'WIDTH', '1169', '20120712000000', 'UFIS$ADMIN', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GROUPS', 'GLOBAL', 'ITEM_1', '[{"CategoryCode":"GAT_GRP""CategoryName":"Gates""EntityName":"EntDbGate""VectorName":"VectorGate""KeyProperty":"GateName""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"GateName""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}]}{"CategoryCode":"PST_GRP""CategoryName":"Aircraft Stands""EntityName":"EntDbParkingStand""VectorName":"VectorParkingSign""KeyProperty":"Name""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}]}{"CategoryCode":"CHU_GRP""CategoryName":"Laterals""EntityName":"EntDbChute""VectorName":"VectorChute""KeyProperty":"ChuteName""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"ChuteName""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}]}{"CategoryCode":"ACT_GRP""CategoryName":"Aircraft Types""EntityName":"EntDbAircraftType""VectorName":"VectorAirplane""KeyProperty":"IATACode""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"IATACode""Header":"IATA""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"ICAOCode""Header":"ICAO""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"ALT_GRP""CategoryName":"Airlines""EntityName":"EntDbAirline""VectorName":"VectorAirline""KeyProperty":"ICAOCode""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"IATACode""Header":"IATA""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"ICAOCode""Header":"ICAO""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"APT_GRP""CategoryName":"Airports""EntityName":"EntDbAirport""VectorName":"VectorAirport""KeyProperty":"IATACode""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"IATACode""Header":"IATA""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"ICAOCode""Header":"ICAO""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"HAG_GRP""CategoryName":"Handling Agents""EntityName":"EntDbHandlingAgent""VectorName":"VectorAgent""KeyProperty":"Code""Columns":[{"$type":"TextColumnViewModel""FieldName":"Code""Header":"Code""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"BLT_GRP""CategoryName":"Baggage Belts""EntityName":"EntDbBaggageBelt""VectorName":"VectorBaggageBelt""KeyProperty":"Name""Types":["FRMS_RULE"]"Columns":[{"$type":"TextColumnViewModel""FieldName":"Name""Header":"Name""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"Terminal""Header":"Terminal""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}{"CategoryCode":"FSI_GRP""CategoryName":"Flight Sector IDs""EntityName":"EntDbFlightSectorID""VectorName":"VectorFlightRoute""KeyProperty":"SectorIDCode""Columns":[{"$type":"TextColumnViewModel""FieldName":"SectorIDCode""Header":"Code""AllowEditing":1"HorizontalHeaderContentAlignment":1"Width":80.0}{"$type":"TextColumnViewModel""FieldName":"SectorIDName""Header":"Name""Allow', '              ', ' ', '              ', ' ', 'HAJ');
Insert into GCFTAB
   (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO)
 Values
   ('GROUPS', 'GLOBAL', 'ITEM_2', 'Editing":1"HorizontalHeaderContentAlignment":1"Width":200.0}]}]', '              ', ' ', '              ', ' ', 'HAJ');
COMMIT;
