SET DEFINE OFF;
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (601, 'DSAR      ', 'Flight              ', 'A_Flight_                     ', 'Flight Number                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (602, 'DSAR      ', 'C/S                 ', 'A_C/S_                        ', 'Call Sign                                         ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (603, 'DSAR      ', 'ORG                 ', 'A_ORG_                        ', 'Origin Airport                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (604, 'DSAR      ', 'VIA                 ', 'A_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (605, 'DSAR      ', '                    ', 'A_VIAN_                       ', 'Number of Via Airport                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (606, 'DSAR      ', 'A                   ', 'A_A_                          ', 'Arrival Type (IFR/VFR)                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (607, 'DSAR      ', 'Na                  ', 'A_Na_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (608, 'DSAR      ', 'SIBT                ', 'A_SIBT_                       ', 'Schedule In-Block Time                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (609, 'DSAR      ', 'ELDT                ', 'A_ELDT_                       ', 'Estimated Landing Time                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (610, 'DSAR      ', 'TLDT                ', 'A_TLDT_                       ', 'Target Landing Time                               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (611, 'DSAR      ', 'TMO                 ', 'A_TMO_                        ', 'Twenty Mile                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (612, 'DSAR      ', 'ALDT                ', 'A_ALDT_                       ', 'Actual Landing Time                               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (613, 'DSAR      ', 'RWY                 ', 'A_RWY_                        ', 'Runway                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (614, 'DSAR      ', 'EIBT                ', 'A_EIBT_                       ', 'Estimated In-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (615, 'DSAR      ', 'AIBT                ', 'A_AIBT_                       ', 'Actual In-Block Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (616, 'DSAR      ', 'POS                 ', 'A_POS_                        ', 'Position                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (617, 'DSAR      ', 'Gate                ', 'A_Gate_                       ', 'Gate                                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (618, 'DSAR      ', 'Belt                ', 'A_Belt_                       ', 'Belt                                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (619, 'DSAR      ', 'REG                 ', 'A_REG_                        ', 'Registration                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (620, 'DSAR      ', 'A/C                 ', 'A_A/C_                        ', 'Aircraft Type                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (621, 'DSAR      ', 'Date SIBT           ', 'A_Date SIBT_                  ', 'Date of SIBT                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (622, 'DSAR      ', 'I                   ', 'A_I_                          ', 'Remark Flag                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (623, 'DSAR      ', 'V                   ', 'A_V_                          ', 'Number of VIP                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (624, 'DSAR      ', 'REM                 ', 'A_REM_                        ', 'Remark                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (625, 'DSAR      ', 'Rem. 1 ARR Daily    ', 'A_Rem. 1 ARR Daily_           ', 'Arrival Remark 1 for Daily Schedule               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (626, 'DSAR      ', 'Rem. ARR Seasonal   ', 'A_Rem. ARR Seasonal_          ', 'Arrival Remark for Seasonal Schedule              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (101, 'DSAG      ', 'Flight              ', 'A_Flight_                     ', 'Flight Number                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (102, 'DSAG      ', 'C/S                 ', 'A_C/S_                        ', 'Call Sign                                         ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (103, 'DSAG      ', 'ORG                 ', 'A_ORG_                        ', 'Origin Airport                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (104, 'DSAG      ', 'VIA                 ', 'A_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (105, 'DSAG      ', '                    ', 'A_VIAN_                       ', 'Number of Via Airport                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (106, 'DSAG      ', 'A                   ', 'A_A_                          ', 'Arrival Type (IFR/VFR)                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (107, 'DSAG      ', 'Na                  ', 'A_Na_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (108, 'DSAG      ', 'SIBT                ', 'A_SIBT_                       ', 'Schedule In-Block Time                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (109, 'DSAG      ', 'ELDT                ', 'A_ELDT_                       ', 'Estimated Landing Time                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (110, 'DSAG      ', 'TLDT                ', 'A_TLDT_                       ', 'Target Landing Time                               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (111, 'DSAG      ', 'TMO                 ', 'A_TMO_                        ', 'Twenty Mile                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (112, 'DSAG      ', 'ALDT                ', 'A_ALDT_                       ', 'Actual Landing Time                               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (113, 'DSAG      ', 'RWY                 ', 'A_RWY_                        ', 'Runway                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (114, 'DSAG      ', 'EIBT                ', 'A_EIBT_                       ', 'Estimated In-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (115, 'DSAG      ', 'AIBT                ', 'A_AIBT_                       ', 'Actual In-Block Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (116, 'DSAG      ', 'POS                 ', 'A_POS_                        ', 'Position                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (117, 'DSAG      ', 'Gate                ', 'A_Gate_                       ', 'Gate                                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (118, 'DSAG      ', 'Belt                ', 'A_Belt_                       ', 'Belt                                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (119, 'DSAG      ', 'REG                 ', 'A_REG_                        ', 'Registration                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (120, 'DSAG      ', 'A/C                 ', 'A_A/C_                        ', 'Aircraft Type                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (121, 'DSAG      ', 'Date ALDT           ', 'A_Date ALDT_                  ', 'Date of ALDT                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (122, 'DSAG      ', 'I                   ', 'A_I_                          ', 'Remark Flag                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (123, 'DSAG      ', 'V                   ', 'A_V_                          ', 'Number of VIP                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (124, 'DSAG      ', 'REM                 ', 'A_REM_                        ', 'Remark                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (125, 'DSAG      ', 'Rem. 1 ARR Daily    ', 'A_Rem. 1 ARR Daily_           ', 'Arrival Remark 1 for Daily Schedule               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (126, 'DSAG      ', 'Rem. ARR Seasonal   ', 'A_Rem. ARR Seasonal_          ', 'Arrival Remark for Seasonal Schedule              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (201, 'DSDG      ', 'Flight              ', 'D_Flight_                     ', 'Flight Number                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (202, 'DSDG      ', 'C/S                 ', 'D_C/S_                        ', 'Call Sign                                         ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (203, 'DSDG      ', 'DES                 ', 'D_DES_                        ', 'Dstination Airport                                ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (204, 'DSDG      ', 'VIA                 ', 'D_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (205, 'DSDG      ', '                    ', 'D_VIAN_                       ', 'Number of Via Airport                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (206, 'DSDG      ', 'A                   ', 'D_A_                          ', 'Arrival Type (IFR/VFR)                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (207, 'DSDG      ', 'Na                  ', 'D_Na_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (208, 'DSDG      ', 'SOBT                ', 'D_SOBT_                       ', 'Schedule Off-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (209, 'DSDG      ', 'EOBT                ', 'D_EOBT_                       ', 'Estimated Off-Block Time                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (210, 'DSDG      ', 'TOBT                ', 'D_TOBT_                       ', 'Target Off-Block Time                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (211, 'DSDG      ', 'TSAT                ', 'D_TSAT_                       ', 'Target Start Up Approval Time                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (212, 'DSDG      ', 'ASRT                ', 'D_ASRT_                       ', 'Actual Start Up Request Time                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (213, 'DSDG      ', 'ASAT                ', 'D_ASAT_                       ', 'Actual Start Up Approval Time                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (214, 'DSDG      ', 'AOBT                ', 'D_AOBT_                       ', 'Actual Off-Block Time                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (215, 'DSDG      ', 'CTOT                ', 'D_CTOT_                       ', 'Calculated Take Off Time                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (216, 'DSDG      ', 'RWY                 ', 'D_RWY_                        ', 'Runway                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (217, 'DSDG      ', 'TTOT                ', 'D_TTOT_                       ', 'Target Take Off Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (218, 'DSDG      ', 'ATOT                ', 'D_ATOT_                       ', 'Actual Take Off Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (219, 'DSDG      ', 'POS                 ', 'D_POS_                        ', 'Position                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (220, 'DSDG      ', 'Gate1               ', 'D_Gate1_                      ', 'Gate 1                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (221, 'DSDG      ', 'Gate2               ', 'D_Gate2_                      ', 'Gate 2                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (222, 'DSDG      ', 'Cki                 ', 'D_Cki_                        ', 'Check-In Counter                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (223, 'DSDG      ', 'REG                 ', 'D_REG_                        ', 'Registration                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (224, 'DSDG      ', 'A/C                 ', 'D_A/C_                        ', 'Aircraft Type                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (225, 'DSDG      ', 'Date SOBT           ', 'D_Date SOBT_                  ', 'Date of SOBT                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (226, 'DSDG      ', 'I                   ', 'D_I_                          ', 'Remark Flag                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (227, 'DSDG      ', 'V                   ', 'D_V_                          ', 'Number of VIP                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (228, 'DSDG      ', 'REM                 ', 'D_REM_                        ', 'Remark                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (229, 'DSDG      ', 'Rem. 1 DEP Daily    ', 'D_Rem. 1 DEP Daily_           ', 'Departure Remark 1 for Daily Schedule             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (230, 'DSDG      ', 'Rem. DEP Seasonal   ', 'D_Rem. DEP Seasonal_          ', 'Departure Remark for Seasonal Schedule            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (301, 'DSDE      ', 'Flight              ', 'D_Flight_                     ', 'Flight Number                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (302, 'DSDE      ', 'C/S                 ', 'D_C/S_                        ', 'Call Sign                                         ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (303, 'DSDE      ', 'DES                 ', 'D_DES_                        ', 'Dstination Airport                                ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (304, 'DSDE      ', 'VIA                 ', 'D_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (305, 'DSDE      ', '                    ', 'D_VIAN_                       ', 'Number of Via Airport                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (306, 'DSDE      ', 'A                   ', 'D_A_                          ', 'Arrival Type (IFR/VFR)                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (307, 'DSDE      ', 'Na                  ', 'D_Na_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (308, 'DSDE      ', 'SOBT                ', 'D_SOBT_                       ', 'Schedule Off-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (309, 'DSDE      ', 'EOBT                ', 'D_EOBT_                       ', 'Estimated Off-Block Time                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (310, 'DSDE      ', 'TOBT                ', 'D_TOBT_                       ', 'Target Off-Block Time                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (311, 'DSDE      ', 'TSAT                ', 'D_TSAT_                       ', 'Target Start Up Approval Time                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (312, 'DSDE      ', 'ASRT                ', 'D_ASRT_                       ', 'Actual Start Up Request Time                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (313, 'DSDE      ', 'ASAT                ', 'D_ASAT_                       ', 'Actual Start Up Approval Time                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (314, 'DSDE      ', 'AOBT                ', 'D_AOBT_                       ', 'Actual Off-Block Time                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (315, 'DSDE      ', 'CTOT                ', 'D_CTOT_                       ', 'Calculated Take Off Time                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (316, 'DSDE      ', 'RWY                 ', 'D_RWY_                        ', 'Runway                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (317, 'DSDE      ', 'TTOT                ', 'D_TTOT_                       ', 'Target Take Off Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (318, 'DSDE      ', 'ATOT                ', 'D_ATOT_                       ', 'Actual Take Off Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (319, 'DSDE      ', 'POS                 ', 'D_POS_                        ', 'Position                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (320, 'DSDE      ', 'Gate1               ', 'D_Gate1_                      ', 'Gate 1                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (321, 'DSDE      ', 'Gate2               ', 'D_Gate2_                      ', 'Gate 2                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (322, 'DSDE      ', 'Cki                 ', 'D_Cki_                        ', 'Check-In Counter                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (323, 'DSDE      ', 'REG                 ', 'D_REG_                        ', 'Registration                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (324, 'DSDE      ', 'A/C                 ', 'D_A/C_                        ', 'Aircraft Type                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (325, 'DSDE      ', 'Date ATOT           ', 'D_Date ATOT_                  ', 'Date of ATOT                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (326, 'DSDE      ', 'I                   ', 'D_I_                          ', 'Remark Flag                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (327, 'DSDE      ', 'V                   ', 'D_V_                          ', 'Number of VIP                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (328, 'DSDE      ', 'REM                 ', 'D_REM_                        ', 'Remark                                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (329, 'DSDE      ', 'Rem. 1 DEP Daily    ', 'D_Rem. 1 DEP Daily_           ', 'Departure Remark 1 for Daily Schedule             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (330, 'DSDE      ', 'Rem. DEP Seasonal   ', 'D_Rem. DEP Seasonal_          ', 'Departure Remark for Seasonal Schedule            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (401, 'FSCH      ', 'Arrival             ', 'A_Arrival_                    ', 'Arrival Flight Number                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (402, 'FSCH      ', 'ID                  ', 'A_ID_                         ', 'Inter/Domestic                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (403, 'FSCH      ', 'J                   ', 'A_J_                          ', 'Number of Codeshared Flight                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (404, 'FSCH      ', 'Date                ', 'A_Date_                       ', 'Schedule In-Block Date                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (405, 'FSCH      ', 'SIBT                ', 'A_SIBT_                       ', 'Schedule In-Block Time                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (406, 'FSCH      ', 'A                   ', 'A_A_                          ', 'Day of Arrival                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (407, 'FSCH      ', 'Na                  ', 'A_Na_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (408, 'FSCH      ', 'K                   ', 'A_K_                          ', 'Key Code                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (409, 'FSCH      ', 'S                   ', 'A_S_                          ', 'Service Type                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (410, 'FSCH      ', 'ORG                 ', 'A_ORG_                        ', 'Origin Airport                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (411, 'FSCH      ', 'VIA                 ', 'A_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (412, 'FSCH      ', 'POS                 ', 'A_POS_                        ', 'Arrival Position                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (413, 'FSCH      ', 'Gate1               ', 'A_Gate1_                      ', 'Arrival Gate 1                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (414, 'FSCH      ', 'Gate2               ', 'A_Gate2_                      ', 'Arrival Gate 2                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (415, 'FSCH      ', 'Belt                ', 'A_Belt_                       ', 'Baggage Belt                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (416, 'FSCH      ', 'I                   ', 'A_I_                          ', '?                                                 ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (417, 'FSCH      ', 'Type                ', 'A_Type_                       ', 'Aircraft Type                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (418, 'FSCH      ', 'Registration        ', 'A_Registration_               ', 'Aircraft Registration                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (419, 'FSCH      ', 'Departure           ', 'D_Departure_                  ', 'Departure Flight Number                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (420, 'FSCH      ', 'ID                  ', 'D_ID_                         ', 'Inter/Domestic                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (421, 'FSCH      ', 'J                   ', 'D_J_                          ', 'Number of Codeshared Flight                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (422, 'FSCH      ', 'Date                ', 'D_Date_                       ', 'Schedule Off-Block Date                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (423, 'FSCH      ', 'SOBT                ', 'D_SOBT_                       ', 'Schedule Off-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (424, 'FSCH      ', 'A                   ', 'D_A_                          ', 'Day of Departure                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (425, 'FSCH      ', 'Na                  ', 'D_Na_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (426, 'FSCH      ', 'K                   ', 'D_K_                          ', 'Key Code                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (427, 'FSCH      ', 'S                   ', 'D_S_                          ', 'Service Type                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (428, 'FSCH      ', 'VIA                 ', 'D_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (429, 'FSCH      ', 'DES                 ', 'D_DES_                        ', 'Destination Airport                               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (430, 'FSCH      ', 'POS                 ', 'D_POS_                        ', 'Departure Position                                ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (431, 'FSCH      ', 'Gate1               ', 'D_Gate1_                      ', 'Departure Gate 1                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (432, 'FSCH      ', 'Gate2               ', 'D_Gate2_                      ', 'Departure Gate 2                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (433, 'FSCH      ', 'Cki                 ', 'D_Cki_                        ', 'Check-In Counter                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (434, 'FSCH      ', 'I                   ', 'D_I_                          ', '?                                                 ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (435, 'FSCH      ', 'Rem. ARR Seasonal   ', 'A_Rem. ARR Seasonal_          ', 'Arrival Remark for Seasonal Schedule              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (436, 'FSCH      ', 'Rem. 1 ARR Daily    ', 'A_Rem. 1 ARR Daily_           ', 'Arrival Remark 1 for Daily Schedule               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (437, 'FSCH      ', 'Rem. DEP Seasonal   ', 'D_Rem. DEP Seasonal_          ', 'Departure Remark for Seasonal Schedule            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (438, 'FSCH      ', 'Rem. 1 DEP Daily    ', 'D_Rem. 1 DEP Daily_           ', 'Departure Remark 1 for Daily Schedule             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (501, 'DARO      ', 'FLNR                ', 'A_FLNR_                       ', 'Arrival Flight Number                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (502, 'DARO      ', 'DT                  ', 'A_DT_                         ', 'Arrival Date                                      ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (503, 'DARO      ', 'ID                  ', 'A_ID_                         ', 'Inter/Domestic                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (504, 'DARO      ', 'NA                  ', 'A_NA_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (505, 'DARO      ', 'ORG                 ', 'A_ORG_                        ', 'Origin Airport                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (506, 'DARO      ', 'K                   ', 'A_K_                          ', 'Key Code                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (507, 'DARO      ', 'VIA                 ', 'A_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (508, 'DARO      ', 'ATOT                ', 'A_ATOT_                       ', 'Actual Take-Off Time                              ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (509, 'DARO      ', 'SIBT                ', 'A_SIBT_                       ', 'Schedule In-Block Time                            ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (510, 'DARO      ', 'EIBT                ', 'A_EIBT_                       ', 'Estimated In-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (511, 'DARO      ', 'TMO                 ', 'A_TMO_                        ', 'Twenty Mile                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (512, 'DARO      ', 'ALDT/ON-BLOCK       ', 'A_ALDT/ON-BLOCK_              ', 'Actual Landed Time/In-Block Time                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (513, 'DARO      ', 'POS A/D             ', 'A_POS A/D_                    ', 'Arrival Position                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (514, 'DARO      ', 'A/C                 ', 'A_A/C_                        ', 'Aircraft Type                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (515, 'DARO      ', 'REG                 ', 'A_REG_                        ', 'Aircraft Registration                             ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (516, 'DARO      ', 'FLNR                ', 'D_FLNR_                       ', 'Departure Flight Number                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (517, 'DARO      ', 'DT                  ', 'D_DT_                         ', 'Departure Date                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (518, 'DARO      ', 'ID                  ', 'D_ID_                         ', 'Inter/Domestic                                    ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (519, 'DARO      ', 'NA                  ', 'D_NA_                         ', 'Flight Nature                                     ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (520, 'DARO      ', 'VIA                 ', 'D_VIA_                        ', 'Via Airport                                       ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (521, 'DARO      ', 'DES                 ', 'D_DES_                        ', 'Destination Airport                               ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (522, 'DARO      ', 'K                   ', 'D_K_                          ', 'Key Code                                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (523, 'DARO      ', 'SOBT                ', 'D_SOBT_                       ', 'Schedule Off-Block Time                           ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (524, 'DARO      ', 'EOBT                ', 'D_EOBT_                       ', 'Estimated Off-Block Time                          ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (525, 'DARO      ', 'AOBT/ATOT           ', 'D_AOBT/ATOT_                  ', 'Actual Off-Blick Time/Actual Take-Off Time        ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (526, 'DARO      ', 'CKI                 ', 'D_CKI_                        ', 'Check-In Counter                                  ');
Insert into COLTAB
   (SEQN, SCCD, CONM, COID, CODS)
 Values
   (527, 'DARO      ', 'GATE                ', 'D_GATE_                       ', 'Departure Gate                                    ');
COMMIT;
