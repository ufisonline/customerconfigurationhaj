CREATE TABLE GSCTAB
   (     URNO VARCHAR(10 BYTE) DEFAULT ' ',
GNAM VARCHAR(10 BYTE) DEFAULT ' ',
FLTI VARCHAR2(64 BYTE) DEFAULT ' ',
FLTO VARCHAR2(64 BYTE) DEFAULT ' ',
USEC CHAR(32) DEFAULT ' ' ,
USEU CHAR(32) DEFAULT ' ' ,
CDAT CHAR(14) DEFAULT ' ' ,
LSTU CHAR(14) DEFAULT ' ' ,
HOPO CHAR(3) DEFAULT 'HAJ'
)
TABLESPACE "CEDA02" ;

commit;

COMMENT ON COLUMN GSCTAB.URNO IS 'ID';
COMMENT ON COLUMN GSCTAB.GNAM IS 'Gate Name (GAT.GNAM)';
COMMENT ON COLUMN GSCTAB.FLTI IS 'Possible Incoming (Arrival) Flight Sector IDs at particular time frame (without swing operation)';
COMMENT ON COLUMN GSCTAB.FLTO IS 'Possible Outgoing (Departure) Flight Sector IDs at particular time frame (without swing operation)';

commit;

ALTER TABLE GSCTAB ADD PRIMARY KEY (URNO);

commit;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'HAJ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'GSCTAB'
and a.column_name IN
('URNO','GNAM','FLTI','FLTO','USEC', 'USEU', 'CDAT', 'LSTU', 'HOPO');

commit;