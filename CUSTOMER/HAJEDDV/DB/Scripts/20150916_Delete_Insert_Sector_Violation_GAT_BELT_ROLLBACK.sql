-- Mark old sector violation rule rec_status with 'XX'
Update RULE_DETAIL SET REC_STATUS = 'XX' WHERE ID in (
'0a306f3a-2e9f-4fc8-b295-379085eb69a8',
'64f08f25-4bf3-47dd-8135-94a9ab42ed5f',
'94ca56f4-08a5-402d-ac12-2a036f5b5922'
) and REC_STATUS = 'X';


-- Mark active sector violation rule rec_status with 'X'
Update RULE_DETAIL SET REC_STATUS = 'X' WHERE ID in (
'0a306f3a-2e9f-4fc8-b295-379085eb69a8',
'64f08f25-4bf3-47dd-8135-94a9ab42ed5f',
'94ca56f4-08a5-402d-ac12-2a036f5b5922'
) and REC_STATUS = ' ';


-- Rollback the old sector violation
Update RULE_DETAIL SET REC_STATUS = ' ' WHERE ID in (
'0a306f3a-2e9f-4fc8-b295-379085eb69a8',
'64f08f25-4bf3-47dd-8135-94a9ab42ed5f',
'94ca56f4-08a5-402d-ac12-2a036f5b5922'
) and REC_STATUS = 'XX';

commit;