ALTER TABLE DEMTAB ADD ( 
DBES CHAR(1) DEFAULT ' ',
DENS CHAR(1) DEFAULT ' ');

COMMENT ON COLUMN DEMTAB.DBES IS 'DEBE status: S/schedule, E/estimate, A/actual';
COMMENT ON COLUMN DEMTAB.DENS IS 'DEEN status: S/schedule, E/estimate, A/actual';

DELETE SYSTAB WHERE TANA = 'DEM' and FINA in ('DBES','DENS');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'HAJ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'DEMTAB'
and a.column_name IN ('DBES','DENS');

commit;

