CREATE TABLE VFBTAB 
(	"URNO" CHAR(10 BYTE), 
	"UVME" CHAR(10 BYTE), 
	"UVFI" CHAR(10 BYTE), 
	"BCFN" VARCHAR2(50 BYTE), 
	"TABN" VARCHAR2(64 BYTE), 
	"MTYP" VARCHAR2(30 BYTE), 
	"ATYP" CHAR(1 BYTE), 
	"CLOR" CHAR(7 BYTE), 
	"HOPO" CHAR(3 BYTE) DEFAULT 'HAJ', 
	"LSTU" CHAR(14 BYTE), 
	"USEU" CHAR(32 BYTE), 
	"CDAT" CHAR(14 BYTE), 
	"USEC" CHAR(32 BYTE)
);

COMMENT ON COLUMN VFBTAB.URNO IS 'URNO identifies VFBTAB';
COMMENT ON COLUMN VFBTAB.UVME IS 'URNO of VMETAB';
COMMENT ON COLUMN VFBTAB.UVFI IS 'URNO of VFITAB';
COMMENT ON COLUMN VFBTAB.BCFN IS 'Broadcast field name';
COMMENT ON COLUMN VFBTAB.TABN IS '';
COMMENT ON COLUMN VFBTAB.MTYP IS 'IRT-Insert, DRT-Delete, URT/UFR, etc-Update, ""-All (Default)';
COMMENT ON COLUMN VFBTAB.ATYP IS 'N-New Line, U-Update Existing';
COMMENT ON COLUMN VFBTAB.CLOR IS 'Alert different color code. Default is Red';
COMMENT ON COLUMN VFBTAB.HOPO IS 'Home Airport';
COMMENT ON COLUMN VFBTAB.LSTU IS 'Modification DateTime';
COMMENT ON COLUMN VFBTAB.USEU IS 'User who updated the status record';
COMMENT ON COLUMN VFBTAB.CDAT IS 'Creation DateTime';
COMMENT ON COLUMN VFBTAB.USEC IS 'User who created the status record';

delete from systab where TANA = 'VFB';

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'HAJ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'VFBTAB'
and a.column_name IN ('URNO', 'UVME', 'UVFI', 'BCFN', 'TABN', 'MTYP', 'ATYP', 'CLOR', 'HOPO', 'LSTU', 'USEU', 'CDAT', 'USEC');

commit;

CREATE TABLE SMLTAB 
   (	"URNO" CHAR(10 BYTE), 
	"SECU" CHAR(10 BYTE), 
	"PMNU" CHAR(10 BYTE), 
	"CMNU" CHAR(10 BYTE), 
	"HOPO" CHAR(3 BYTE) DEFAULT 'HAJ', 
	"LSTU" CHAR(14 BYTE), 
	"USEU" CHAR(32 BYTE), 
	"CDAT" CHAR(14 BYTE), 
	"USEC" CHAR(32 BYTE)
   );

COMMENT ON COLUMN SMLTAB.URNO IS 'URNO identifies SMLTAB';
COMMENT ON COLUMN SMLTAB.SECU IS 'URNO of SECTAB';
COMMENT ON COLUMN SMLTAB.PMNU IS 'URNO of VPMTAB';
COMMENT ON COLUMN SMLTAB.CMNU IS 'URNO of VFITAB';
COMMENT ON COLUMN SMLTAB.HOPO IS 'Home Airport';
COMMENT ON COLUMN SMLTAB.LSTU IS 'Modification DateTime';
COMMENT ON COLUMN SMLTAB.USEU IS 'User who updated the status record';
COMMENT ON COLUMN SMLTAB.CDAT IS 'Creation DateTime';
COMMENT ON COLUMN SMLTAB.USEC IS 'User who created the status record';

delete from systab where TANA = 'SML';

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'HAJ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'SMLTAB'
and a.column_name IN ('URNO', 'SECU', 'PMNU', 'CMNU', 'HOPO', 'LSTU', 'USEU', 'CDAT', 'USEC');

commit;


CREATE TABLE VPSTAB
   (	"URNO" CHAR(10 BYTE) DEFAULT ' ', 
	"SECU" CHAR(10 BYTE) DEFAULT ' ', 
	"UVFI" CHAR(10 BYTE) DEFAULT ' ', 
	"ISTK" CHAR(1 BYTE) DEFAULT 'T', 
	"ENAM" VARCHAR2(64 BYTE) DEFAULT ' ', 
	"LNAM" VARCHAR2(64 BYTE) DEFAULT ' ', 
	"FITY" CHAR(1 BYTE) DEFAULT ' ', 
	"FRMT" VARCHAR2(32 BYTE) DEFAULT ' ', 
	"TDEL" CHAR(15 BYTE) DEFAULT ' ', 
	"LSTU" CHAR(14 BYTE) DEFAULT ' ', 
	"USEU" CHAR(32 BYTE) DEFAULT ' ', 
	"CDAT" CHAR(14 BYTE) DEFAULT ' ', 
	"USEC" CHAR(32 BYTE) DEFAULT ' ', 
	"HOPO" CHAR(3 BYTE) DEFAULT 'HAJ'
   );

COMMENT ON COLUMN VPSTAB.URNO IS 'URNO identifies VPSTAB';
COMMENT ON COLUMN VPSTAB.SECU IS 'URNO Identifies SECTAB';
COMMENT ON COLUMN VPSTAB.UVFI IS 'URNO of VFITAB';
COMMENT ON COLUMN VPSTAB.ISTK IS 'T-True, F-False';
COMMENT ON COLUMN VPSTAB.ENAM IS 'English Name';
COMMENT ON COLUMN VPSTAB.LNAM IS 'Local Name';
COMMENT ON COLUMN VPSTAB.FITY IS 'Field Type - C (Character), D (DateTime), N (Number)';
COMMENT ON COLUMN VPSTAB.FRMT IS 'Format String (e.g: HH:mm -> 13:00)';
COMMENT ON COLUMN VPSTAB.TDEL IS 'Use as ceda datetime format(e.g, one hour different from now time -> 00000000010000)';
COMMENT ON COLUMN VPSTAB.LSTU IS 'Modification DateTime';
COMMENT ON COLUMN VPSTAB.USEU IS 'User who updated the status record';
COMMENT ON COLUMN VPSTAB.CDAT IS 'Creation DateTime';
COMMENT ON COLUMN VPSTAB.USEC IS 'User who created the status record';
COMMENT ON COLUMN VPSTAB.HOPO IS 'Home Airport';

delete from systab where TANA = 'VPS';

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'HAJ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'VPSTAB'
and a.column_name IN ('URNO', 'SECU', 'UVFI', 'ISTK', 'ENAM', 'LNAM', 'FITY', 'FRMT', 'TDEL', 'LSTU', 'USEU', 'CDAT', 'USEC', 'HOPO');

commit;

CREATE TABLE VLETAB 
   (	"URNO" CHAR(10 BYTE), 
	"SECU" CHAR(10 BYTE), 
	"UVFI" CHAR(10 BYTE), 
	"ENAM" VARCHAR2(64 BYTE), 
	"LNAM" VARCHAR2(64 BYTE), 
	"LCTP" CHAR(1 BYTE), 
	"LCOR" CHAR(10 BYTE), 
	"INDX" NUMBER(4,0), 
	"LSTU" CHAR(14 BYTE), 
	"USEU" CHAR(32 BYTE), 
	"CDAT" CHAR(14 BYTE), 
	"USEC" CHAR(32 BYTE), 
	"CODE" CHAR(10 BYTE) DEFAULT ' ', 
	"HOPO" CHAR(3 BYTE) DEFAULT 'HAJ'
   );
 
COMMENT ON COLUMN VLETAB.URNO IS 'URNO identifies VLETAB';
COMMENT ON COLUMN VLETAB.SECU IS 'URNO Identifies SECTAB';
COMMENT ON COLUMN VLETAB.UVFI IS 'URNO of VFITAB';
COMMENT ON COLUMN VLETAB.ENAM IS 'English Name';
COMMENT ON COLUMN VLETAB.LNAM IS 'Local Name';
COMMENT ON COLUMN VLETAB.LCTP IS 'B-Bar, L-Line, P-Pie';
COMMENT ON COLUMN VLETAB.LCOR IS 'Color Code for legend';
COMMENT ON COLUMN VLETAB.INDX IS 'Legend sorting order';
COMMENT ON COLUMN VLETAB.LSTU IS 'Modification DateTime';
COMMENT ON COLUMN VLETAB.USEU IS 'User who updated the status record';
COMMENT ON COLUMN VLETAB.CDAT IS 'Creation DateTime';
COMMENT ON COLUMN VLETAB.USEC IS 'User who created the status record';
COMMENT ON COLUMN VLETAB.HOPO IS 'Home Airport';

delete from systab where TANA = 'VLE';

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'HAJ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'VLETAB'
and a.column_name IN ('URNO', 'SECU', 'UVFI', 'ENAM', 'LNAM', 'LCTP', 'LCOR', 'INDX', 'LSTU', 'USEU', 'CDAT', 'USEC', 'CODE', 'HOPO');

commit;
