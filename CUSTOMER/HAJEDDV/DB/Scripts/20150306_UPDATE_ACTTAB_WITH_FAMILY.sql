update acttab act set ACIS='A'  Where  REGEXP_LIKE (act.ACWS,'[[:digit:]]')   and TO_NUMBER(trim(act.ACWS), '99999.99') <15;
commit;
update acttab act set ACIS='B'  Where  REGEXP_LIKE (act.ACWS,'[[:digit:]]')   and 
TO_NUMBER(trim(act.ACWS), '99999.99') >= 15 and TO_NUMBER(trim(act.ACWS), '99999.99') < 24;
commit;
update acttab act set ACIS='C'  Where  REGEXP_LIKE (act.ACWS,'[[:digit:]]')   and 
TO_NUMBER(trim(act.ACWS), '99999.99') >= 24 and TO_NUMBER(trim(act.ACWS), '99999.99') < 36;
commit;
update acttab act set ACIS='D'  Where  REGEXP_LIKE (act.ACWS,'[[:digit:]]')   and 
TO_NUMBER(trim(act.ACWS), '99999.99') >= 36 and TO_NUMBER(trim(act.ACWS), '99999.99') < 52;
commit;
update acttab act set ACIS='E'  Where  REGEXP_LIKE (act.ACWS,'[[:digit:]]')   and 
TO_NUMBER(trim(act.ACWS), '99999.99') >= 52 and TO_NUMBER(trim(act.ACWS), '99999.99') < 65;
commit;
update acttab act set ACIS='F'  Where  REGEXP_LIKE (act.ACWS,'[[:digit:]]')   and 
TO_NUMBER(trim(act.ACWS), '99999.99') >= 65 and TO_NUMBER(trim(act.ACWS), '99999.99') < 80;
commit;
update acttab set afmc=' ';
commit;