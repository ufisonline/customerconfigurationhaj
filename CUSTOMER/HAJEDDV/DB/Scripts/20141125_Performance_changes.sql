--diable direct path
alter system set events '10949 trace name context forever'; 
--alter table AFTTAB storage (buffer_pool keep);
--alter table APTTAB storage (buffer_pool keep);

--Index for GRMTAB
CREATE INDEX GRMTAB_VALU ON GRMTAB
(VALU)
LOGGING
NOPARALLEL;

--Indexes for GAVTAB view 
DROP INDEX APTTAB_APC4M;

CREATE INDEX APTTAB_APC4M ON APTTAB
(APC4, APSN, APN2)
LOGGING
TABLESPACE CEDA01
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL
COMPUTE STATISTICS
ONLINE;

--Recreate CAVTAB view 
DROP VIEW CAVTAB;

CREATE OR REPLACE FORCE VIEW cavtab (ckic,
                                     ctyp,
                                     flno,
                                     ckbs,
                                     ckba,
                                     ckes,
                                     ckea,
                                     disp,
                                     ckit,
                                     apc3,
                                     stof,
                                     etof,
                                     tiff,
                                     jfno,
                                     apsn,
                                     apn2,
                                     urno,
                                     flnu,
                                     gtd1,
                                     gtd2,
                                     dseq,
                                     via3,
                                     rema,
                                     remp,
                                     csgn,
                                     coix
                                    )
AS
   SELECT a.ckic, a.ctyp, b.flno, a.ckbs, a.ckba, a.ckes, a.ckea, a.disp,
          a.ckit, b.apc3, b.stof, b.etof, b.tiff, d.jfno,c.apsn, c.apn2,
          a.urno, a.flnu, b.gtd1, b.gtd2, b.dseq, b.via3, a.rema, b.remp,
          d.csgn, a.coix
     FROM ccatab a
     INNER JOIN fddtab b 
     ON a.flnu = b.aurn
     INNER JOIN afttab d
     on  b.aurn = d.urno
     INNER JOIN 
      apttab c
     ON d.des4 = c.apc4     
     where 
       b.adid = 'D'
      AND c.apc3 != ' ' 
      AND (b.dseq > 0)
      AND (   (    ckbs != ' '
               AND a.ckba = ' '
               AND ckbs <=
                      TO_CHAR (SYS_EXTRACT_UTC (SYSTIMESTAMP),
                               'YYYYMMDDHH24MISS'
                              )
               AND ckes >=
                      TO_CHAR (SYS_EXTRACT_UTC (SYSTIMESTAMP),
                               'YYYYMMDDHH24MISS'
                              )
              )
           OR (    a.ckba != ' '
               AND a.ckba <=
                      TO_CHAR (SYS_EXTRACT_UTC (SYSTIMESTAMP),
                               'YYYYMMDDHH24MISS'
                              )
               AND (   a.ckea = ' '
                    OR a.ckea >=
                          TO_CHAR (SYS_EXTRACT_UTC (SYSTIMESTAMP),
                                   'YYYYMMDDHH24MISS'
                                  )
                   )
              )
          )



