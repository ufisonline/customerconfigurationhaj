SET DEFINE OFF;
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (1, 'ACDM                ', 'FPLA                            ', 'ATC Flight Plan Activation for arrival flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (2, 'ACDM                ', 'FPLD                            ', 'ATC Flight Plan Activation for departure flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (3, 'ACDM                ', 'FPLF                            ', 'ATC Flight Plan timing', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (4, 'ACDM                ', 'ATOT                            ', 'Actual Take Off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (5, 'ACDM                ', 'ACOR                            ', 'Local Radar Update Varies according to airport', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (6, 'ACDM                ', 'ELDT                            ', 'Estimated Landing Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (7, 'ACDM                ', 'TLDT                            ', 'Target Landing Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (8, 'ACDM                ', 'ALDT                            ', 'Actual Landing Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (10, 'ACDM                ', 'AIBT                            ', 'Actual In-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (12, 'ACDM                ', 'TOBT                            ', 'Target Off-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (15, 'ACDM                ', 'ASBT                            ', 'Actual Start Boarding Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (17, 'ACDM                ', 'AEGT                            ', 'Actual End of Ground Handling Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (19, 'ACDM                ', 'ASRT                            ', 'Actual Start Up Request Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (21, 'ACDM                ', 'EOBT                            ', 'Estimated Off-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (24, 'ACDM                ', 'RTOT                            ', 'Request to Take Off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (26, 'ACDM                ', 'ATOT                            ', 'Actual Take Off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (28, 'ACDM                ', 'AETT                            ', 'Actual End Of Taxi Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (31, 'ACDM                ', 'EETT                            ', 'Estimated End Of Taxi Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (33, 'ACDM                ', 'TISD                            ', 'Best Departure Time Status - for TIFD', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (35, 'ACDM                ', 'ETTT                            ', 'Estimated Turn-round Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (38, 'ACDM                ', 'AXIT                            ', 'Actual Taxi-in Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (40, 'ACDM                ', 'AXOT                            ', 'Actual Taxi-out Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (42, 'ACDM                ', 'SOBT                            ', 'Scheduled Off-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (45, 'ACDM                ', 'STTY                            ', 'Scheduled Turn-round Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (47, 'ACDM                ', 'STAR                            ', 'Standard Arrival Route', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (49, 'ACDM                ', 'STAA                            ', 'Status Arrival Fight Status', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (51, 'ACDM                ', 'TRMA                            ', 'Terminal Arrival', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (54, 'ACDM                ', 'IFRA                            ', 'IFR/VFR Arrival', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (56, 'ACDM                ', 'TMOA                            ', 'Final approach Varies according to airport', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (58, 'ACDM                ', 'ACT5                            ', 'Aircraft Type ICAO', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (61, 'ACDM                ', 'CSGN                            ', 'Call Sign', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (63, 'ACDM                ', 'DES4                            ', 'Destination Code ICAO', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (66, 'ACDM                ', 'PSTD                            ', 'Aircraft Stand Departure', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (68, 'ACDM                ', 'STTT                            ', 'Scheduled Turn-round Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (70, 'FLIGHT RESP         ', 'TWR                             ', 'Tower', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (73, 'FLIGHT STATUS       ', 'RTS                             ', 'Returned Taxi', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (75, 'FLIGHT STATUS       ', 'GRD                             ', 'Ground Movement', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (78, 'FLIGHT STATUS       ', 'TOW                             ', 'Towing', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (80, 'FLIGHT STATUS       ', 'RFT                             ', 'Returned Flight ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (83, 'FLIGHT STATUS       ', 'OPR                             ', 'Operation (default)', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (109, 'FLIGHT STATUS       ', 'DEP                             ', 'Departed', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (106, 'FLIGHT STATUS       ', 'TXG                             ', 'Taxi given', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (107, 'FLIGHT STATUS       ', 'RTS                             ', 'Returned Taxi', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (108, 'FLIGHT STATUS       ', 'GRD                             ', 'Ground Movement', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (85, 'FLIGHT STATUS       ', 'TFO                             ', 'Take-Off From Outstation', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (86, 'FLIGHT STATUS       ', '30M                             ', 'Thirty Minutes Inbound', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (87, 'FLIGHT STATUS       ', '10M                             ', 'Ten Miles Out', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (88, 'FLIGHT STATUS       ', 'LAN                             ', 'Landed', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (89, 'FLIGHT STATUS       ', 'ONB                             ', 'On-block', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (90, 'FLIGHT STATUS       ', 'FPL                             ', 'Schedule Cancelled', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (91, 'FLIGHT STATUS       ', 'SIT                             ', 'Slot Issue Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (92, 'FLIGHT STATUS       ', 'CNL                             ', 'Cancelled Flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (93, 'FLIGHT STATUS       ', 'AOT                             ', 'Automatic/System TOBT', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (94, 'FLIGHT STATUS       ', 'MOT                             ', 'Manual entered TOBT', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (95, 'FLIGHT STATUS       ', 'SEQ                             ', 'Sequenced ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (96, 'FLIGHT STATUS       ', 'SBY                             ', 'Standby', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (97, 'FLIGHT STATUS       ', 'BGB                             ', 'Begin of boarding ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (98, 'FLIGHT STATUS       ', 'ENB                             ', 'End of boarding', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (99, 'FLIGHT STATUS       ', 'RDY                             ', 'Aircraft ready ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (100, 'FLIGHT STATUS       ', 'RMH                             ', 'Remote Holding ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (101, 'FLIGHT STATUS       ', 'SUR                             ', 'Start-up requested', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (102, 'FLIGHT STATUS       ', 'SUG                             ', 'Start-up given ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (103, 'FLIGHT STATUS       ', 'PBR                             ', 'Push-back requested', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (104, 'FLIGHT STATUS       ', 'TXR                             ', 'Taxi requested ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (105, 'FLIGHT STATUS       ', 'PBG                             ', 'Push-back given', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (110, 'FLIGHT STATUS       ', 'RTNG                            ', 'Returned,in flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (111, 'FLIGHT STATUS       ', 'DVTG                            ', 'Diverted, in flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (112, 'FLIGHT STATUS       ', 'PDEP                            ', 'Not yet departed', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (113, 'FLIGHT STATUS       ', 'OFBL                            ', 'Taxiing for takeoff', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (114, 'FLIGHT STATUS       ', 'ENRT                            ', 'In flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (115, 'FLIGHT STATUS       ', 'LAND                            ', 'Landed and taxiing', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (116, 'FLIGHT STATUS       ', 'ARVD                            ', 'Arrived', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (117, 'FLIGHT STATUS       ', 'CNLD                            ', 'Cancelled', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (118, 'FLIGHT STATUS       ', 'DVTD                            ', 'Arrived diversion airport							 ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (119, 'FLIGHT STATUS       ', 'DNLD                            ', 'Taxiing diversion airport', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (120, 'FLIGHT STATUS       ', 'RROF                            ', 'Rerouted, taxiing', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (121, 'FLIGHT STATUS       ', 'RRTG                            ', 'Rerouted, in flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (122, 'FLIGHT STATUS       ', 'RRTD                            ', 'Rerouted, landed', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (146, 'ACDM                ', 'BAC1                            ', 'Target Box1 Schedule Close Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (147, 'ACDM                ', 'BAC4                            ', 'Target Box4 Schedule Close Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (148, 'ACDM                ', 'BAO1                            ', 'Target Box1 Schedule Opening Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (149, 'ACDM                ', 'BAO4                            ', 'Target Box4 Schedule Opening Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (150, 'ACDM                ', 'ETOD                            ', 'FIDS Estimated Departure Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (151, 'ACDM                ', 'GD1X                            ', 'Departure Gate1 Actual Begin Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (152, 'ACDM                ', 'GD1Y                            ', 'Departure Gate1 Actual End Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (153, 'ACDM                ', 'GD2X                            ', 'Departure Gate2 Actual Begin Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (154, 'ACDM                ', 'GD2Y                            ', 'Departure Gate2 Actual End Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (155, 'ACDM                ', 'NXTI                            ', 'Next Information', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (156, 'ACDM                ', 'PDBS                            ', 'Position Departure Schedule Start', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (157, 'ACDM                ', 'PDES                            ', 'Position Departure Schedule End', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (158, 'ACDM                ', 'TIFD                            ', 'Time Frame of Departure', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (159, 'ACDM                ', 'W1BA                            ', 'Lounge1 Actual Start Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (160, 'ACDM                ', 'W1EA                            ', 'Lounge1 Actual End Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (161, 'ACDM                ', 'W2BA                            ', 'Lounge2 Actual Start Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (162, 'ACDM                ', 'W2EA                            ', 'Lounge2 Actual End Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (164, 'ACDM                ', 'RSTD                            ', 'Requested Schedule Departure Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (163, 'ACDM                ', 'RSTA                            ', 'Requested Schedule Arrival Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (123, 'FLIGHT STATUS       ', 'RTDR                            ', 'Returned to stand', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (124, 'FLIGHT STATUS       ', 'RTND                            ', 'Returned departure airport', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (125, 'FLIGHT STATUS       ', 'DLND                            ', 'Diverted', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (128, 'FLIGHT STATUS       ', 'C                               ', 'Cancelled', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (130, 'FLIGHT STATUS       ', 'M                               ', 'Diverted out', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (133, 'FLIGHT STATUS       ', 'T                               ', 'To be notified', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (67, 'ACDM                ', 'GTD1                            ', 'Gate Departure', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (69, 'ACDM                ', 'RESP                            ', 'Responsibility tagged with the flight currently', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (71, 'FLIGHT RESP         ', 'APR                             ', 'Apron', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (72, 'FLIGHT RESP         ', 'GHD                             ', 'Ground Handler', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (74, 'FLIGHT STATUS       ', 'TXG                             ', 'Taxi given ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (76, 'FLIGHT STATUS       ', 'NOP                             ', 'No Operation', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (77, 'FLIGHT STATUS       ', 'SCH                             ', 'Scheduled Flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (79, 'FLIGHT STATUS       ', 'CNL                             ', 'Cancelled Flight', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (81, 'FLIGHT STATUS       ', 'RRO                             ', 'Re-routed', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (82, 'FLIGHT STATUS       ', 'DIV                             ', 'Divert ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (84, 'FLIGHT STATUS       ', 'MAP                             ', 'Missed Approach (Touch and Go)', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (126, 'FLIGHT STATUS       ', 'RRLD                            ', 'Rerouted and landing', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (127, 'FLIGHT STATUS       ', 'A                               ', 'Diverted in', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (129, 'FLIGHT STATUS       ', 'L                               ', 'Delayed', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (131, 'FLIGHT STATUS       ', 'O                               ', 'Overflown', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (132, 'FLIGHT STATUS       ', 'R                               ', 'Ramp/Air return', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (134, 'FLIGHT STATUS       ', 'X                               ', 'Deleted', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (135, 'warning_code        ', 'DONTLOAD                        ', 'DO NOT LOAD', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (9, 'ACDM                ', 'EIBT                            ', 'Estimated In-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (11, 'ACDM                ', 'ACGT                            ', 'Actual Commence of Ground Handling Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (13, 'ACDM                ', 'TOBC                            ', 'Target Off-block Time - counter ', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (14, 'ACDM                ', 'TSAT                            ', 'Target Start Up Approval Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (16, 'ACDM                ', 'ESBT                            ', 'Estimated Start Boarding Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (18, 'ACDM                ', 'ARDT                            ', 'Actual Ready Time (for Movement)', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (20, 'ACDM                ', 'ASAT                            ', 'Actual Start Up Approved Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (22, 'ACDM                ', 'AOBT                            ', 'Actual Off-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (23, 'ACDM                ', 'ETOT                            ', 'Estimated Take Off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (25, 'ACDM                ', 'TTOT                            ', 'Target Take Off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (27, 'ACDM                ', 'APET                            ', 'Actual Pushback End Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (29, 'ACDM                ', 'ASTT                            ', 'Actual Start Of Taxi Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (30, 'ACDM                ', 'CTOT                            ', 'Calculated Take-off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (32, 'ACDM                ', 'TISA                            ', 'Best Arrival Time Status - for TIFA', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (34, 'ACDM                ', 'MTTT                            ', 'Minimum Turn-round Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (36, 'ACDM                ', 'ATTT                            ', 'Actual Turn-round Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (37, 'ACDM                ', 'EXIT                            ', 'Estimated Taxi-in Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (39, 'ACDM                ', 'EXOT                            ', 'Estimated Taxi-out Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (41, 'ACDM                ', 'SIBT                            ', 'Scheduled In-block Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (43, 'ACDM                ', 'SLDT                            ', 'Scheduled-Landing Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (44, 'ACDM                ', 'STOT                            ', 'Scheduled-Take-off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (46, 'ACDM                ', 'AGHT                            ', 'Actual Ground Handling Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (48, 'ACDM                ', 'SIDR                            ', 'Standard Instrument Departure', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (50, 'ACDM                ', 'STAD                            ', 'Status Departure Fight Status', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (52, 'ACDM                ', 'TRMD                            ', 'Terminal Departure', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (53, 'ACDM                ', 'CTOT                            ', 'Calculated Take Off Time', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (55, 'ACDM                ', 'IFRD                            ', 'IFR/VFR Departure', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (57, 'ACDM                ', 'REGN                            ', 'Aircraft Registration', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (59, 'ACDM                ', 'TTYP                            ', 'Traffic Type/Flight Nature', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (60, 'ACDM                ', 'FTYP                            ', 'Flight Type', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (62, 'ACDM                ', 'ORG4                            ', 'Origin Code ICAO', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (64, 'ACDM                ', 'VIA4                            ', 'Last/First Via Code ICAO', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (65, 'ACDM                ', 'PSTA                            ', 'Aircraft Stand Arrival', '              ', '              ', '                                ', '                                ', '   ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (137, 'ACDM                ', 'BGDT                            ', 'Baggage Delivery Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (138, 'ACDM                ', 'ETOA                            ', 'FIDS Estimated Arrival Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (139, 'ACDM                ', 'PABS                            ', 'Position Arrival Schedule Start', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (136, 'ACDM                ', 'ONBL                            ', 'Onblock Time', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (140, 'ACDM                ', 'PAES                            ', 'Position Arrival Schedule End', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (141, 'ACDM                ', 'T1BA                            ', 'Transit Belt1 Actual Open', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (142, 'ACDM                ', 'T1EA                            ', 'Transit Belt1 Actual End', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (143, 'ACDM                ', 'T2BA                            ', 'Transit Belt2 Actual Open', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (144, 'ACDM                ', 'T2EA                            ', 'Transit Belt2 Actual End', '              ', '              ', '                                ', '                                ', 'HAJ');
Insert into TAGTAB
   (URNO, KEYS, TAGN, TEXT, CDAT, LSTU, USEC, USEU, HOPO)
 Values
   (145, 'ACDM                ', 'TIFA                            ', 'Time Frame of Arrival', '              ', '              ', '                                ', '                                ', 'HAJ');
COMMIT;
