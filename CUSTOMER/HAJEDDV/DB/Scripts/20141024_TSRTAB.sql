SET DEFINE OFF;
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('VIAN      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Anzahl Via                      ', '0', '          ', '          ', 'NAME(VIA_COUNT)#', 'TRIM', '19328595  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('WRO1      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Warteraum                       ', '0', 'WNAM      ', 'WRO       ', 'NAME(BOARDING_ROOM)#', 'TRIM', '19328596  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('URNO      ', 'AFT       ', '20000816120000', 'PAXT(Total Pax)', '111', 'NONE                            ', 'HAJ', '20000816120000', 'PAX bkd.                        ', '0', 'AFTU      ', 'PAX       ', 'AFT;URNO;TRIM;PAX (IDS_PAXT);;PAX:PAXT:#PAXT(Total Pax)#;', 'TRIM', '19328597  ', 'hag                             ', 'hag                             ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ACT3      ', 'AZA       ', '19990924170000', 'ACWS(Spannweite)|SEAT(Sitze)|ACHE(Hoehe)|ACLE(Laenge)|ENTY(Antriebsart)|ENNO(Anz. Triebwerke)', '000', '                                ', 'HAJ', '              ', 'A/C 3 (IATA)                    ', '0', 'ACT3      ', 'ACT       ', 'NAME(AC_TYPE_IATA)#ACWS(WINGSPAN)|SEAT(SEATS)|ACHE(AC_HEIGHT)|ACLE(AC_LENGTH)|ENTY(ENGINE_TYPE)|ENNO(COUNT_OF_ENGINES)', 'TRIM', '19328598  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('BAZ1      ', 'AFT       ', '20030211130000', 'BAZ1(Carousel1)', '001', '                                ', 'HAJ', '              ', 'Carousel1                       ', '0', 'BAZ1      ', 'AFT       ', 'AFT;BAZ1;TRIM;(IDS_BAZ1);;AFT:BAZ1:#BAZ1(IDS_BAZ1)#;', 'TRIM', '19328600  ', 'hag                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('BAZ2      ', 'AFT       ', '20030211130000', 'BAZ2(Carousel2)', '001', '                                ', 'HAJ', '              ', 'Carousel2                       ', '0', 'BAZ2      ', 'AFT       ', 'AFT;BAZ2;TRIM;(IDS_BAZ2);;AFT:BAZ2:#BAZ2(IDS_BAZ2)#;', 'TRIM', '19328601  ', 'hag                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('BAGW      ', 'AFT       ', '20040108130000', 'BAGW(Total Deadload)', '011', '                                ', 'HAJ', '              ', 'Total Deadload                  ', '0', 'BAGW      ', 'AFT       ', 'AFT;BAGW;TRIM;(IDS_BAGW);;AFT:BAGW:#BAGW(IDS_BAGW)#;', 'TRIM', '19328605  ', 'hag                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TGA1      ', 'AFT       ', '20070329000000', ' ', '010', '                                ', 'HAJ', '              ', 'Terminal 1 Gate 1 Arrival       ', '0', 'GNAM      ', 'GAT       ', 'NAME(T_GATE1_ARRIVAL)#', 'TRIM', '22222222  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TGD1      ', 'AFT       ', '20070329000000', ' ', '001', '                                ', 'HAJ', '              ', 'Terminal 1 Gate 1 TakeOff       ', '0', 'GNAM      ', 'GAT       ', 'NAME(T_GATE1_DEPARTURE)#', 'TRIM', '22222223  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ACCP      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Accompanist presence        ', '1', 'ACCP      ', 'TSP       ', 'PRM-Accompanist presence', 'TRIM', '22222224  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DOGP      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Dog for blind presence      ', '1', 'DOGP      ', 'TSP       ', 'PRM-Dog for blind presence', 'TRIM', '22222225  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('WCTP      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Wheel chair typology        ', '1', 'WCTP      ', 'TSP       ', 'PRM-Wheel chair typology', 'TRIM', '22222226  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PSTA      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Position Arrival            ', '1', 'PNAM      ', 'PST       ', 'PRM-Position Arrival', 'TRIM', '22222227  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PSTD      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Position Departure          ', '1', 'PNAM      ', 'PST       ', 'PRM-Position Departure', 'TRIM', '22222228  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ALOC      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Arrival Location            ', '1', 'PRMD      ', 'PRD       ', 'PRM-Arrival Location', 'TRIM', '22222229  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('GEND      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Gender                      ', '1', 'GEND      ', 'TSP       ', 'PRM-Gender', 'TRIM', '22222230  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('LANG      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Language                    ', '1', 'LANG      ', 'TSP       ', 'PRM-Language', 'TRIM', '22222231  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('NATL      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Nationality                 ', '1', 'NATL      ', 'TSP       ', 'PRM-Nationality', 'TRIM', '22222232  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PRMT      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-TYPE                        ', '1', 'PRMT      ', 'TSP       ', 'PRM-TYPE', 'TRIM', '22222233  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ALC3      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-Airline Code                ', '1', 'ALC2      ', 'ALT       ', 'PRM-Airline Code', 'TRIM', '22222234  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ABSR      ', 'DPV       ', '20080415120000', ' ', '111', '                                ', 'HAJ', '              ', 'PRM-BOOKING CHANNEL             ', '1', 'ABSR      ', 'TSP       ', 'PRM-BOOKING CHANNEL', 'TRIM', '22222235  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ORG3      ', 'DPV       ', '20080415120000', ' ', '010', '                                ', 'HAJ', '              ', 'PRM-Origin                      ', '1', 'APC3      ', 'APT       ', 'PRM-Origin', 'TRIM', '22222236  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DES3      ', 'DPV       ', '20080415120000', ' ', '001', '                                ', 'HAJ', '              ', 'PRM-Destination                 ', '1', 'APC3      ', 'APT       ', 'PRM-Destination', 'TRIM', '22222237  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ACT3      ', 'DPV       ', '20080415120000', ' ', '001', '                                ', 'HAJ', '              ', 'PRM-Aircraft Type 3LC           ', '1', 'ACT3      ', 'ACT       ', 'PRM-Aircraft Type 3LC', 'TRIM', '22222238  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('FLTI      ', 'AFT       ', '20080415120000', ' ', '001', '                                ', 'HAJ', '              ', 'Flight ID                       ', '1', 'SICO      ', 'FSI       ', 'NAME(Flight ID)#', 'TRIM', '22222239  ', 'CEDA                            ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('HAPX      ', 'AFT       ', '20001004170000', ' ', '011', '                                ', 'HAJ', '20001219123342', 'HDL-Agent Pax                   ', '0', 'HSNA      ', 'HAG       ', 'AFT;HAPX;TRIM;Handling Agent(IDS_HAPX);;HDL:HSNA##;', 'TRIM', '19324712  ', 'mne                             ', 'hag                             ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('SFIF      ', 'AFT       ', '20001004170000', ' ', '011', '                                ', 'HAJ', '20001004170000', 'franzφsischsprachig', '0', '          ', '          ', ' ', 'TRIM', '19324713  ', 'mne                             ', 'mne                             ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('HARA      ', 'AFT       ', '20001004170000', ' ', '011', '                                ', 'HAJ', '20001219123316', 'HDL-Agent Ramp                  ', '0', 'HSNA      ', 'HAG       ', 'AFT;HARA;TRIM;Handling Agent(IDS_HARA);;HDL:HSNA##;', 'TRIM', '19324714  ', 'mne                             ', 'hag                             ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DDLF      ', 'AFT       ', '20001004170000', 'DDLF(Deadload Hold 5)', '011', '                                ', 'HAJ', '20001219133033', 'Deadload Hold 5                 ', '0', 'DDLF      ', 'AFT       ', 'AFT;DDLF;TRIM;(IDS_DDLF);;AFT:DDLF:#DDLF(IDS_DDLF)#;', 'TRIM', '19324715  ', 'mne                             ', 'hag                             ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DDLR      ', 'AFT       ', '20001004170000', 'DDLR(Total Deadload Rot)', '100', '                                ', 'HAJ', '20001219133001', 'Total Deadload Rot              ', '0', 'DDLR      ', 'AFT       ', 'AFT;DDLR;TRIM;(IDS_DDLR);;AFT:DDLR:#DDLR(IDS_DDLR)#;', 'TRIM', '19324716  ', 'mne                             ', 'hag                             ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ACT3      ', 'AFT       ', '19990624200000', 'ACWS(Spannweite)|SEAT(Sitze)|ACHE(Hoehe)|ACLE(Laenge)|ENTY(Antriebsart)|ENNO(Anz. Triebwerke)|ACBT(A/C-BodyType)', '100', '                                ', 'HAJ', '              ', 'A/C 3 (IATA)                    ', '0', 'ACT3      ', 'ACT       ', 'NAME(AC_TYPE_IATA)#ACWS(WINGSPAN)|SEAT(SEATS)|ACHE(AC_HEIGHT)|ACLE(AC_LENGTH)|ENTY(ENGINE_TYPE)|ENNO(COUNT_OF_ENGINES)', 'TRIM', '19328522  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ACT5      ', 'AFT       ', '19990624200000', 'ACWS(Spannweite)|SEAT(Sitze)|ACHE(Hoehe)|ACLE(Laenge)|ENTY(Antriebsart)|ENNO(Anz. Triebwerke)|ACBT(A/C-BodyType)', '100', '                                ', 'HAJ', '              ', 'A/C 5 (ICAO)                    ', '0', 'ACT5      ', 'ACT       ', 'NAME(AC_TYPE_ICAO)#ACWS(WINGSPAN)|SEAT(SEATS)|ACHE(AC_HEIGHT)|ACLE(AC_LENGTH)|ENTY(ENGINE_TYPE)|ENNO(COUNT_OF_ENGINES)', 'TRIM', '19328523  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ACTI      ', 'AFT       ', '19990624200000', 'ACWS(Spannweite)|SEAT(Sitze)|ACHE(Hoehe)|ACLE(Laenge)|ENTY(Antriebsart)|ENNO(Anz. Triebwerke)', '100', '                                ', 'HAJ', '              ', 'A/C Mod. ICAO Code              ', '0', 'ACTI      ', 'ACT       ', 'NAME(AC_MOD_ICAO)#ACWS(WINGSPAN)|SEAT(SEATS)|ACHE(AC_HEIGHT)|ACLE(AC_LENGTH)|ENTY(ENGINE_TYPE)|ENNO(COUNT_OF_ENGINES)', 'TRIM', '19328524  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('AIRB      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Startzeit (Beste Zeit)          ', '0', '          ', '          ', 'NAME(TAKEOFF_TIME)#', 'DTIM', '19328525  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ALC2      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Airlinecode 2 (IATA)            ', '0', 'ALC2      ', 'ALT       ', 'NAME(AIRLINECODE_IATA)#', 'TRIM', '19328526  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ALC3      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Airlinecode 3 (ICAO)            ', '0', 'ALC3      ', 'ALT       ', 'NAME(AIRLINECODE_ICAO)#', 'TRIM', '19328527  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ANNX      ', 'AFT       ', '19990624200000', ' ', '100', 'NONE                            ', 'HAJ', '              ', 'Laerm Annex                     ', '0', 'ANNX      ', 'ACR       ', 'NAME(LAERM_ANNEX)#', 'TRIM', '19328528  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('BAGN      ', 'AFT       ', '19990624200000', 'BAGN(Baggage (Pieces))', '011', '                                ', 'HAJ', '              ', 'Gepaeckstuecke                  ', '0', 'BAGN      ', 'AFT       ', 'NAME(BAGGAGE_PCS)#', 'LONG', '19328529  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('BLT1      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Gepaeckband 1                   ', '0', 'BNAM      ', 'BLT       ', 'NAME(BAGGAGE_BELT1)#', 'TRIM', '19328530  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('BLT2      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Gepaeckband 2                   ', '0', 'BNAM      ', 'BLT       ', 'NAME(BAGGAGE_BELT2)#', 'TRIM', '19328531  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('CGOT      ', 'AFT       ', '19990624200000', 'CGOT(Cargo (Tons))', '011', '                                ', 'HAJ', '              ', 'Fracht (to)                     ', '0', 'CGOT      ', 'AFT       ', 'NAME(CARGO_TONS)#', 'LONG', '19328532  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('CHT3      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Veranstalter                    ', '0', 'CHTC      ', 'CHT       ', 'NAME(VERANSTALTER)#', 'TRIM', '19328533  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('CSGN      ', 'AFT       ', '19990624200000', ' ', '100', '                                ', 'HAJ', '              ', 'Call Sign                       ', '0', '          ', '          ', 'NAME(CALL_SIGN)#', 'TRIM', '19328534  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DCD1      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Delaycode Dep. 1                ', '0', 'DECN      ', 'DEN       ', 'NAME(DELAYCODE_DEP1)#', 'TRIM', '19328535  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DCD2      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Delaycode Dep. 2                ', '0', 'DECN      ', 'DEN       ', 'NAME(DELAYCODE_DEP2)#', 'TRIM', '19328536  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DES3      ', 'AFT       ', '19990624200000', 'LAND(Laenderkennung)|APTT(Airport Typ)', '001', '                                ', 'HAJ', '              ', 'Destination 3 (IATA)            ', '0', 'APC3      ', 'APT       ', 'NAME(DESTINATION_IATA)#LAND(COUNTRY_CODE)|APTT(AIRPORT_TYPE)', 'TRIM', '19328537  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DES4      ', 'AFT       ', '19990624200000', 'LAND(Laenderkennung)|APTT(Airport Typ)', '001', '                                ', 'HAJ', '              ', 'Destination 4 (ICAO)            ', '0', 'APC4      ', 'APT       ', 'NAME(DESTINATION_ICAO)#LAND(COUNTRY_CODE)|APTT(AIRPORT_TYPE)', 'TRIM', '19328538  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DOOA      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Flugtag Ankunft                 ', '0', '          ', '          ', 'NAME(TAG_ANKUNFT)#', 'TRIM', '19328539  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DOOD      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Flugtag Abflug                  ', '0', '          ', '          ', 'NAME(TAG_ABFLUG)#', 'TRIM', '19328540  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DTD1      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Delaytime Dep. 1                ', '0', '          ', '          ', 'NAME(DELAYTIME_DEP1)#', 'TRIM', '19328541  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('DTD2      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Delaytime Dep. 2                ', '0', '          ', '          ', 'NAME(DELAYTIME_DEP2)#', 'TRIM', '19328542  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ENTY      ', 'AFT       ', '19990624200000', ' ', '100', 'NONE                            ', 'HAJ', '              ', 'Antriebsart                     ', '0', 'ENTY      ', 'ACR       ', 'NAME(ENGINE_TYPE)#', 'TRIM', '19328543  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ETAI      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'ETA (Beste Zeit)                ', '0', '          ', '          ', 'NAME(IDS_ETA)#', 'DTIM', '19328544  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ETDI      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'ETD (Beste Zeit)                ', '0', '          ', '          ', 'NAME(IDS_ETD)#', 'DTIM', '19328545  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('EXT1      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Ausgang 1 Ankunft               ', '0', 'ENAM      ', 'EXT       ', 'NAME(EXIT1_ARRIVAL)#', 'TRIM', '19328546  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('EXT2      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Ausgang 2 Ankunft               ', '0', 'ENAM      ', 'EXT       ', 'NAME(EXIT2_ARRIVAL)#', 'TRIM', '19328547  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('FLNS      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Suffix                          ', '0', '          ', '          ', 'NAME(SUFFIX)#', 'TRIM', '19328548  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('FLTN      ', 'AFT       ', '19990624200000', 'FLTN(Flightnumber)', '011', '                                ', 'HAJ', '              ', 'Flight Number                   ', '0', 'FLTN      ', 'AFT       ', 'NAME(FLIGHT_NO)#', 'LONG', '19328549  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('FTYP      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Flight Status                   ', '0', '          ', '          ', 'NAME(FLIGHT_STATUS)#', 'TRIM', '19328550  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('GTA1      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Gate 1 Ankunft                  ', '0', 'GNAM      ', 'GAT       ', 'NAME(GATE1_ARRIVAL)#', 'TRIM', '19328551  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('GTA2      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Gate 2 Ankunft                  ', '0', 'GNAM      ', 'GAT       ', 'NAME(GATE2_ARRIVAL)#', 'TRIM', '19328552  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('GTD1      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Gate 1 Abflug                   ', '0', 'GNAM      ', 'GAT       ', 'NAME(GATE1_DEPARTURE)#', 'TRIM', '19328553  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('GTD2      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Doppelgate Abflug               ', '0', 'GNAM      ', 'GAT       ', 'NAME(GLOBAL_DEP_GATE)#', 'TRIM', '19328554  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('HTYP      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Abfertigungsart                 ', '0', 'HTYP      ', 'HTY       ', 'NAME(HANDLING_TYPE)#', 'TRIM', '19328555  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('IFRA      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Anflugverfahren                 ', '0', '          ', '          ', 'NAME(ANFLUG_VERFAHREN)#', 'TRIM', '19328556  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('IFRD      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Abflugverfahren                 ', '0', '          ', '          ', 'NAME(ABFLUG_VERFAHREN)#', 'TRIM', '19328557  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ISKD      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'ISK                             ', '0', '          ', '          ', 'NAME(ISK)#', 'DTIM', '19328558  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('JCNT      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Anzahl Joint Flt.No.            ', '0', '          ', '          ', 'NAME(JOINT_FLT_COUNT)#', 'TRIM', '19328559  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('JFNO      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Joint Flt.No.                   ', '0', '          ', '          ', 'NAME(JOINT_FLT_NUMBER)#', 'TRIM', '19328560  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('LAND      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Landezeit (Beste Zeit)          ', '0', '          ', '          ', 'NAME(TOUCH_DOWN_TIME)#', 'DTIM', '19328561  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('MAIL      ', 'AFT       ', '19990624200000', 'MAIL(Mail (Tons))', '011', '                                ', 'HAJ', '              ', 'Mail (to)                       ', '0', 'MAIL      ', 'AFT       ', 'NAME(MAIL_TONS)#', 'LONG', '19328562  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('MING      ', 'AFT       ', '19990624200000', 'MING(Min. Groundtime)', '100', '                                ', 'HAJ', '              ', 'Mindestbodenzeit                ', '0', 'MING      ', 'AFT       ', 'NAME(MIN_GROUNDTIME)#', 'LONG', '19328563  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('MTOW      ', 'AFT       ', '19990624200000', 'MTOW(MTOW)', '001', '                                ', 'HAJ', '              ', 'MTOW                            ', '0', 'MTOW      ', 'AFT       ', 'NAME(IDS_MTOW)#', 'LONG', '19328564  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('NOSE      ', 'AFT       ', '19990624200000', ' ', '100', '                                ', 'HAJ', '              ', 'Sitze                           ', '0', '          ', '          ', 'NAME(SEATS)#', 'LONG', '19328565  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('OFBL      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Offblock Zeit (Beste Zeit)      ', '0', '          ', '          ', 'NAME(OFFBLOCK_TIME)#', 'DTIM', '19328566  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ONBL      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Onblock Zeit (Beste Zeit)       ', '0', '          ', '          ', 'NAME(ONBLOCK_TIME)#', 'DTIM', '19328567  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ORG3      ', 'AFT       ', '19990624200000', 'LAND(Laenderkennung)|APTT(Airport Typ)', '010', '                                ', 'HAJ', '              ', 'Origin 3 (IATA)                 ', '0', 'APC3      ', 'APT       ', 'NAME(ORIGIN_IATA)#LAND(COUNTRY_CODE)|APTT(AIRPORT_TYPE)', 'TRIM', '19328568  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('ORG4      ', 'AFT       ', '19990624200000', 'LAND(Laenderkennung)|APTT(Airport Typ)', '010', '                                ', 'HAJ', '              ', 'Origin 4 (ICAO)                 ', '0', 'APC4      ', 'APT       ', 'NAME(ORIGIN_ICAO)#LAND(COUNTRY_CODE)|APTT(AIRPORT_TYPE)', 'TRIM', '19328569  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAID      ', 'AFT       ', '19990624200000', ' ', '111', '                                ', 'HAJ', '              ', 'Barzahler                       ', '0', '          ', '          ', 'NAME(BARZAHLER)#', 'TRIM', '19328570  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAX1      ', 'AFT       ', '19990624200000', 'PAX1(PAX First)', '011', '                                ', 'HAJ', '              ', 'PAX First                       ', '0', 'PAX1      ', 'AFT       ', 'NAME(PAX_FIRST)#', 'LONG', '19328571  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAX2      ', 'AFT       ', '19990624200000', 'PAX2(PAX Business)', '011', '                                ', 'HAJ', '              ', 'PAX Business                    ', '0', 'PAX2      ', 'AFT       ', 'NAME(PAX_BUSINESS)#', 'LONG', '19328572  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAX3      ', 'AFT       ', '19990624200000', 'PAX3(PAX Economy)', '011', '                                ', 'HAJ', '              ', 'PAX Economy                     ', '0', 'PAX3      ', 'AFT       ', 'NAME(PAX_ECONOMY)#', 'LONG', '19328573  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAXF      ', 'AFT       ', '19990624200000', 'PAXF(PAX Transfer)', '011', '                                ', 'HAJ', '              ', 'PAX Transfer                    ', '0', 'PAXF      ', 'AFT       ', 'NAME(PAX_TRANSFER)#', 'LONG', '19328574  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAXI      ', 'AFT       ', '19990624200000', 'PAXI(PAX Transit)', '011', '                                ', 'HAJ', '              ', 'PAX Transit                     ', '0', 'PAXI      ', 'AFT       ', 'NAME(PAX_TRANSIT)#', 'LONG', '19328575  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PAXT      ', 'AFT       ', '19990624200000', 'PAXT(PAX Total)', '011', '                                ', 'HAJ', '              ', 'PAX Total                       ', '0', 'PAXT      ', 'AFT       ', 'NAME(PAX_TOTAL)#', 'LONG', '19328576  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PSTA      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Arrival Position                ', '0', 'PNAM      ', 'PST       ', 'NAME(POSITION_ARRIVAL)#', 'TRIM', '19328577  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('PSTD      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Departure Position              ', '0', 'PNAM      ', 'PST       ', 'NAME(POSITION_DEPARTURE)#', 'TRIM', '19328578  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('REGN      ', 'AFT       ', '19990624200000', ' ', '100', 'NONE                            ', 'HAJ', '              ', 'Registration                    ', '0', 'REGN      ', 'ACR       ', 'NAME(REGISTRATION)#', 'TRIM', '19328579  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('RWYA      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Landebahn                       ', '0', 'RNAM      ', 'RWY       ', 'NAME(RUNWAY_ARR)#', 'TRIM', '19328580  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('RWYD      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Startbahn                       ', '0', 'RNAM      ', 'RWY       ', 'NAME(RUNWAY_DEP)#', 'TRIM', '19328581  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('SLOT      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Slot                            ', '0', '          ', '          ', 'NAME(SLOT)#', 'DTIM', '19328582  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('STOA      ', 'AFT       ', '19990624200000', 'STOA(STA)', '010', '                                ', 'HAJ', '              ', 'STA                             ', '0', 'STOA      ', 'AFT       ', 'NAME(IDS_STA)#', 'DTIM', '19328583  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('STOD      ', 'AFT       ', '19990624200000', 'STOD(STD)', '001', '                                ', 'HAJ', '              ', 'STD                             ', '0', 'STOD      ', 'AFT       ', 'NAME(IDS_STD)#', 'DTIM', '19328584  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('STYP      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Service Typ                     ', '0', 'STYP      ', 'STY       ', 'NAME(SERVICE_TYPE)#', 'TRIM', '19328585  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TIFA      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Beste Zeit Ankunft              ', '0', '          ', '          ', 'NAME(BEST_TIME_ARR)#', 'DTIM', '19328586  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TIFD      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Beste Zeit Abflug               ', '0', '          ', '          ', 'NAME(BEST_TIME_DEP)#', 'DTIM', '19328587  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TMOA      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'TMO (Beste Zeit)                ', '0', '          ', '          ', 'NAME(IDS_TMO)#', 'DTIM', '19328588  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TTYP      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Nature                          ', '0', 'TTYP      ', 'NAT       ', 'NAME(NATURE)#', 'TRIM', '19328589  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('TWR1      ', 'AFT       ', '19990624200000', ' ', '011', '                                ', 'HAJ', '              ', 'Terminal                        ', '0', 'TERM      ', 'GAT       ', 'NAME(TERMINAL)#', 'TRIM', '19328590  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('VIA3      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Via 3 (IATA)                    ', '0', 'APC3      ', 'APT       ', 'NAME(VIA_IATA)#', 'TRIM', '19328591  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('VIA4      ', 'AFT       ', '19990624200000', ' ', '010', '                                ', 'HAJ', '              ', 'Via 4 (ICAO)                    ', '0', 'APC4      ', 'APT       ', 'NAME(VIA_ICAO)#', 'TRIM', '19328592  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('VIA3      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Via 3 (IATA)                    ', '0', 'APC3      ', 'APT       ', 'NAME(VIA_IATA)#', 'TRIM', '19328593  ', 'tsc                             ', '                                ', ' ');
Insert into TSRTAB
   (BFLD, BTAB, CDAT, EXTE, FIRE, FORM, HOPO, LSTU, NAME, OPER, RFLD, RTAB, TEXT, TYPE, URNO, USEC, USEU, VALU)
 Values
   ('VIA4      ', 'AFT       ', '19990624200000', ' ', '001', '                                ', 'HAJ', '              ', 'Via 4 (ICAO)                    ', '0', 'APC4      ', 'APT       ', 'NAME(VIA_ICAO)#', 'TRIM', '19328594  ', 'tsc                             ', '                                ', ' ');
COMMIT;
