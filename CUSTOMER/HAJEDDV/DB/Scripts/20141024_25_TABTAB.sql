SET DEFINE OFF;
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Previous password table', '              ', '      ', 'PWD     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'PWS     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rules equipment groups', '              ', '      ', 'REG     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '136       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Release table RMS', '              ', '      ', 'REL     ', ' ', ' ', ' ', ' ', '      ', ' ', '137       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'REM     ', ' ', ' ', ' ', ' ', '      ', ' ', '156       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rules equipment', '              ', '      ', 'REQ     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '138       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Resolution table for Functions and Qualification (RPF and RPQ)', '              ', '      ', 'RFQ     ', ' ', ' ', ' ', ' ', '      ', ' ', '139       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rules locations', '              ', '      ', 'RLO     ', ' ', ' ', ' ', ' ', '      ', ' ', '140       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Shift roster', '              ', '      ', 'ROS     ', ' ', ' ', ' ', ' ', '      ', ' ', '141       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rules personal functions', '              ', '      ', 'RPF     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '142       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rules personal qualifications', '              ', '      ', 'RPQ     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '143       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Request Table', '              ', '      ', 'RQE     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rule demand (duty requirments)', '              ', '      ', 'RUD     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '144       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rule Event', '              ', '      ', 'RUE     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '145       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20010201134221', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rule demand view!', '20010201140200', '      ', 'RUV     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '146       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'RWU     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Runway Table', '              ', 'L02TAB', 'RWY     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '147       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Zuordnung Arbeitsvertragsarten', '              ', '      ', 'SCO     ', ' ', ' ', ' ', ' ', '      ', ' ', '148       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Staff Default allocation Table', '              ', '      ', 'SDA     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '149       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000309100452', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Staff daily Data', '20000309100701', '      ', 'SDD     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '150       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'shift demand group', '              ', '      ', 'SDG     ', ' ', ' ', ' ', ' ', '      ', ' ', '151       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000807103255', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Single Distribution', '20000807103414', '      ', 'SDI     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '152       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'shift demand', '              ', '      ', 'SDT     ', ' ', ' ', ' ', ' ', '      ', ' ', '153       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Flight Sesson', '              ', '      ', 'SEA     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '154       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Security Table (SEC)', '              ', 'SLGTAB', 'SEC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '155       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'service catalogue equipment', '              ', '      ', 'SEE     ', ' ', ' ', ' ', ' ', '      ', ' ', '156       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Service Catalogue Functions', '              ', '      ', 'SEF     ', ' ', ' ', ' ', ' ', '      ', ' ', '157       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Service Catalogue equipment groups', '              ', '      ', 'SEG     ', ' ', ' ', ' ', ' ', '      ', ' ', '158       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Service catalogue locations', '              ', '      ', 'SEL     ', ' ', ' ', ' ', ' ', '      ', ' ', '159       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Dervice catalogue Qualifications', '              ', '      ', 'SEQ     ', ' ', ' ', ' ', ' ', '      ', ' ', '160       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Ground handling services', '              ', '      ', 'SER     ', ' ', ' ', ' ', ' ', '      ', ' ', '161       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SET     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '162       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Resolution Table for Functions and Qalifications (SEF and SEQ)', '              ', '      ', 'SFQ     ', ' ', ' ', ' ', ' ', '      ', ' ', '163       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SHF     ', ' ', ' ', ' ', ' ', '      ', ' ', '166       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Log table for SEC', '              ', '      ', 'SLG     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '167       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000309094909', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Staff Monthly Data', '20000309095228', '      ', 'SMD     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '168       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'System Maintenance and monitoring', '              ', '      ', 'SMM     ', ' ', ' ', ' ', ' ', 'SMMS  ', ' ', '169       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Zuordnung Organisationseinheit', '              ', '      ', 'SOR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '170       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Zuordnung Qualifikation', '              ', '      ', 'SPE     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '171       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Zuordnung Funktion', '              ', '      ', 'SPF     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '172       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Additional Info on flight nature and handling type', '              ', 'L02TAB', 'SPH     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '173       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Shift roster', '              ', '      ', 'SPL     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '174       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SQS     ', ' ', ' ', ' ', ' ', '      ', ' ', '175       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20010129120827', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Salary Code', '20010129121744', '      ', 'SRC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '176       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SRE     ', ' ', ' ', ' ', ' ', '      ', ' ', '185       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Special Requirement Log', '              ', '      ', 'SRL     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', 'L01TAB', 'SSI     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '177       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', 'L01TAB', 'SSR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Zuordnung Fahrgemeinschaft', '              ', '      ', 'STE     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '178       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Employees basic data', '              ', '      ', 'STF     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '179       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Standard Rotation', '              ', 'L02TAB', 'STR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '180       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000314092008', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Status Appendix', '20000314092645', '      ', 'STS     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '181       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Service types', '              ', '      ', 'STY     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '182       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000803100528', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '20010130150250', '      ', 'SWF     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '183       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Zuordnung Work groups', '              ', '      ', 'SWG     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '184       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Syncronisation', '              ', '      ', 'SYN     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '185       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'System Table (SYS)', '              ', '      ', 'SYS     ', ' ', ' ', ' ', ' ', '      ', ' ', '186       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Table Table (SYS)', '              ', '      ', 'TAB     ', ' ', ' ', ' ', ' ', '      ', ' ', '187       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'TAG     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20010315152513', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Temporary Data', '20010315152736', '      ', 'TDA     ', ' ', ' ', ' ', ' ', '      ', ' ', '188       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Fahrgemeinschaften', '              ', '      ', 'TEA     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '189       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'TFR     ', ' ', ' ', ' ', ' ', '      ', ' ', '190       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Time Parameter table', '              ', 'L02TAB', 'TIP     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '191       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Telex Data Table', '              ', 'L03TAB', 'TLX     ', ' ', ' ', ' ', ' ', 'TLX   ', ' ', '192       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Templates', '              ', '      ', 'TPL     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '193       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Template Source', '              ', '      ', 'TSR     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '194       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Taxi way Table', '              ', 'L02TAB', 'TWY     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '195       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Text table (languge)', '              ', '      ', 'TXT     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '196       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'UAA     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000502145250', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Org. Units for Budget Calculation', '20000503114307', '      ', 'UBU     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '197       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000807102406', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Use of Common Demand', '20001026134745', '      ', 'UCD     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '198       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'UDF     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '199       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'User defined views', '              ', 'L00TAB', 'UDV     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '200       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000502143800', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Result Data of Headcount calculation', '20000502144259', '      ', 'UHD     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '201       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Unit load device', '              ', '      ', 'ULD     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000529120150', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Result Mutations', '20000529120532', '      ', 'UMU     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '202       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'UST     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'User defined Configurations', '              ', '      ', 'VCD     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '204       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Version', '              ', 'L02TAB', 'VER     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '205       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VFI     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Very important persons table', '              ', 'L01TAB', 'VIP     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '206       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VME     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'View Point Definition', '              ', '      ', 'VPD     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VPF     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VPM     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'WAR     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Wegzeiten', '              ', 'L02TAB', 'WAY     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '208       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'WCA     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Groups of WAY-Entries', '20001027153218', '      ', 'WGN     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '209       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Workgroups table', '              ', '      ', 'WGP     ', ' ', ' ', ' ', ' ', '      ', ' ', '210       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'WGR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '211       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Employees wishes for Shifts', '              ', '      ', 'WIS     ', ' ', ' ', ' ', ' ', '      ', ' ', '212       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Working Rules', '              ', '      ', 'WOR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '213       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Waiting Lounges', '              ', 'L02TAB', 'WRO     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '214       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Parking Position Table', '20000324183958', 'L02TAB', 'PST     ', 'GRP', ' ', ' ', ' ', 'DBDPS ', ' ', '109       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000324173918', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Aircraft Type', '20001208144226', 'L02TAB', 'ACT     ', 'GRP', ' ', ' ', ' ', 'DBDPS ', ' ', '3         ', 'SYSADMIN', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   ('Airline Table describes any attributes of an airline. Beginning with its name and shortcuts the table also holds the adress of the head and so on.', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Airlines table', '20000327175730', 'L02TAB', 'ALT     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '7         ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Airport Table', '20000327175829', 'L02TAB', 'APT     ', 'GRP', ' ', ' ', ' ', 'DBDPS ', ' ', '8         ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Flight natures Table', '20000324183433', 'L02TAB', 'NAT     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '89        ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000308163441', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Profile for PAX at Checkin', '20000316152453', '      ', 'PRO     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '106       ', 'SYSADMIN', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Profile for PAX (general)', '20000316152709', '      ', 'PXA     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '110       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'PAX-Classes for Profiles', '20000316152730', '      ', 'PXC     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '111       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Static Group Members', '20000316153614', '      ', 'SGM     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '137       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Static groups', '20000316153708', '      ', 'SGR     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '138       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Validity table', '20000316155239', '      ', 'VAL     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '165       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Value Table', '20000316155440', '      ', 'VTP     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '169       ', ' ', 'SYSADMIN');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Checkin Counters', '              ', 'L02TAB', 'CIC     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '29        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Flight Sector ID Table', '20140609      ', '      ', 'FSI     ', ' ', ' ', ' ', ' ', 'FSI   ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SWI     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Account', '              ', '      ', 'ACC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'ACI     ', ' ', ' ', ' ', ' ', '      ', ' ', '2         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Aircraft Registrations', '              ', 'L02TAB', 'ACR     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '2         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000920094245', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Account Definition - Definition and description of an account', '20001026141526', '      ', 'ADE     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '4         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Import Interface to Employees Basic Data corrosponding to STFTAB', '              ', '      ', 'ADR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '5         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000308171115', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'ADS Account Definition Supplements. Additional Table to ADE', '20000308174107', '      ', 'ADS     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '6         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Aircraft Family', '              ', '      ', 'AFM     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '7         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Actual Flight Table', '              ', 'L01TAB', 'AFT     ', ' ', 'STORAGE ( INITIAL 500 M NEXT 50 M PCTINCREASE 0  )', ' ', ' ', 'AFT   ', 'CEDA2', '8         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Allocation Types', '              ', '      ', 'ALO     ', ' ', ' ', ' ', ' ', '      ', ' ', '9         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Additional passenger table', '              ', '      ', 'APX     ', ' ', ' ', ' ', ' ', '      ', ' ', '12        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Archive Flight Table', '              ', 'L01TAB', 'ARC     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '13        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Bewertungsfaktoren', '              ', '      ', 'ASF     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '14        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'ASZ     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'ATR     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Weather Table', '              ', '      ', 'AWI     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '15        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Baggage Departure Information', '              ', '      ', 'BDI     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', 'L02TAB', 'BLK     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '16        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Last minute baggage', '              ', '      ', 'BLM     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '17        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Baggage Belts', '              ', 'L02TAB', 'BLT     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '18        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basic Shift data Table', '              ', '      ', 'BSD     ', 'GRP', ' ', ' ', ' ', '      ', ' ', '19        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000503114724', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Budget', '20000503115431', '      ', 'BUD     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '20        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', 'N', ' ', '              ', '      ', 'BUT     ', ' ', ' ', ' ', ' ', '      ', ' ', '18        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Calendar (SEC)', '              ', 'SLGTAB', 'CAL     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '21        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Checkin-Counter Allocation', '              ', 'L00TAB', 'CCA     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '22        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Checkin-Counter Class', '              ', 'L01TAB', 'CCC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '23        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000728122124', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Code Share Table', '20000728124222', '      ', 'CDS     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '24        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'CFL     ', ' ', ' ', ' ', ' ', '      ', ' ', '25        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20010214112723', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Actually a view on gattab and cictab', '20010214112744', '      ', 'CGV     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '26        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'CHA     ', ' ', ' ', ' ', ' ', '      ', ' ', '27        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Veranstalter', '              ', '      ', 'CHT     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '28        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basicdata Table for Chutes', '              ', '      ', 'CHU     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Checklist processing basic data', '              ', '      ', 'CLB     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Checklist processing online', '              ', '      ', 'CLP     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000804121305', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Contract Holiday', '20010202161853', '      ', 'COH     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '30        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Conditions', '              ', '      ', 'CON     ', ' ', ' ', ' ', ' ', '      ', ' ', '31        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Work contract table', '              ', '      ', 'COT     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '32        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'CRA     ', ' ', ' ', ' ', ' ', '      ', ' ', '28        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'CRC     ', ' ', ' ', ' ', ' ', '      ', ' ', '29        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20130613      ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'CRITICAL RULES TABLE', '20130613      ', '      ', 'CRR     ', ' ', ' ', ' ', ' ', 'CRR   ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   ('Definition of different Groups for Holyday Reduction caused by Absence', '20010117090612', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Holyday Reduction', '20010117091421', '      ', 'CTH     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '33        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Beaufschlagung', '              ', '      ', 'DBO     ', ' ', ' ', ' ', ' ', '      ', ' ', '34        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Delay Codes flights', '              ', '      ', 'DCF     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'DEL     ', ' ', ' ', ' ', ' ', '      ', ' ', '35        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Demand Table', '              ', '      ', 'DEM     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '36        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Delay Code Table', '              ', 'L02TAB', 'DEN     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '37        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'DET     ', ' ', ' ', ' ', ' ', '      ', ' ', '38        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Display Device Table', '              ', '      ', 'DEV     ', ' ', ' ', ' ', ' ', '      ', ' ', '39        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', '      ', '      ', ' ', 'HAJ', ' ', 'Display Devices', '              ', '      ', 'DEVTAB  ', ' ', ' ', ' ', ' ', '      ', ' ', '1000      ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Dynamic Groups', '              ', '      ', 'DGR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '40        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   ('Group Delegation for JOBTAB', '20001115182119', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Group Delegation', '20001115182530', '      ', 'DLG     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '41        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Delay Log', '              ', '      ', 'DLY     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Daily Roster Absence', '              ', '      ', 'DRA     ', ' ', ' ', ' ', ' ', '      ', ' ', '42        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Daily Roster Deviation', '              ', '      ', 'DRD     ', ' ', ' ', ' ', ' ', '      ', ' ', '43        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Rosterdaily  Group', '              ', '      ', 'DRG     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '44        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Daily Roster record', '              ', '      ', 'DRR     ', ' ', ' ', ' ', ' ', '      ', ' ', '45        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Daily roster supplement', '              ', '      ', 'DRS     ', ' ', ' ', ' ', ' ', '      ', ' ', '46        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Wishes in daily shift', '              ', '      ', 'DRW     ', ' ', ' ', ' ', ' ', '      ', ' ', '47        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Display Layouts', '              ', '      ', 'DSP     ', ' ', ' ', ' ', ' ', '      ', ' ', '48        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', '      ', '      ', ' ', 'HAJ', ' ', 'Display Layouts', '              ', '      ', 'DSPTAB  ', ' ', ' ', ' ', ' ', '      ', ' ', '1001      ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'daily roster', '              ', '      ', 'DSR     ', ' ', ' ', ' ', ' ', '      ', ' ', '49        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'DST     ', ' ', ' ', ' ', ' ', '      ', ' ', '50        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Engine Type Table', '              ', '      ', 'ENT     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '51        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'EQA     ', ' ', ' ', ' ', ' ', '      ', ' ', '53        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'EQT     ', ' ', ' ', ' ', ' ', '      ', ' ', '54        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Equipment Table', '              ', '      ', 'EQU     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '52        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'ESP     ', ' ', ' ', ' ', ' ', '      ', ' ', '53        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'EXC     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Exits table', '              ', 'L02TAB', 'EXT     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '54        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FEA     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'FIDS-Remarks Table', '              ', 'L02TAB', 'FID     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '55        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000929163736', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Prognose Data', '20000929164446', '      ', 'FIT     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '56        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Functions (SEC)', '              ', 'SLGTAB', 'FKT     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '57        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FLD     ', ' ', ' ', ' ', ' ', '      ', ' ', '58        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'FLGTAB', '              ', '      ', 'FLG     ', ' ', ' ', ' ', ' ', '      ', ' ', '59        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FLZ     ', ' ', ' ', ' ', ' ', '      ', ' ', '60        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Delay Log', '              ', '      ', 'FML     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FMZ     ', ' ', ' ', ' ', ' ', '      ', ' ', '61        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FXT     ', ' ', ' ', ' ', ' ', '      ', ' ', '63        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Gates', '              ', 'L02TAB', 'GAT     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '64        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'GDL     ', ' ', ' ', ' ', ' ', '      ', ' ', '65        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Equipment groups', '              ', '      ', 'GEG     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '66        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'requirenments - Einsatzdaten', '              ', '      ', 'GHD     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '67        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'contracts - Einsatzvorgaben', '              ', '      ', 'GHP     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '68        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Leistungskatalog', '              ', '      ', 'GHS     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '69        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Ground power unit allocation table', '              ', '      ', 'GPA     ', ' ', ' ', ' ', ' ', '      ', ' ', '70        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basic shift plan', '              ', 'L02TAB', 'GPL     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '71        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' premises members', '              ', '      ', 'GPM     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '72        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Gruppenzuordnung', '              ', '      ', 'GRM     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '73        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Groupnames', '              ', '      ', 'GRN     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '74        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Groups Table (SEC)', '              ', 'SLGTAB', 'GRP     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '75        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basic shift plan (weekly)', '              ', 'L02TAB', 'GSP     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '76        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'History Table', '              ', '      ', 'H00     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'History Table', '              ', '      ', 'H01     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Handling agents', '              ', 'L02TAB', 'HAG     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '77        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'HAI     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '78        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'HAS-authentification-table', '              ', '      ', 'HAT     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'HCFTAB', '              ', '      ', 'HCF     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000502144800', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basic Data for Headcount calculation', '20000502145105', '      ', 'HDC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '79        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'HOL     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '80        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'handling status section', '              ', '      ', 'HSS     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'handling agent  type assignment for flights', '              ', 'L02TAB', 'HTA     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '100       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Handling Types', '              ', 'L02TAB', 'HTY     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '81        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Flight De-icing Information', '              ', '      ', 'ICE     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'ICF     ', ' ', ' ', ' ', ' ', '      ', ' ', '82        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'ICH     ', ' ', ' ', ' ', ' ', '      ', ' ', '83        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Table fro logging alerts', '              ', '      ', 'ICS     ', ' ', ' ', ' ', ' ', 'ALERT ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Index Table (SYS)', '              ', '      ', 'IDX     ', ' ', ' ', ' ', ' ', '      ', ' ', '84        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'IFF     ', ' ', ' ', ' ', ' ', '      ', ' ', '85        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'imphdl temp flight table', '              ', '      ', 'IFT     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'IID     ', ' ', ' ', ' ', ' ', '      ', ' ', '86        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'IMC     ', ' ', ' ', ' ', ' ', '      ', ' ', '90        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'IMH     ', ' ', ' ', ' ', ' ', '      ', ' ', '87        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Airport Information', '              ', '      ', 'INF     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '88        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20010214110934', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Interactice Voice System Codes', '20010214111614', '      ', 'IVS     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '89        ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'View on Jobtab and AFTTAB', '              ', '      ', 'JAV     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'JMA     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Jobs table', '              ', '      ', 'JOB     ', ' ', ' ', ' ', ' ', '      ', ' ', '104       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Job Demand resolution table', '              ', '      ', 'JOD     ', ' ', ' ', ' ', ' ', '      ', ' ', '91        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Job types', '              ', '      ', 'JTY     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '92        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Log Table', '              ', '      ', 'L00     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '93        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Log Table', '              ', '      ', 'L01     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '94        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Log Table', '              ', '      ', 'L02     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '95        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Log Table', '              ', '      ', 'L03     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '96        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'LBL     ', ' ', ' ', ' ', ' ', '      ', ' ', '97        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'LOA     ', ' ', ' ', ' ', ' ', '      ', ' ', '98        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Log Table', '              ', '      ', 'LOG     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '99        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MAA     ', ' ', ' ', ' ', ' ', '      ', ' ', '104       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MAW     ', ' ', ' ', ' ', ' ', '      ', ' ', '105       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000804122207', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Minimal Free in Month', '20010202161922', '      ', 'MFM     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '100       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MID     ', ' ', ' ', ' ', ' ', '      ', ' ', '101       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MPG     ', ' ', ' ', ' ', ' ', '      ', ' ', '102       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MSB     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MSC     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Master Shift table', '              ', '      ', 'MSD     ', ' ', ' ', ' ', ' ', '      ', ' ', '103       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MSG     ', ' ', ' ', ' ', ' ', '      ', ' ', '104       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MSR     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'MSS     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000529122247', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basic Data for Mutations', '20000529133400', '      ', 'MUT     ', ' ', ' ', ' ', ' ', 'RMS   ', ' ', '105       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Telex Adresses Table', '              ', 'L02TAB', 'MVT     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '106       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Non flight members', '              ', '      ', 'NFM     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '108       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Non flight premises', '              ', '      ', 'NFP     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '109       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Number Table (SYS)', '              ', '      ', 'NUM     ', ' ', ' ', ' ', ' ', '      ', ' ', '110       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000804123658', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Norm-Arbeitszeit fuer 40h Vertrag', '20000804123922', '      ', 'NWH     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '111       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   ('Defines the relation between ODATAB (absences) and COTTAB (contracts)', '20010124170302', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'ODA assign to COT', '20010202161939', '      ', 'OAC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '112       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Basic data absence', '              ', '      ', 'ODA     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '113       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000310152900', 'TAB   ', '      ', 'ppen enthalten.�Falls zwei Organisationseinheiten den gleichen Dienstgruppencode enthalten sind dennoch zwei unterschiedliche Gruppen (Mitarbeiter) gemeint.�', 'HAJ', ' ', 'Organisation Duty Groups - Dienstgruppen bzw. Kadergruppen m�sse', '20000502101724', '      ', 'ODG     ', ' ', ' ', ' ', 'gebildet werden.�Eine Organisationseinheit kann mehrere Dienstg', 'RMS   ', ' ', '114       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', 'N', ' ', '              ', '      ', 'OPT     ', ' ', ' ', ' ', ' ', '      ', ' ', '80        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Organisational unit', '              ', '      ', 'ORG     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '115       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   ('Defines the relation between ODATAB (absences) and COTTAB (contracts)', '20010129113536', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'PMX assign to COT', '20010202162135', '      ', 'PAC     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '116       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Display Page Table', '              ', '      ', 'PAG     ', ' ', ' ', ' ', ' ', '      ', ' ', '117       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', '      ', '      ', ' ', 'HAJ', ' ', 'Page Layouts', '              ', '      ', 'PAGTAB  ', ' ', ' ', ' ', ' ', '      ', ' ', '1002      ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Parameter table for user manipulated Parameters', '              ', '      ', 'PAR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '118       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Passengerdata', '20001026135033', '      ', 'PAX     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '119       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'PDE     ', ' ', ' ', ' ', ' ', '      ', ' ', '120       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Qualifications', '              ', '      ', 'PER     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '121       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Functions', '              ', '      ', 'PFC     ', 'GRP', ' ', ' ', ' ', 'BDPS  ', ' ', '122       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Passenger Prognose Data', '              ', '      ', 'PGN     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '123       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'PGP     ', ' ', ' ', ' ', ' ', '      ', ' ', '124       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Belegungsvorgabe', '              ', '      ', 'PGR     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '125       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', 'N', ' ', '              ', '      ', 'PIC     ', ' ', ' ', ' ', ' ', '      ', ' ', '89        ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '20000822101028', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Point Matrix', '20000822101307', '      ', 'PMX     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '126       ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Pool Assignment table', '              ', '      ', 'POA     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '127       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Pool Definition Table', '              ', '      ', 'POL     ', ' ', ' ', ' ', ' ', 'AFT   ', ' ', '128       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Reduction level', '              ', '      ', 'PRC     ', ' ', ' ', ' ', ' ', '      ', ' ', '129       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Profiles Table (SEC)', '              ', 'SLGTAB', 'PRV     ', ' ', ' ', ' ', ' ', 'BDPS  ', ' ', '131       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Geometrievorgaben', '              ', '      ', 'PSG     ', ' ', ' ', ' ', ' ', 'OPLA  ', ' ', '132       ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'CSD     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'CSH     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VLE     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Status Rule Header table', '              ', '      ', 'SRH     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'BCO     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'RAL     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Status definition table', '              ', '      ', 'SDE     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'online status table', '              ', '      ', 'OST     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', 'Extension to AFTTAB', '              ', '      ', 'AF1     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', 'SYSADMIN', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'OSE     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FSR     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VPS     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VPL     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SML     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'VFB     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'BDC     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'FRO     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'IRM     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'GSN     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'TOR     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SWC     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'GGG     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
Insert into TABTAB
   (ADDI, CDAT, DBTY, DTYP, DVIW, HOPO, IGHO, LNAM, LSTU, LTAB, LTNA, MODU, OOPT, SMMF, SNAM, TATY, TSPA, URNO, USEC, USEU)
 Values
   (' ', '              ', 'TAB   ', '      ', ' ', 'HAJ', ' ', ' ', '              ', '      ', 'SWR     ', ' ', ' ', ' ', ' ', '      ', ' ', '1         ', ' ', ' ');
COMMIT;
