SET DEFINE OFF;
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, ID_HOPO, CREATED_DATE, OPT_LOCK)
 Values
   ('6', 'fnMaxFlightCount', 'Function to count total number of flights.', 1, '[]', ' ', ' ', TO_DATE('08/26/2014 00:49:04', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, OPT_LOCK)
 Values
   ('7', 'isSecondWithinFirst', 'Function to check if one date pair is within the range of the other', 3, '[{"Name":"OuterTimeFrameStart""Description":"Start Date of Outer Time Frame""DataType":2"Value":null}{"Name":"OuterTimeFrameEnd""Description":"End Date of Outer Time Frame""DataType":2"Value":null}{"Name":"InnerTimeFrameStart""Description":"Start Date of Inner Time Frame""DataType":2"Value":null}{"Name":"InnerTimeFrameEnd""Description":"End Date of Inner Time Frame""DataType":2"Value":null}]', ' ', 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, ID_HOPO, CREATED_DATE, OPT_LOCK)
 Values
   ('8', 'isEquals', 'Function to check if the given two strings are equal.', 3, '[{"Name":"Source""Description":"Main String""DataType":3"Value":""}{"Name":"Target""Description":"Check if two strings are equal ""DataType":3"Value":null}]', ' ', ' ', TO_DATE('09/04/2014 04:14:13', 'MM/DD/YYYY HH24:MI:SS'), 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, OPT_LOCK)
 Values
   ('1', 'getTwoDatesDiffInMinute', 'Function to calculate the difference between two date/time', 1, '[{"Name":"DateTime1""Description":"Date/time 1""DataType":2"Value":null}{"Name":"DateTime2""Description":"Date/time 2""DataType":2"Value":null}]', ' ', 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, OPT_LOCK)
 Values
   ('2', 'addNumber', 'Function to add three numbers 
Example :
Number1 :1
Number2 :2
 $addnumber1 = 3 ', 1, '[{"Name":"Number1""Description":"Number 1 Example :1""DataType":1"Value":null}{"Name":"Number2""Description":"Number 2 Example :2""DataType":1"Value":null}]', ' ', 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, OPT_LOCK)
 Values
   ('5', 'findGapInMinutes', 'Function to calculate the difference between two date/time', 1, '[{"Name":"FromDate1""Description":"From Date 1""DataType":2"Value":null}{"Name":"ToDate1""Description":"To Date 1""DataType":2"Value":null}{"Name":"FromDate2""Description":"From Date 2""DataType":2"Value":null}{"Name":"ToDate2""Description":"To Date 2""DataType":2"Value":null}]', ' ', 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, OPT_LOCK)
 Values
   ('3', 'isOverlap', 'Function to check overlap between 2 date pairs', 3, '[{"Name":"FromDate1""Description":"From Date 1""DataType":2"Value":null}{"Name":"ToDate1""Description":"To Date 1""DataType":2"Value":null}{"Name":"FromDate2""Description":"From Date 2""DataType":2"Value":null}{"Name":"ToDate2""Description":"To Date 2""DataType":2"Value":null}{"Name":"Duration""Description":"Duration""DataType":1"Value":0}]', ' ', 0);
Insert into SYS_FUNCTION
   (ID, NAME, DESCR, RET_TYPE, PARAMS, REC_STATUS, OPT_LOCK)
 Values
   ('4', 'isContain', 'Function to check if the given string2 contains in the given string 2', 3, '[{"Name":"Source""Description":"Main String""DataType":3"Value":null}{"Name":"Target""Description":"Target String to be searched""DataType":3"Value":null}]', ' ', 0);
COMMIT;
