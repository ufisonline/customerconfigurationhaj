SET DEFINE OFF;
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSAR      ', 'Daily Schedule - Arrival                          ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSAG      ', 'Daily Schedule - Arrival Ground                   ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSDG      ', 'Daily Schedule - Departure Ground                 ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSDE      ', 'Daily Schedule - Departure                        ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('FSCH      ', 'Flight Schedule                                   ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DARO      ', 'Daily Rotation                                    ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSAR      ', 'Daily Schedule - Arrival                          ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSAG      ', 'Daily Schedule - Arrival Ground                   ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSDG      ', 'Daily Schedule - Departure Ground                 ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DSDE      ', 'Daily Schedule - Departure                        ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('FSCH      ', 'Flight Schedule                                   ');
Insert into SCNTAB
   (SCCD, SCNM)
 Values
   ('DARO      ', 'Daily Rotation                                    ');
COMMIT;
