SET DEFINE OFF;
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1000', 'exception_code', 'EWALC', 'Error - Wrong Airline', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1001', 'exception_code', 'EWAPT', 'Error - Wrong Airport', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1002', 'exception_code', 'EWACT', 'Error - Wrong Aircraft Type', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1003', 'exception_code', 'EWREG', 'Error - Wrong Aircraft Registration', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1004', 'exception_code', 'EWSEV', 'Error  - Wrong Flight Service Type', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1005', 'exception_code', 'EWVAL', 'Error - Wrong validity range i.e. VAFR > VATO', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1006', 'exception_code', 'ENMAN', 'Error - No Mandatory Data', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1007', 'exception_code', 'EWTM', 'Error - Wrong Time Format', 'MSG_EXCEPTION', 'exception_code', ' ', 'HAJ', '*', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1008', 'schedule_info', 'SEQN', 'Sequence Number', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1009', 'schedule_info', 'ROTA', 'Rotation Flag', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1010', 'schedule_info', 'ACTI', 'Schedule Action', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1011', 'schedule_info', 'SEAS', 'Flight Season', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1012', 'schedule_info', 'VAFR', 'Season validity from', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1013', 'schedule_info', 'VATO', 'Season validity to', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1014', 'schedule_info', 'DOOP', 'Day Of Operation', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1015', 'schedule_info', 'NOSE', 'Number of Aircraft Seats', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1016', 'schedule_info', 'AAL2', 'Arrival IATA Airline Code', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1017', 'schedule_info', 'AAL3', 'Arrival ICAO Airline Code', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1018', 'schedule_info', 'AFLN', 'Arrival Flight Numeric Number', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1019', 'schedule_info', 'ACT3', 'IATA Aircraft Type', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1020', 'schedule_info', 'ACT5', 'ICAO Aircraft Type', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1021', 'schedule_info', 'AOR3', 'Arrival IATA Origin', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1022', 'schedule_info', 'APR3', 'Arrival IATA Previous Station', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1023', 'schedule_info', 'AOR4', 'Arrival ICAO Origin', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1024', 'schedule_info', '38078', 'Arrival ICAO Previous Station', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1025', 'schedule_info', 'ARTM', 'Arrival Time in UTC - HHMM', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1026', 'schedule_info', 'ASTY', 'Arrival Service Type', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1027', 'schedule_info', 'ATER', 'Arrival Terminal', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1028', 'schedule_info', 'AREG', 'Arrival Registration', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1029', 'schedule_info', 'DETM', 'Departure Time in UTC - HHMM', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1030', 'schedule_info', 'DAOF', 'Day Offset 1,2 or blank', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1031', 'schedule_info', 'DNX3', 'Departure Next IATA Station', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1032', 'schedule_info', 'DDE3', 'Departure IATA Destination', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1033', 'schedule_info', 'DNX4', 'Departure Next ICAO Station', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1034', 'schedule_info', 'DDE4', 'Departure ICAO Destination', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1035', 'schedule_info', 'DSTY', 'Departure Service Type', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1036', 'schedule_info', 'DTER', 'Departure Terminal', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1037', 'schedule_info', 'DREG', 'Departure Registration', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1038', 'schedule_info', 'FREQ', 'Frequency Per Week', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1039', 'schedule_info', 'DAL2', 'Departure Airline Code IATA ', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1040', 'schedule_info', 'DAL3', 'Departure Airline Code ICAO ', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1041', 'schedule_info', 'DFLN', 'Departure Flight Number', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1042', 'schedule_info', 'FLT', 'Departure Flight Number', 'CFG_MSG_TEMPLATE', 'content_type', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1043', 'schedule_info', 'Y', 'EditFlag - Edited By User', 'FLT_SEASONAL', 'EDTF', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1044', 'schedule_info', 'A', 'EditFlag - Accepted By User', 'FLT_SEASONAL', 'EDTF', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1045', 'schedule_info', 'onespace', 'EditFlag - Otherwise', 'FLT_SEASONAL', 'EDTF', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1046', 'schedule_info', 'N', 'RequestFlag - New schedule requested', 'FLT_SEASONAL', 'REQF', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1047', 'schedule_info', 'onespace', 'RequestFlag - Otherwise', 'FLT_SEASONAL', 'REQF', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1048', 'schedule_info', 'Y', 'ProcessNowFlag - Process Now', 'FLT_SEASONAL', 'PNOW', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1049', 'schedule_info', 'N', 'ProcessNowFlag - Not to process', 'FLT_SEASONAL', 'PNOW', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1050', 'schedule_info', 'onespace', 'ProcessNowFlag - Process Later', 'FLT_SEASONAL', 'PNOW', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1051', 'schedule_info', 'P', 'ProcessStatus - Process Successful', 'FLT_SEASONAL', 'STAT', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1052', 'schedule_info', 'E', 'ProcessStatus - Process with Errors', 'FLT_SEASONAL', 'STAT', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1053', 'schedule_info', 'onespace', 'ProcessStatus - Not yet process', 'FLT_SEASONAL', 'STAT', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1054', 'schedule_info', 'N', 'ScheduleAction - New schedule', 'FLT_SEASONAL', 'ACTI', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1055', 'schedule_info', 'I', 'ScheduleAction - New schedule', 'FLT_SEASONAL', 'ACTI', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1056', 'schedule_info', 'K', 'ScheduleAction - Schedule confirmed', 'FLT_SEASONAL', 'ACTI', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1057', 'schedule_info', 'X', 'ScheduleAction - Schedule cancelled', 'FLT_SEASONAL', 'ACTI', ' ', 'HAJ', 'FLTIMP', 0);
Insert into SYS_OPERATION_STAT
   (ID, OPERATION_TYPE, STATUS_CODE, STATUS_DESC, TABLE_NAME, FIELD_NAME, REC_STATUS, ID_HOPO, DATA_SOURCE, OPT_LOCK)
 Values
   ('1058', 'schedule_info', 'APR4', 'Arrival ICAO Previous Station', 'CFG_MSG_TEMPLATE', 'content_name', ' ', 'HAJ', 'FLTIMP', 0);
COMMIT;
