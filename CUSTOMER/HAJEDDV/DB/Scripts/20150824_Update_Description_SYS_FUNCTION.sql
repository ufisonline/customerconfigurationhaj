update sys_function set descr='Function to calculate total Passengers at the Gate' where trim(name) = 'fnMaxPaxCountByGate';
update sys_function set descr='Function to add two numbers' where trim(name) = 'addNumber';
update sys_function set descr='Function to check if a given string (source) contains the other string (target)' where trim(name)='isContain';
update sys_function set descr='Function to check whether the two demands have the same flight or not' where trim(name) = 'isSameFlight';
commit;