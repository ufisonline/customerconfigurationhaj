CREATE TABLE CLSTAB
(
  CALL  CHAR(9 BYTE)                            DEFAULT ' ',
  CDAT  CHAR(14 BYTE)                           DEFAULT ' ',
  FLNO  CHAR(9 BYTE)                            DEFAULT ' ',
  HOPO  CHAR(3 BYTE)                            DEFAULT 'HAJ',
  LSTU  CHAR(14 BYTE)                           DEFAULT ' ',
  URNO  NUMBER(10)                              DEFAULT 0,
  USEC  CHAR(32 BYTE)                           DEFAULT ' ',
  USEU  CHAR(32 BYTE)                           DEFAULT ' ',
  VPFR  CHAR(14 BYTE)                           DEFAULT ' ',
  VPTO  CHAR(14 BYTE)                           DEFAULT ' '
)
TABLESPACE CEDA01
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CLSTAB_URNO ON CLSTAB
(URNO)
NOLOGGING
TABLESPACE CEDA01IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

