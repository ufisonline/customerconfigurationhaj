/* 
If the user login to domain ... then no authentication info will be asked from the user 
If the user doesn't login to domain ... he has to login using UFIS authentication info ...
update GCFTAB set PARA='LDAP', VALU='D' where SECT='LDAP_TYPE';
*/

/* Only CEDA
update GCFTAB set PARA='LDAP' ,VALU='X' where SECT='LDAP_TYPE';

*/
delete from GCFTAB where sect in ('LDAP_TYPE','LDAP_SRV_IP1','LDAP_SRV_IP2');
INSERT INTO GCFTAB (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO) VALUES('ACTIVEDIRECTORY', 'LDAP_TYPE','LDAP','N','20130928000000','UFIS$ADMIN','20130928000000','','HAJ');

INSERT INTO GCFTAB (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO) VALUES('ACTIVEDIRECTORY', 'LDAP_SRV_IP1','-','10.1.100.1:389','20130928000000','UFIS$ADMIN','20130928000000','','HAJ');

INSERT INTO GCFTAB (APNA, SECT, PARA, VALU, CDAT, USEC, LSTU, USEU, HOPO) VALUES('ACTIVEDIRECTORY', 'LDAP_SRV_IP2','-','10.1.100.1:389','20130928000000','UFIS$ADMIN','20130928000000','','HAJ');
commit;