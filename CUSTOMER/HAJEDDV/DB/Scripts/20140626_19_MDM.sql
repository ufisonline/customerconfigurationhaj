/* Formatted on 2014/10/14 13:22 (Formatter Plus v4.8.8) */
CREATE TABLE trbtab
  (
    "URNO" NUMBER DEFAULT 0,
    "BNAM" CHAR(5 BYTE) DEFAULT ' ',
    "TERM" CHAR(1 BYTE) DEFAULT ' ',
    "HOPO" CHAR(3 BYTE) DEFAULT 'HAJ',
    "CDAT" CHAR(14 BYTE) DEFAULT ' ',
    "LSTU" CHAR(14 BYTE) DEFAULT ' ',
    "USEC" CHAR(32 BYTE) DEFAULT ' ',
    "USEU" CHAR(32 BYTE) DEFAULT ' ',
    "VAFR" CHAR(14 BYTE) DEFAULT ' ',
    "VATO" CHAR(14 BYTE) DEFAULT ' ',
    "MAXF" CHAR(2 BYTE) DEFAULT ' ',
    "MAXB" CHAR(2 BYTE) DEFAULT ' ',
    "MAXP" CHAR(2 BYTE) DEFAULT ' ',
    "HOME" CHAR(3 BYTE) DEFAULT ' '
  )
  TABLESPACE "CEDA02";
COMMENT ON COLUMN "TRBTAB"."URNO"
IS
  'Unique record number';
COMMENT   ON COLUMN "TRBTAB"."BNAM"
IS
  'Transit Belt Name';
COMMENT   ON COLUMN "TRBTAB"."TERM"
IS
  'Terminal';
COMMENT   ON COLUMN "TRBTAB"."HOPO"
IS
  'Home Airport';
COMMENT   ON COLUMN "TRBTAB"."CDAT"
IS
  'Creation Date';
COMMENT   ON COLUMN "TRBTAB"."LSTU"
IS
  'Last update Date';
COMMENT   ON COLUMN "TRBTAB"."USEC"
IS
  'User Creator';
COMMENT   ON COLUMN "TRBTAB"."USEU"
IS
  'User last update';
COMMENT   ON COLUMN "TRBTAB"."VAFR"
IS
  'Valid from';
COMMENT   ON COLUMN "TRBTAB"."VATO"
IS
  'Valid to';
COMMENT   ON COLUMN "TRBTAB"."MAXF"
IS
  'Max. number of flights';
COMMENT   ON COLUMN "TRBTAB"."MAXB"
IS
  'Max. number of bags';
COMMENT   ON COLUMN "TRBTAB"."MAXP"
IS
  'Max. number of transit passenger';



REM INSERTING into SYSTAB
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Transit Belt Name', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '5       ', 'BNAM            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '            
                                                                                                                    ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Creation Date', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '14      ', 'CDAT            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '                
                                                                                                                ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'DATE', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Home Airport', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '3       ', 'HOPO            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '                 
                                                                                                               ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Last update Date', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '14      ', 'LSTU            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '             
                                                                                                                   ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'DATE', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Max. number of bags', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '2       ', 'MAXB            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '          
                                                                                                                      ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr,
             cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Max. number of flights', 'N',
             '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '2       ', 'MAXF            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '       
                                                                                                                         ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr,
             cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Max. number of transit passenger', 'N',
             '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '2       ', 'MAXP            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', '
N',
             '                                                                                                                                ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Terminal', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '1       ', 'TERM            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '                     
                                                                                                           ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Unique record number', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '22      ', 'URNO            ', 'N  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '         
                                                                                                                       ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'LONG', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('User Creator', 'N', '',
             '',
             ''
             '',
             '',
             '',
             '32', 'USEC', 'C', '',
             '', '',
             '',
             '',
             '', '', '',
             '', 'HAJ', 'N',
             '',
             '',
             '',
             '',
             '',
             'TAB',
             '.', 'N', '',
             '', 'N',
             'TRB', 'TRB', '', 'N',
             'TRIM', '', '1'
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('User last update', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '32      ', 'USEU            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '             
                                                                                                                   ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Valid from', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '14      ', 'VAFR            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '                   
                                                                                                             ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'DATE', '   ', '1         '
            );
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('Valid to', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '14      ', 'VATO            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '                     
                                                                                                           ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'TRB                             ', 'TRB       ', ' ', 'N',
             'DATE', '   ', '1         '
            );


COMMIT ;


ALTER TABLE gattab ADD apbr CHAR(1) DEFAULT ' ';
ALTER TABLE gattab ADD ( "CONC" VARCHAR2(2) );

COMMENT ON COLUMN gattab."CONC" IS '';
ALTER TABLE acttab ADD ( "SEA2" CHAR(3) );

COMMENT ON COLUMN "ACTTAB"."SEA2" IS '';
ALTER TABLE acrtab ADD ( "SEA2" CHAR(3) );

COMMENT ON COLUMN "ACRTAB"."SEA2" IS '';

ALTER TABLE "ALTTAB" ADD ( "OFST" CHAR(10) );

COMMENT ON COLUMN "ALTTAB"."OFST" IS '';
ALTER TABLE "CRCTAB" ADD ( "FIXR" CHAR(1) );

COMMENT ON COLUMN "CRCTAB"."FIXR" IS '';

ALTER TABLE "CHUTAB" ADD ( "HOME" CHAR(3) );

COMMENT ON COLUMN "CHUTAB"."HOME" IS '';
COMMENT ON TABLE coutab IS 'Country Table';
COMMENT ON COLUMN "COUTAB"."USEC" IS 'Record Creation User';
COMMIT ;

REM INSERTING into SYSTAB
INSERT INTO systab
            (addi, clsr, cont,
             datr,
             defa,
             defv,
             depe,
             dscr,
             fele, fina, fity, fmts,
             ftan, gcfl,
             gdsr,
             glfl,
             gsty, gtyp, guid,
             hedr, hopo, indx,
             kont,
             labl,
             logd,
             msgt,
             orie,
             proj,
             refe, reqf, sort,
             stat, syst,
             tana, taty, tolt, ttyp,
             TYPE, tzon, urno
            )
     VALUES ('none', 'N', '                                ',
             '                                                ',
             '                                                                ',
             ' ',
             '                                                                ',
             '                                                                                                                                                                                                                                                          ',
             '1       ', 'APBR            ', 'C  ', '                    ',
             '      ', '                                ',
             '                                ',
             '                                                                ',
             ' ', '                                ', '                    ',
             ' ', 'HAJ', 'N',
             '                         
                                                                                                       ',
             '                                                                ',
             '                                ',
             '                                                                                ',
             ' ',
             'TAB                                                                                                                             ',
             '.                                               ', 'N', '    ',
             '                                ', 'N',
             'GAT                             ', 'GAT       ', ' ', 'N',
             'TRIM', '   ', '1         '
            );

COMMIT ;