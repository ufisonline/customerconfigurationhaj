DROP TABLE ILGTAB CASCADE CONSTRAINTS;

CREATE TABLE ILGTAB
(
  URNO  NUMBER(10)                              DEFAULT 0,
  HOPO  CHAR(3 BYTE)                            DEFAULT 'HAJ',
  USEC  CHAR(32 BYTE)                           DEFAULT ' ',
  USEU  CHAR(32 BYTE)                           DEFAULT ' ',
  CDAT  CHAR(14 BYTE)                           DEFAULT ' ',
  LSTU  CHAR(14 BYTE)                           DEFAULT ' ',
  PNAM  CHAR(16 BYTE)                           DEFAULT ' ',
  COMD  CHAR(1 BYTE)                            DEFAULT ' ',
  TABN  CHAR(8 BYTE)                            DEFAULT ' ',
  FLDN  CHAR(4 BYTE)                            DEFAULT ' ',
  FLAG  CHAR(1 BYTE)                            DEFAULT ' ',
  COMM  CHAR(8 BYTE)                            DEFAULT ' ',
  RURN  NUMBER(10)                              DEFAULT 0,
  OBJN  CHAR(8 BYTE)                            DEFAULT ' ',
  SELE  CHAR(256 BYTE)                          DEFAULT ' ',
  FLDS  VARCHAR2(1000 BYTE)                     DEFAULT ' ',
  DATA  VARCHAR2(4000 BYTE)                     DEFAULT ' ',
  SEND  CHAR(1 BYTE)                            DEFAULT ' ',
  ACKF  CHAR(1 BYTE)                            DEFAULT ' ',
  MSGI  VARCHAR2(32 BYTE)
)
TABLESPACE CEDA02
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX ILGTAB_ACKF ON ILGTAB
(ACKF)
LOGGING
TABLESPACE CEDA02IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3120K
            NEXT             1040K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX ILGTAB_CDAT ON ILGTAB
(CDAT)
LOGGING
TABLESPACE CEDA02IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4160K
            NEXT             1040K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX ILGTAB_PNAM ON ILGTAB
(PNAM)
LOGGING
TABLESPACE CEDA02IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7280K
            NEXT             1040K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX ILGTAB_RURN ON ILGTAB
(RURN)
LOGGING
TABLESPACE CEDA02IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4160K
            NEXT             1040K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX ILGTAB_SEND ON ILGTAB
(SEND)
LOGGING
TABLESPACE CEDA02IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3120K
            NEXT             1040K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX ILGTAB_URNO ON ILGTAB
(URNO)
LOGGING
TABLESPACE CEDA02IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2080K
            NEXT             1040K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

