SET DEFINE OFF;
Insert into NUMTAB
   (ACNU, HOPO, KEYS, MAXN, MINN)
 Values
   (110972, 'HAJ', 'CurrentArr', 2147483646, 1);
Insert into NUMTAB
   (ACNU, HOPO, KEYS, MAXN, MINN)
 Values
   (89536, 'HAJ', 'CurrentDep', 2147483646, 1);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (9015, '                    ', 'HAJ', 'CRITAB    ', 2147483647, 1);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (1000, '                    ', 'HAJ', 'HTATAB    ', 2147483646, 1);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (978012979, '                    ', 'HAJ', 'SNOTAB    ', 2147483646, 1000);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (1, '                    ', 'HAJ', 'TEST      ', 1000, 1);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (339518, '                    ', 'HAJ', 'WARTAB    ', 2147483647, 1);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (58334865, '                    ', 'HAJ', 'H00TAB    ', 2147483646, 1000);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (27717, '                    ', 'HAJ', 'H01TAB    ', 2147483646, 1000);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (1669, '                    ', 'HAJ', 'BMSTAB    ', 2147483647, 1);
Insert into NUMTAB
   (ACNU, FLAG, HOPO, KEYS, MAXN, MINN)
 Values
   (250, '                    ', 'HAJ', 'BTRSTAB   ', 2147483647, 1);
Insert into NUMTAB
   (ACNU, FLAG, KEYS, MAXN, MINN)
 Values
   (967986164, '                    ', 'RALTAB    ', 2147483647, 1);
COMMIT;
