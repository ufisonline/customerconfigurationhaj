------------------------------------------------------------------------------
-- circa 2009 : original by BLE for SOCC.
-- This is the "SATS Solution" for recycling URNOs. Basic principles are
--	FUNC_COLLECT_USED will harvest all used urno's from all non-excluded
--	tables into FRA_COLLECT_URNO. (Excluded tables are those in 
--	FRA_IGNORE_TAB plus some predefined ones.) FRA_COLLECT_URNO
--	will be indexed on the start URNO (SURN) of each collected range.
-- 	PROC_CREATE_GET_RANGE will start from the lowest SURN in
--	FRA_COLLECT_USED and determine free ranges. These free ranges are
--	one more than the end of a range up to one less than the start of the
--	next higher range.  
--
-- 20150330 UFIS-7657
-- Prevent URNO's > signed int (2^31) from being recycled.
-- 1.	PROC_CREATE_GET_RANGE : stop the collection once the SURN of FRA_COLLECT_USED
-- 	is equal or greater than 2^31.
--	The existing code already took care to salvage the remaining unused range
--	from the last processed EURN of FRA_COLLECT_USED up to 2^31.
-- 2.	FUNC_COLLECT_URNO : do not collect a URNO range if the start of the range
--	exceeds 2^31.  Note that if the end of a range exceeds 2^31 that is okay
--	because of the provisions made in (1).  The test on SURN in (1) could be
--	considered just an additional safety check since the new filter on SURN
--	in FUNC_COLLECT_USED to not add such a range to FRA_COLLECT_URNO should
--	be sufficient.

------------------------------------------------------------------------------

create or replace PACKAGE "PCK_URNO_COLLECTOR" AS

  PROCEDURE PROC_COLLECTOR_MAIN;
  PROCEDURE PROC_DROP_INDEX( p_Index IN VARCHAR2 );
  PROCEDURE PROC_CLEAN_FRAUSED;
  PROCEDURE PROC_COLLECT_RANGE_BLK( p_TabName IN VARCHAR2 );
  PROCEDURE PROC_CREATE_INDEX ( p_Table IN VARCHAR2, p_Index IN VARCHAR2, p_Column IN VARCHAR2, p_TableSpace IN VARCHAR2);
  PROCEDURE PROC_CREATE_GET_RANGE;

  FUNCTION FUNC_TRUNCATE_TABLE( p_TabName IN VARCHAR2 )
  RETURN NUMBER;

  FUNCTION FUNC_COLLECT_USED
  RETURN NUMBER;

END PCK_URNO_COLLECTOR;

/

create or replace PACKAGE BODY PCK_URNO_COLLECTOR AS

  PROCEDURE PROC_COLLECTOR_MAIN 
    IS    
    v_Success NUMBER := 0;
    v_MyCount NUMBER := 0;
    v_Func VARCHAR2(32) := 'PROC_COLLECTOR_MAIN';
    BEGIN
      v_Success := FUNC_TRUNCATE_TABLE( 'FRALOG' );

      v_Success := FUNC_TRUNCATE_TABLE( 'FRA_COLLECT_URNO' );
      IF v_Success = -1 THEN
        RETURN;
      END IF;
      PROC_DROP_INDEX( 'FRA_COLLECT_URNO.IDX_SURN' );

      v_Success := FUNC_TRUNCATE_TABLE ( 'FRAPOOL' );
      --PROC_CLEAN_FRAUSED;
      PROC_COLLECT_RANGE_BLK('FRATAB'); 
      PROC_COLLECT_RANGE_BLK('FRAUSED'); 
      PROC_COLLECT_RANGE_BLK('NUMTAB'); 
      v_Success := FUNC_COLLECT_USED;
      IF v_Success = -1 THEN
        RETURN;
      END IF;

      PROC_CREATE_INDEX ('FRA_COLLECT_URNO', 'IDX_SURN', 'SURN', 'CEDA04IDX'); 
      PROC_CREATE_GET_RANGE;
      PROC_FRALOG( v_Func, 'URNO SUCCESSFULLY COLLECTED' );
  END PROC_COLLECTOR_MAIN;
--------------------------------------------------------------------------------------------------------------------------
  FUNCTION FUNC_TRUNCATE_TABLE( p_TabName IN VARCHAR2 )
    RETURN NUMBER
    IS  
    v_SqlStr VARCHAR2(1024);
    v_Func VARCHAR2(32) := 'FUNC_TRUNCATE_TABLE';
    PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
      v_SqlStr := 'TRUNCATE TABLE ' || p_TabName;
      EXECUTE IMMEDIATE v_SqlStr;
      COMMIT;
      PROC_FRALOG( v_Func, v_SqlStr );
      return 1;
      EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            PROC_FRALOG( v_Func, 'ErrCode: ' || TO_CHAR(SQLCODE) || ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150)  );
            RETURN -1;
  END FUNC_TRUNCATE_TABLE;
--------------------------------------------------------------------------------------------------------------------------
  PROCEDURE PROC_DROP_INDEX( p_Index IN VARCHAR2 )
    IS
    v_SqlStr VARCHAR2(1024);
    v_Func VARCHAR2(32) := 'PROC_DROP_INDEX';    
    PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
      v_SqlStr := 'DROP INDEX ' || p_Index;
      PROC_FRALOG( v_Func, v_SqlStr );
      EXECUTE IMMEDIATE v_SqlStr;
      COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          IF SUBSTR( SQLERRM, 1, 9 ) = 'ORA-01418' THEN
            PROC_FRALOG( v_Func, 'INDEX ALREADY DROPPED' );
          ELSE
            ROLLBACK;
            PROC_FRALOG( v_Func, '[Index=' || p_Index || ']' || ' ErrCode: ' || 
                                   TO_CHAR(SQLCODE) || ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) );
          END IF;
  END PROC_DROP_INDEX;
  --------------------------------------------------------------------------------------------------------------------------
  PROCEDURE PROC_CLEAN_FRAUSED
    IS
    v_SqlStr VARCHAR2(1024);
    v_Func VARCHAR2(32) := 'PROC_CLEAN_FRAUSED';    
    PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
      v_SqlStr := 'DELETE FROM FRAUSED WHERE OUTM < (SYSDATE-1) AND WHOC <> ''TRIG_NUMTAB''';
      EXECUTE IMMEDIATE v_SqlStr;
      COMMIT;
      PROC_FRALOG( v_Func, v_SqlStr );
      EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            PROC_FRALOG( v_Func, 'ErrCode: ' || TO_CHAR(SQLCODE) || ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) );
  END PROC_CLEAN_FRAUSED;
  --------------------------------------------------------------------------------------------------------------------------
  PROCEDURE PROC_COLLECT_RANGE_BLK( p_TabName IN VARCHAR2 )
    IS
    TYPE FRACUR IS REF CURSOR;
    v_FraCur FRACUR;
    v_Surn NUMBER;
    v_Eurn NUMBER;
    v_Acnu NUMBER;
    v_Minn NUMBER;
    v_Maxn NUMBER;
    v_RowCount NUMBER := 0;
    v_SqlStr VARCHAR2(1024);
    v_Func VARCHAR2(32) := 'PROC_COLLECT_RANGE_BLK';    
    PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
    IF p_TabName = 'NUMTAB' THEN
        v_SqlStr := 'SELECT MINN,ACNU,MAXN FROM NUMTAB WHERE KEYS = ''SNOTAB''';
        PROC_FRALOG( v_Func, v_SqlStr );
        EXECUTE IMMEDIATE v_SqlStr INTO v_Minn, v_Acnu, v_Maxn;
        v_Eurn := v_Maxn;
        IF v_Acnu - v_Minn > 1000000 THEN
                v_Surn := v_Acnu - 1000000;
        ELSE
                v_Surn := v_Minn;
        END IF;
        INSERT /*+ APPEND */ INTO FRA_COLLECT_URNO VALUES (v_Surn,v_Eurn);
        COMMIT;
        RETURN;
    END IF;

    v_SqlStr := 'SELECT SURN,EURN FROM ' || p_TabName;
    IF p_TabName = 'FRAUSED' THEN
        v_SqlStr := v_SqlStr || ' WHERE OUTM < (SYSDATE-1)';
    END IF;
    PROC_FRALOG( v_Func, v_SqlStr );
    OPEN v_FraCur FOR v_SqlStr;
    LOOP
        FETCH v_FraCur INTO v_Surn, v_Eurn;
        EXIT WHEN v_FraCur%NOTFOUND OR v_FraCur%NOTFOUND IS NULL;
        --PROC_FRALOG( v_Func, 'SURN|' || v_Surn || '|EURN|' || v_Eurn || '|' );

        INSERT /*+ APPEND */ INTO FRA_COLLECT_URNO VALUES (v_Surn,v_Eurn);
        v_RowCount := v_RowCount + 1;
        IF v_RowCount >= 1000 THEN
            COMMIT;
            v_RowCount := 0;
        END IF;
    END LOOP;
    COMMIT;
    CLOSE v_FraCur;
  END PROC_COLLECT_RANGE_BLK;
  --------------------------------------------------------------------------------------------------------------------------
  FUNCTION FUNC_COLLECT_USED
    RETURN NUMBER
    IS
    TYPE TABCUR IS REF CURSOR;
    v_TabCur TABCUR;
    v_UrnoCur TABCUR;
    v_TabName USER_TAB_COLUMNS.TABLE_NAME%TYPE;
    v_RowCount NUMBER := 0;
    v_UrnoCount NUMBER := 0;
    v_UrnoNum NUMBER := -1;
    v_Surn NUMBER := -1;
    v_Eurn NUMBER := -1;
    v_Urno VARCHAR(12);
    v_TabNameSQL VARCHAR(1024);
    v_TabUrnoSQL VARCHAR(1024);
    v_Func VARCHAR2(32) := 'FUNC_COLLECT_USED';

    PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN
      v_TabNameSQL := 'SELECT TABLE_NAME ' || 'FROM USER_TAB_COLUMNS ' ||
                      'WHERE TABLE_NAME LIKE ' || '''___TAB''' || 
                      ' AND COLUMN_NAME = ''URNO'' AND TABLE_NAME NOT IN ' ||
                      '(SELECT TRIM(KEYS) FROM NUMTAB WHERE KEYS IS NOT NULL) ' ||  
                      'AND TRIM(TABLE_NAME) NOT IN ' ||
                      '(SELECT TRIM(TANA) FROM FRA_IGNORE_TAB ) ' ||
                      'AND TABLE_NAME NOT IN ' ||
                      '(SELECT VIEW_NAME FROM USER_VIEWS ) ' ||
                      'ORDER BY TABLE_NAME';

    PROC_FRALOG( v_Func, v_TabNameSQL );
    OPEN v_TabCur FOR v_TabNameSQL;
    
    <<tabname_loop>>
    LOOP
        FETCH v_TabCur INTO v_TabName;
        EXIT WHEN v_TabCur%NOTFOUND OR v_TabCur%NOTFOUND IS NULL;
        
        BEGIN
          v_UrnoCount := 0;
          v_RowCount := 0;
          v_TabUrnoSQL := 'SELECT URNO FROM ' || v_TabName || ' WHERE URNO IS NOT NULL ORDER BY LPAD(TRIM(URNO),10)';
          PROC_FRALOG( v_Func, v_TabUrnoSQL );
 
          OPEN v_UrnoCur FOR v_TabUrnoSQL;
          <<urno_loop>>
          LOOP
              FETCH v_UrnoCur INTO v_Urno;
              EXIT WHEN v_UrnoCur%NOTFOUND OR v_UrnoCur%NOTFOUND IS NULL;
              
              BEGIN
                  v_UrnoNum := TO_NUMBER(v_Urno);
                  IF v_Surn < 0 AND v_Eurn < 0 THEN
                        v_Surn := v_UrnoNum;
                        v_Eurn := v_UrnoNum;
                        GOTO urno_loop;
                  END IF;

                  IF v_UrnoNum = v_Eurn + 1 THEN
                        v_Eurn := v_UrnoNum;
                        GOTO urno_loop;
                  END IF;
                  
                  --PROC_FRALOG( v_Func, '[' || v_TabName || ']' || v_Surn || '-' || v_Eurn );
                  EXECUTE IMMEDIATE
                  'INSERT /*+ APPEND */ INTO FRA_COLLECT_URNO VALUES ( ' || v_Surn || ',' || v_Eurn || ')';

                  IF v_RowCount >= 1000
                  THEN
                      COMMIT;
                      v_RowCount := 0;
                  END IF;
                  v_RowCount := v_RowCount + 1;
                  v_UrnoCount := v_UrnoCount + v_Eurn - v_Surn + 1;
                  v_Surn := v_UrnoNum;
                  v_Eurn := v_UrnoNum;
                  
                  EXCEPTION
                    WHEN VALUE_ERROR THEN
                      PROC_FRALOG( v_Func, '[' || v_TabName || ']' || ' ErrCode: ' || TO_CHAR(SQLCODE) ||
                                           ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) || ' UrnoValue: ' || v_Urno );
                    WHEN OTHERS THEN
                      PROC_FRALOG( v_Func, '[' || v_TabName || ']' ||' ErrCode: ' || TO_CHAR(SQLCODE) ||
                                             ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) );
                     IF v_UrnoCur%ISOPEN = TRUE THEN
                         CLOSE v_UrnoCur;
                     END IF;
                     RETURN -1;
              END;
          END LOOP urno_loop;
          IF v_Surn >= 1000 THEN
              EXECUTE IMMEDIATE        
              'INSERT /*+ APPEND */ INTO FRA_COLLECT_URNO VALUES ( ' || v_Surn || ',' || v_Eurn || ')';
                v_UrnoCount := v_UrnoCount + v_Eurn - v_Surn + 1;
          END IF;
          COMMIT;
          CLOSE v_UrnoCur;
          PROC_FRALOG( v_Func, v_TabName || ' = ' || v_UrnoCount || ' records' );       
          v_Surn := -1;
          v_Eurn := -1;
          
          EXCEPTION
              WHEN OTHERS THEN
                  PROC_FRALOG( v_Func, '[Read TabName] ErrCode: ' || TO_CHAR(SQLCODE) || ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) );
                  IF v_TabCur%ISOPEN = TRUE THEN
                       CLOSE v_TabCur;
                  END IF;
                  RETURN -1; 
        END;  
    END LOOP tabname_loop;

    CLOSE v_TabCur;
    RETURN 1;
    
  END FUNC_COLLECT_USED;
--------------------------------------------------------------------------------------------------------------------------
  PROCEDURE PROC_CREATE_INDEX ( p_Table IN VARCHAR2, p_Index IN VARCHAR2, p_Column IN VARCHAR2, p_TableSpace IN VARCHAR2)
    IS
    v_Success INTEGER := -1;
    v_SqlStr  VARCHAR(1024);
    v_Func    VARCHAR2(32) := 'PROC_CREATE_INDEX';
    PRAGMA AUTONOMOUS_TRANSACTION;
    
    BEGIN
     -- v_SqlStr :=  'CREATE INDEX ' || p_Index || ' ON ' || p_Table || ' ( ' || p_Column || ' ) ' ||
     --              'TABLESPACE ' || p_TableSpace || ' ' ||
     --              'PCTFREE 10 ' || 'INITRANS 2 ' || 'MAXTRANS 255 ' || 'STORAGE ' || '( ' || 'INITIAL 1M ' ||
     --              'NEXT 1M ' || 'MINEXTENTS 1 ' || 'MAXENTENTS UNLIMITED ' || 'PCTINCREASE 0 ' || ')';
     v_SqlStr :=  'CREATE INDEX ' || p_Index || ' ON ' || p_Table || ' ( ' || p_Column || ' ) ' ||
                  'TABLESPACE ' || p_TableSpace; 

    PROC_FRALOG( v_Func, v_SqlStr );
    EXECUTE IMMEDIATE v_SqlStr;
    
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        PROC_FRALOG( v_Func, 'ErrCode: ' || TO_CHAR(SQLCODE) || ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) );
  END PROC_CREATE_INDEX;

--------------------------------------------------------------------------------------------------------------------------
  PROCEDURE PROC_CREATE_GET_RANGE
  IS 
      TYPE UrnoType IS REF CURSOR;
      v_UrnoCur    UrnoType;
      v_StartUrno  NUMBER := 0;
      v_EndUrno    NUMBER := 0;
      v_S          NUMBER := 0;
      v_E          NUMBER := 0;
      v_DiffUrno   NUMBER := 0;
      v_MinInFRATAB  NUMBER := 0;
      v_MinInFRAPOOL NUMBER := 0;
      v_Success    NUMBER := 0;
      v_BigCount   NUMBER := 0;
      v_SmallCount NUMBER := 0;
      v_BigSum     NUMBER := 0;
      v_SmallSum   NUMBER := 0;
      v_LastUrno   NUMBER := 2147483647;
      v_Mkey       VARCHAR2(40);
      v_SqlStr     VARCHAR2(1024);
      v_Func       VARCHAR2(32) := 'PROC_CREATE_GET_RANGE';
  BEGIN
      v_SqlStr := 'SELECT VALU FROM FRACFG WHERE KEYS = ''MIN_IN_FRATAB''';
      EXECUTE IMMEDIATE v_SqlStr INTO v_MinInFRATAB;
      IF v_MinInFRATAB < 2500 THEN
        v_MinInFRATAB := 4000;
      END IF;
      PROC_FRALOG( v_Func, v_SqlStr || ' [' || v_MinInFRATAB || ']' );

      v_SqlStr := 'SELECT VALU FROM FRACFG WHERE KEYS = ''MIN_IN_FRAPOOL''';
      EXECUTE IMMEDIATE v_SqlStr INTO v_MinInFRAPOOL;
      IF v_MinInFRAPOOL <= 0 THEN
        v_MinInFRAPOOL := 50;
      END IF;
      PROC_FRALOG( v_Func, v_SqlStr || ' [' || v_MinInFRAPOOL || ']' );

      v_SqlStr := 'SELECT SURN,EURN FROM FRA_COLLECT_URNO ORDER BY SURN';
      PROC_FRALOG( v_Func, v_SqlStr );

      v_S := 1000;
      v_E := 1000;

      OPEN v_UrnoCur FOR v_SqlStr;
      << used_loop >>
      LOOP
          FETCH v_UrnoCur INTO v_StartUrno, v_EndUrno;

          IF v_StartUrno >= v_LastUrno THEN
            v_SqlStr := 'FRA_COLLECT_URNO.SURN EXCEEDED MAX INT';
            PROC_FRALOG( v_Func, v_SqlStr );
          END IF; 

          EXIT WHEN v_UrnoCur%NOTFOUND OR v_UrnoCur%NOTFOUND IS NULL OR v_StartUrno >= v_LastUrno;

          BEGIN
          IF v_StartUrno < v_S THEN
                IF v_EndUrno < v_E THEN
                    GOTO used_loop;
                END IF;
          ELSIF v_StartUrno > v_S THEN
                v_E := v_StartUrno - 1;
                v_DiffUrno := v_E - v_S + 1;                  
                IF v_DiffUrno >= v_MinInFRAPOOL THEN
                        v_Mkey := substr(TO_CHAR(SYSDATE,'YYYYMMDD'),1,8) || '-' || lpad(v_S,10,'0') || '-' || lpad(v_E,10,'0');
                        IF v_DiffUrno >= v_MinInFRATAB THEN
                                v_BigSum := v_BigSum + v_DiffUrno;
                                v_BigCount := v_BigCount + 1;
                                INSERT INTO FRATAB FIELDS ( MKEY, SURN, EURN, NUMS )
                                                   VALUES ( v_Mkey, v_S, v_E, v_DiffUrno );
                                COMMIT;
                        ELSE
                                  v_SmallSum := v_SmallSum + v_DiffUrno;
                                  v_SmallCount := v_SmallCount + 1;
                                  INSERT INTO FRAPOOL FIELDS ( MKEY, SURN, EURN, NUMS )
                                                      VALUES ( v_Mkey, v_S, v_E, v_DiffUrno );
                                COMMIT;
                        END IF;
                END IF;
          END IF;
          v_S := v_EndUrno + 1;
          v_E := v_EndUrno + 1;
             
          EXCEPTION
              WHEN OTHERS THEN
                  ROLLBACK;
                  PROC_FRALOG( v_Func, 'ErrCode: ' || TO_CHAR(SQLCODE) || ' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) ||
                                       ' Surn = ' || v_StartUrno || ' Eurn = ' || v_EndUrno );
              END;
     END LOOP;
     CLOSE v_UrnoCur;
     
     IF v_E < v_LastUrno THEN
         v_S := v_E;
         v_E := v_LastUrno;
         v_DiffUrno := v_E - v_S + 1;
         IF v_DiffUrno >= v_MinInFRAPOOL THEN
                v_Mkey := substr(TO_CHAR(SYSDATE,'YYYYMMDD'),1,8) || '-' || lpad(v_S,10,'0') || '-' || lpad(v_E,10,'0');
                IF v_DiffUrno >= v_MinInFRATAB THEN
                        v_BigSum := v_BigSum + v_DiffUrno;
                        v_BigCount := v_BigCount + 1;
                        INSERT INTO FRATAB FIELDS ( MKEY, SURN, EURN, NUMS )
                                           VALUES ( v_Mkey, v_S, v_E, v_DiffUrno );
                        COMMIT;
                ELSE
                          v_SmallSum := v_SmallSum + v_DiffUrno;
                          v_SmallCount := v_SmallCount + 1;
                          INSERT INTO FRAPOOL FIELDS ( MKEY, SURN, EURN, NUMS )
                                              VALUES ( v_Mkey, v_S, v_E, v_DiffUrno );
                        COMMIT;
                END IF;
        END IF;
     END IF;
     PROC_FRALOG( v_Func, v_BigCount || ' records INSERTED INTO FRATAB WITH RANGE >= 4000 [SUM=' || v_BigSum || ']' );
     PROC_FRALOG( v_Func, v_SmallCount || ' records INSERTED INTO FRAPOOL WITH 50 <= RANGE < 4000 [SUM=' || v_SmallSum || ']' );
    
     EXCEPTION
         WHEN OTHERS THEN
             ROLLBACK;
             PROC_FRALOG( v_Func, 'ErrCode: ' || TO_CHAR(SQLCODE) ||' ErrMsg: ' || SUBSTR(SQLERRM, 1, 150) );
             IF v_UrnoCur%ISOPEN = TRUE THEN
                 CLOSE v_UrnoCur;
             END IF;
  END PROC_CREATE_GET_RANGE;

END PCK_URNO_COLLECTOR;
/
--------------------------------------------------------------------------------------------------------------------------
