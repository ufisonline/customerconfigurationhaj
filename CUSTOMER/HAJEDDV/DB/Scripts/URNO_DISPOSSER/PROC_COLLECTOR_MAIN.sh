
# Setup Environment
. /ceda/etc/OracleEnv
. /ceda/etc/UfisEnvCron

TMPLOG=/ceda/tmp/`basename $0`$$.log
F_SQL=PROC_COLLECTOR_MAIN.sql

date > $TMPLOG

sqlplus $CEDADBUSER@UFIS/$CEDADBPW @$F_SQL

date >> $TMPLOG
