
CREATE SEQUENCE  FRALOG_SEQN  MINVALUE 1 MAXVALUE 1.00000000000000E+27 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  CYCLE ;

------------------------------------------------------------------------------------------------
CREATE TABLE FRATAB
       (MKEY   CHAR(30) PRIMARY KEY,
        SURN   NUMBER(10),
        EURN   NUMBER(10),
        NUMS   NUMBER(10)
) tablespace CEDA04
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 4M
    next 4M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
COMMENT ON COLUMN FRATAB.MKEY
IS 'Comprising Date-StartUrno-EndUrno';
COMMENT ON COLUMN FRATAB.SURN
IS 'Start Urno';
COMMENT ON COLUMN FRATAB.EURN
IS 'EURN';
COMMENT ON COLUMN FRATAB.NUMS
IS 'Number of URNO in this range';

------------------------------------------------------------------------------------------------

  CREATE INDEX FRATAB_NUMS ON FRATAB ("NUMS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 64K NEXT 64K MINEXTENTS 1 MAXEXTENTS unlimited
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CEDA04IDX" ;

------------------------------------------------------------------------------------------------

CREATE TABLE FRAPOOL
       (MKEY   CHAR(30) PRIMARY KEY,
        SURN   NUMBER(10),
        EURN   NUMBER(10),
        NUMS   NUMBER(10)
) tablespace CEDA04
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 4M
    next 4M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
COMMENT ON COLUMN FRAPOOL.MKEY
IS 'Comprising Date-StartUrno-EndUrno';
COMMENT ON COLUMN FRAPOOL.SURN
IS 'Start Urno';
COMMENT ON COLUMN FRAPOOL.EURN
IS 'EURN';
COMMENT ON COLUMN FRAPOOL.NUMS
IS 'Number of URNO in this range';

------------------------------------------------------------------------------------------------

  CREATE INDEX FRAPOOL_NUMS ON FRAPOOL ("NUMS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 3M NEXT 1M MINEXTENTS 1 MAXEXTENTS unlimited
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CEDA04IDX" ;
 
------------------------------------------------------------------------------------------------
CREATE TABLE FRAUSED
       (MKEY   CHAR(30) PRIMARY KEY,
        SURN   NUMBER(10),
        EURN   NUMBER(10),
        NUMS   NUMBER(10),
        REQU   NUMBER(10),
        OUTM   DATE DEFAULT SYSDATE,
        WHOC   CHAR(20),
        CLIE   CHAR(40)
) tablespace CEDA04
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 4M
    next 4M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
COMMENT ON COLUMN FRAUSED.MKEY
IS 'Comprising Date-StartUrno-EndUrno';
COMMENT ON COLUMN FRAUSED.SURN
IS 'Start Urno';
COMMENT ON COLUMN FRAUSED.EURN
IS 'EURN';
COMMENT ON COLUMN FRAUSED.NUMS
IS 'Number of URNO is this range';
COMMENT ON COLUMN FRAUSED.REQU
IS 'Number of URNO requested';
COMMENT ON COLUMN FRAUSED.OUTM
IS 'Timestamp of slot being taken up';
COMMENT ON COLUMN FRAUSED.WHOC
IS 'Who used up the slot';
COMMENT ON COLUMN FRAUSED.CLIE
IS 'client name, if applicable';

------------------------------------------------------------------------------------------------
  CREATE INDEX FRAUSED_OUTM ON FRAUSED ("OUTM") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 64K NEXT 64K MINEXTENTS 1 MAXEXTENTS unlimited
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CEDA04IDX" ;
 
  CREATE INDEX FRAUSED_WHOC ON FRAUSED ("WHOC") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 64K NEXT 64K MINEXTENTS 1 MAXEXTENTS unlimited
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CEDA04IDX" ;
------------------------------------------------------------------------------------------------
CREATE TABLE FRA_COLLECT_URNO
(
	SURN	NUMBER(10,0),
	EURN	NUMBER(10,0)
)
tablespace CEDA04
  pctfree 10
  initrans 1
  maxtrans 255
  nologging
  storage
  (
    initial 5M
    next 1M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
COMMENT ON COLUMN FRA_COLLECT_URNO.SURN
IS 'Urno exists in CEDA';
------------------------------------------------------------------------------------------------
  CREATE INDEX SURN ON FRA_COLLECT_URNO ("SURN") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 2M NEXT 1M MINEXTENTS 1 MAXEXTENTS unlimited
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CEDA04IDX" ;
------------------------------------------------------------------------------------------------
CREATE TABLE FRA_IGNORE_TAB
(	"TANA" CHAR(12)
) tablespace CEDA04
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

------------------------------------------------------------------------------------------------

CREATE TABLE FRACFG
(	
  "KEYS" VARCHAR2(32),
  "VALU" VARCHAR2(32)	
) tablespace CEDA04
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 1M
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

------------------------------------------------------------------------------------------------
  CREATE INDEX FRACFG_KEYS ON FRACFG ("KEYS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 32K NEXT 32K MINEXTENTS 1 MAXEXTENTS unlimited
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CEDA04IDX" ;
------------------------------------------------------------------------------------------------
CREATE TABLE FRALOG (
  SEQN		NUMBER(10),
  FUNC          VARCHAR2(32),
  MESG     	VARCHAR2(1024), 
  CDAT 		DATE DEFAULT SYSDATE
)
 TABLESPACE CEDA04
   PCTFREE 10   PCTUSED 40
   INITRANS 1   MAXTRANS 255 
 STORAGE ( 
   INITIAL 1M NEXT 1M PCTINCREASE 0
   MINEXTENTS 1 MAXEXTENTS unlimited )
   NOCACHE;
COMMENT ON COLUMN FRALOG.SEQN
IS 'Sequence ID';
COMMENT ON COLUMN FRALOG.FUNC
IS 'Function Name';
COMMENT ON COLUMN FRALOG.MESG
IS 'Log Descriptions';
COMMENT ON COLUMN FRALOG.CDAT
IS 'Log Time';

------------------------------------------------------------------------------------------------
