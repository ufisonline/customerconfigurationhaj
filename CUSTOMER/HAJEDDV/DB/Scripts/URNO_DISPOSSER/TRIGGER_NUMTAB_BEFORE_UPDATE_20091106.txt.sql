create or replace TRIGGER TRIG_NUMTAB
BEFORE UPDATE ON NUMTAB 
REFERENCING OLD AS oldRow NEW AS newRow 
FOR EACH ROW 
DECLARE
vSurn number;
vEurn number;
vSetSurn number;
vSetEurn number;
vNums number;
vRemain number;
vRequest number;
vMkey char(30);
vNewMkey char(30);
vFunc char(12) := 'TRIG_NUMTAB';

BEGIN
        vRemain := :newRow.MAXN - :newRow.ACNU;
        IF (:newRow.keys = 'SNOTAB' and vRemain < 3500) THEN
                vRequest := :newRow.ACNU-:oldRow.ACNU;
                SELECT TRIM(MKEY), SURN, EURN, NUMS INTO vMkey, vSurn, vEurn, vNums FROM (SELECT * FROM FRATAB ORDER BY MKEY ASC) WHERE ROWNUM = 1;
                DELETE FROM FRATAB WHERE MKEY = vMkey;
                INSERT INTO FRAUSED ( MKEY,SURN,EURN,NUMS,WHOC,REQU,CLIE ) VALUES ( vMkey, vSurn, vEurn, vNums, vFunc, vRequest, 'ceda' );
                vNewMkey := substr(TO_CHAR(SYSDATE,'YYYYMMDD'),1,8) || '-' || lpad(:newRow.MINN,10,'0') || '-' || lpad(:newRow.ACNU,10,'0');
                INSERT INTO FRAUSED ( MKEY,SURN,EURN,NUMS,WHOC,REQU,CLIE ) VALUES ( vNewMkey, :newRow.MINN, :newRow.ACNU, :newRow.ACNU-:newRow.MINN+1, vFunc,
0, 'ceda' );
                vNewMkey := substr(TO_CHAR(SYSDATE,'YYYYMMDD'),1,8) || '-' || lpad(:newRow.ACNU,10,'0') || '-' || lpad(:newRow.MAXN,10,'0');
                vRemain := vRemain + 1;
                INSERT INTO FRAPOOL ( MKEY, SURN, EURN, NUMS ) VALUES ( vNewMkey, :newRow.ACNU, :newRow.MAXN, vRemain );
                :newRow.ACNU := vSurn;
                :newRow.MINN := vSurn;
                :newRow.MAXN := vEurn;
        END IF;
END;
/
