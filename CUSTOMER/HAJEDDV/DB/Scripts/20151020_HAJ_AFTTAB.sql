ALTER TABLE AFTTAB ADD PFRB CHAR(5);
ALTER TABLE AFTTAB ADD PTOB CHAR(5);
ALTER TABLE AFTTAB ADD SFIN CHAR(14);
ALTER TABLE AFTTAB ADD CTR1 CHAR(32);
ALTER TABLE AFTTAB ADD CTR2 CHAR(32);
ALTER TABLE AFTTAB ADD CXPX NUMBER(22);
ALTER TABLE AFTTAB ADD CXBG NUMBER(22);
ALTER TABLE AFTTAB ADD CXUD NUMBER(22);

ALTER TABLE AFTTAB MODIFY PFRB  DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY PTOB  DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY SFIN  DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY CTR1  DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY CTR2  DEFAULT ' ';

COMMENT ON COLUMN AFTTAB.PFRB IS 'Plan from bay';
COMMENT ON COLUMN AFTTAB.PTOB IS 'Plan to bay';
COMMENT ON COLUMN AFTTAB.SFIN IS 'Short final date';
COMMENT ON COLUMN AFTTAB.CTR1 IS 'Controller 1';
COMMENT ON COLUMN AFTTAB.CTR2 IS 'Controller 2';
COMMENT ON COLUMN AFTTAB.CXPX IS 'Passenger connection alert count';
COMMENT ON COLUMN AFTTAB.CXBG IS 'Bag connection alert count';
COMMENT ON COLUMN AFTTAB.CXUD IS 'ULD connection alert count';
COMMIT;

DELETE FROM SYSTAB WHERE TANA='AFT' AND FINA IN ('PFRB', 'PTOB', 'SFIN', 'CTR1', 'CTR2', 'CXPX', 'CXBG', 'CXUD');
COMMIT;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') ADDI
,a.column_name FINA
,'HAJ' HOPO
,a.data_length FELE
, substr(a.data_type,0,1) FITY
, 'N' INDX 
,'TAB' PROJ
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' SYST
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'AFTTAB' 
and a.column_name IN ( 
'PFRB'
,'PTOB'
,'SFIN'
,'CTR1'
,'CTR2'
,'CXPX'
,'CXBG'
,'CXUD'
);
commit;